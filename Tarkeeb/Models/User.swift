//
//  User.swift
//  Tarkeeb
//
//  Created by Razi Tiwana on 05/09/2018.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import Foundation
import ObjectMapper

enum Gender : String {
    case male = "Male"
    case female = "Female"
    case other = "Other"
    case notSpecified = "Not Specified"
    
//    var displayName: String {
//        switch self {
//        case .male:
//            return "Male"
//        case .female:
//            return "Female"
//        case .other:
//            return "Other"
//        case .notSpecified:
//            return "Not Specified"
//        }
//    }
    
}

enum AccountType : Int {
    case free = 1
    case premuim = 2
}

enum UserType : Int {
    case ordinary = 1
    case chef = 2
}

struct User: Mappable {
    
    var userID : Int?
    var userName : String?
    var email : String?
    var userBio : String?
    var userPicture : String?
    var coverImage : String?
    var firstName : String?
    var lastName : String?
    var gender = Gender.notSpecified
    var dateOfBirth : String?
    var contactNumber : String?
    var isBanned = false
    var isVerified = false
    var accountType = AccountType.free
    var userType = UserType.ordinary
    var loggedInWithFacebook = false
    var numberOfFollwers = 0
    var numberOfFollwing = 0
    var totalRecipes = 0
    var totalSavedRecipes = 0
    var facebookUserID : String?
    var facebookAccessToken : String?
    var facrbookProfileURL :String?
    var location :String?
    var isFollowed = false
    var chefCaps : ChefCaps?
    
    var notificationID: Int?
    
    var unreadNotificationCount: String?
    
    // just for login
    var password : String?
    
    var isOwnProfile:Bool {
        get {
            return (userID ?? 0) == User.currentUser?.userID
        }
    }
    
    init() {
        
    }
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        userID               <- map["Id"]
        userName             <- map["UserName"]
        email                <- map["Email"]
        userBio              <- map["UserBio"]
        userPicture          <- map["Picture"]
        coverImage           <- map["CoverImage"]
        firstName            <- map["FirstName"]
        lastName             <- map["LastName"]
        gender               <- map["Gender"]
        dateOfBirth          <- map["DateOfBirth"]
        contactNumber        <- map["ContactNumber"]
        isBanned             <- map["IsBanned"]
        isVerified           <- map["isVerified"]
        accountType          <- map["AccountTypeID"]
        userType             <- map["UserTypeID"]
        loggedInWithFacebook <- map["loggedInWithFacebook"]
        numberOfFollwers     <- map["NoOfFollowers"]
        numberOfFollwing     <- map["NoOfFollwing"]
        totalRecipes         <- map["TotalRecipes"]
        totalSavedRecipes    <- map["NoOfSavedRecipes"]
        facebookUserID       <- map["facebookUserID"]
        facebookAccessToken  <- map["facebookAccessToken"]
        facrbookProfileURL   <- map["facebookProfileURL"]
        password             <- map["Password"]
        isFollowed           <- map["isFollowed"]
        chefCaps             <- map["ChefCaps"]
        unreadNotificationCount  <- map["unreadNotificationCount"]
        location  <- map["location"]
        
        if userID == nil {
            userID               <- map["id"]
            userName             <- map["userName"]
            email                <- map["email"]
            userBio              <- map["userBio"]
            userPicture          <- map["picture"]
            coverImage           <- map["coverImage"]
            firstName            <- map["firstName"]
            lastName             <- map["lastName"]
            gender               <- map["gender"]
            dateOfBirth          <- map["dateOfBirth"]
            contactNumber        <- map["contactNumber"]
            isBanned             <- map["isBanned"]
            isVerified           <- map["isVerified"]
            accountType          <- map["accountTypeID"]
            userType             <- map["userTypeID"]
            loggedInWithFacebook <- map["loggedInWithFacebook"]
            numberOfFollwers     <- map["noOfFollowers"]
            numberOfFollwing     <- map["noOfFollwing"]
            totalRecipes         <- map["totalRecipes"]
            totalSavedRecipes    <- map["noOfSavedRecipes"]
            facebookUserID       <- map["facebookUserID"]
            facebookAccessToken  <- map["facebookAccessToken"]
            facrbookProfileURL   <- map["facebookProfileURL"]
            password             <- map["password"]
            isFollowed           <- map["isFollowed"]
            chefCaps             <- map["chefCaps"]
            unreadNotificationCount  <- map["unreadNotificationCount"]
            location  <- map["location"]
        }
    }
    
    func fullName() -> String {
        if let name = firstName {
            if let lastName = lastName {
                return name + " " + lastName
            }
            else {
                return name
            }
        } else {
            return ""
        }
    }
}

extension User {
    
    static func logout() {
        User.currentUser = nil
    }
    
    private static var currentUserKey: String {
        return "CURRENT_USER"
    }
    
    static var currentUser: User? {
        get {
            let defaults = UserDefaults.standard
            guard let userJSON = defaults.object(forKey: currentUserKey) as? [String : Any] else {
                return nil
            }
            
            let user = Mapper<User>().map(JSON: userJSON)
            
            return user
        }
        
        set {
            let defaults = UserDefaults.standard
            
            if let newUser = newValue {
                let userJSONDict = Mapper().toJSON(newUser)
                defaults.set(userJSONDict, forKey: currentUserKey)
                
            } else {
                defaults.set(nil, forKey: currentUserKey)
            }
            defaults.synchronize()
        }
    }
}
