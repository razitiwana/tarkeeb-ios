//
//  Notification.swift
//  Tarkeeb
//
//  Created by Osama Azmat khan on 05/09/2018.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import Foundation
import ObjectMapper

// TODO: - Osama
// Add parameters and Mapping
// Look into ObjectMapper for better understanding
// Add enum for NotificationType



enum NotificationType : Int {
    case other = 0
    case like = 1
    case comment = 2
    case sharedBy = 3
    case follow = 4
}

struct Notification: Mappable {
    
    var notificationID : Int?
    var content : String?
    var notificationType = NotificationType.other
    var isRead: Bool?
    var postTitle : String?
    var postID : Int?
    var postPicture : Int?
    var timeStamp : String?
    var user : User?
    
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        notificationID      <- map["id"]
        postID              <- map["recipeID"]
        timeStamp           <- map["timeStamp"]
        content             <- map["notificationContent"]
        notificationType    <- map["notificationTypeId"]
        isRead              <- map["isRead"]
        postTitle           <- map["notificationRecipeTitle"]
        postID              <- map["notificationRecipeId"]
        postPicture         <- map["notificationRecipePicture"]
        timeStamp           <- map["timeStamp"]
        user                <- map["user"]
    }
}
