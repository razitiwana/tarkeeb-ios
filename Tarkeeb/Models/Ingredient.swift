//
//  Ingredient.swift
//  Tarkeeb
//
//  Created by Razi Tiwana on 05/09/2018.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import Foundation
import ObjectMapper

struct Ingrediant: Mappable {
    
    var ingrediantID : Int?
    var name : String?
    var quantity : String?
    
    init?() {
        
    }
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        ingrediantID   <- map["Id"]
        name           <- map["Name"]
        quantity       <- map["Quantity"]
    }
}
