//
//  Feedback.swift
//  Tarkeeb
//
//  Created by Razi Tiwana on 06/09/2018.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import Foundation
import ObjectMapper

struct Feedback: Mappable {
    
    var feedbackID : Int?
    var content : String?
    var timeStamp : String?
    var user : User?
    var numberOfThumbsUp = 0
    var numberOfThumbsdown = 0
    
    var numberOfVotes: Int  {
        get {
            return numberOfThumbsUp - numberOfThumbsdown
        }
    }
    
    init?() {
        
    }
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        feedbackID         <- map["Id"]
        content            <- map["SuggestionContent"]
        timeStamp          <- map["TimeStamp"]
        user               <- map["User"]
        numberOfThumbsUp   <- map["NoOfThumbsUp"]
        numberOfThumbsdown <- map["NoOfThumbsDown"]
    }
}
