//
//  Constants.swift
//  Tarkeeb
//
//  Created by Razi Tiwana on 30/09/2018.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import Foundation

struct Constants {
    static let countToFetchAtOneTime = 10
    
    // To Solve content inset bug when reload setting this to more then any individual cell can ever be.
     static let estimatedRecipeCellSize = 1000
     static let estimatedNotificationCellSize = 500
    
    // Links
    static let aboutURL = "http://tarkeeb.io"
    static let termsURL = "http://tarkeeb.io/terms-and-conditions/"
    static let privacyPolicy = "http://tarkeeb.io/privacy-policy/"
    static let blogs = "http://tarkeeb.io/blog/"
    static let inviteLink = "http://tarkeeb.io/hamari-tarkeeb/"
    //    static let inviteLink = "http://onelink.to/g7fah2"
    
    // StatusCodes
     static let bannedUser = 453
    
}
