//
//  Comment.swift
//  Tarkeeb
//
//  Created by Razi Tiwana on 06/09/2018.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import Foundation
import ObjectMapper

struct Comment: Mappable {
    
    var commentID : Int?
    var postID : Int?
    var timeStamp : String?
    var content : String?
    var user : User?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        commentID   <- map["id"]
        postID      <- map["recipeID"]
        timeStamp   <- map["timeStamp"]
        content     <- map["discription"]
        user        <- map["user"]
    }
}
