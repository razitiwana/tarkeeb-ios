//
//  SearchResult.swift
//  Tarkeeb
//
//  Created by Razi Tiwana on 05/09/2018.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import Foundation
import ObjectMapper

struct SearchResult: Mappable {
    
    var chefs : [User]?
    var tags : [Tag]?
    var recipies : [Recipe]?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        chefs    <- map["Users"]
        tags     <- map["Tags"]
        recipies <- map["Recipes"]
    }
}
