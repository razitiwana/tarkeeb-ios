//
//  Recipe.swift
//  Tarkeeb
//
//  Created by Razi Tiwana on 05/09/2018.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import Foundation
import ObjectMapper
import SwiftDate

enum PostType : Int {
    case recipe = 1
    case sharedRecipe = 2
}

struct Recipe: Mappable {
    
    var user : User?
    var postID : Int?
    var title : String?
    var discription : String?
    var picture : String?
    var timeRequired : String?
    var timeStamp : Double?
    var postType = PostType.recipe
    var sharingUser : User?
    var sharingTimeStamp: String?
    var isLiked = false
    var isSaved = false
    var servings: String?
    
    var notificationID: Int?
    
    var postViews = 0
    var numberOfInstructions = 0
    var numberOfIngrediants = 0
    var numberOflikes = 0
    var numberOfComments = 0
    
    var instructions : [Instruction]?
    var ingrediants : [Ingrediant]?
    var tags : [Tag]?
    
    var postedTime : Date? {
        get {
            
            if let timeStamp  = timeStamp {
                let date = Date(timeIntervalSince1970: TimeInterval(timeStamp / 1000))
//            let date = DateInRegion(string:String(timeStamp.prefix(19)), format: .custom("yyyy-MM-dd'T'HH:mm:ss"), fromRegion: Region.GMT())?.absoluteDate
                return date
            }
            return nil
        }
    }
    
    init() {
        
    }
    
    init?(map: Map) {
        user                 <- map["user"]
        postID               <- map["id"]
        title                <- map["title"]
        discription          <- map["discription"]
        picture              <- map["picture"]
        timeRequired         <- map["timeRequired"]
        timeStamp            <- map["timeStamp"]
        postType             <- map["postType"]
        sharingUser          <- map["sharingUser"]
        sharingTimeStamp     <- map["sharingTimeStamp"]
        isLiked              <- map["isLiked"]
        isSaved              <- map["isSaved"]
        postViews            <- map["postViews"]
        numberOfInstructions <- map["recipeInstructions"]
        numberOfIngrediants  <- map["recipeIngredients"]
        numberOflikes        <- map["noOfLikes"]
        numberOfComments     <- map["noOfComments"]
        instructions         <- map["instructionData"]
        ingrediants          <- map["ingredientData"]
        tags                 <- map["recipeTags"]
        servings                 <- map["servings"]
        
    }
    
    func mapping(map: Map) {
        
    }
}
