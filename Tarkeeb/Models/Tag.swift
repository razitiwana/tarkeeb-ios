//
//  Tag.swift
//  Tarkeeb
//
//  Created by Razi Tiwana on 05/09/2018.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import Foundation
import ObjectMapper

struct Tag: Mappable {
    
    var tagID : Int?
    var name : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        tagID     <- map["Id"]
        name      <- map["Name"]
    }
}
