//
//  ForceUpdate.swift
//  Tarkeeb
//
//  Created by Razi Tiwana on 05/09/2018.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import Foundation
import ObjectMapper

struct ForceUpdate: Mappable {
    
    var platform : String?
    var minimumSupportedVersion : String?
    var latestVersion : String?
    var shouldForceUpdate = false
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        platform                  <- map["Platform"]
        minimumSupportedVersion   <- map["MinimumSupportedVersion"]
        latestVersion             <- map["LatestVersion"]
    }
}
