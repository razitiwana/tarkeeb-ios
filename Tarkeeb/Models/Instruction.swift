//
//  Instruction.swift
//  Tarkeeb
//
//  Created by Razi Tiwana on 05/09/2018.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import Foundation
import ObjectMapper

struct Instruction: Mappable {
    
    var instructionID : Int?
    var title : String?
    var number : Int?
    var content : String?
    var picture : String?
    
    init?() {
        
    }
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        instructionID   <- map["Id"]
        title           <- map["Title"]
        number          <- map["Number"]
        content     <- map["Discription"]
        picture         <- map["Picture"]
    }
}
