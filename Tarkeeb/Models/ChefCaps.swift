//
//  ChefCaps.swift
//  Tarkeeb
//
//  Created by Razi Tiwana on 05/09/2018.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import Foundation
import ObjectMapper

struct ChefCaps: Mappable {
    
    var cap1 : String?
    var cap2 : String?
    var cap3 : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        cap1   <- map["Cap1"]
        cap2   <- map["Cap2"]
        cap3   <- map["Cap3"]
    }
}
