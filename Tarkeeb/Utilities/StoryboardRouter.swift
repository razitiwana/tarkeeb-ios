//
//  StoryboardRouter.swift
//  Tarkeeb
//
//  Created by Razi Tiwana on 05/09/2018.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import Foundation
import UIKit

class StoryboardRouter {
    
    // MARK: - Storyboards
    
    static private func mainStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
    
    static private func homeStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "Home", bundle: Bundle.main)
    }
    
    static private func searchStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "Search", bundle: Bundle.main)
    }
    
    static private func notificationStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "Notification", bundle: Bundle.main)
    }
    
    static private func profileStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "Profile", bundle: Bundle.main)
    }
    
    static private func feedbackStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "Feedback", bundle: Bundle.main)
    }
    
    static private func settingsStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "Settings", bundle: Bundle.main)
    }
    
    static private func createStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "Create", bundle: Bundle.main)
    }
    
    static private func miscellaneousStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "Miscellaneous", bundle: Bundle.main)
    }
    
    
    
    // MARK: - ViewControllers
    
    static func signInViewController() -> SignInViewController {
        return mainStoryboard().instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
    }
    
    static func mainTabViewController() -> MainTabBarViewController {
        return mainStoryboard().instantiateViewController(withIdentifier: "MainTabBarViewController") as! MainTabBarViewController
    }
    
    static func profileViewController() -> ProfileViewController {
        return profileStoryboard().instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
    }
    
    static func recipeDetailViewController() -> RecipeDetailViewController {
        return homeStoryboard().instantiateViewController(withIdentifier: "RecipeDetailViewController") as! RecipeDetailViewController
    }
    
    static func submitFeedbackViewController() -> SubmitFeedbackViewController {
        return feedbackStoryboard().instantiateViewController(withIdentifier: "SubmitFeedbackViewController") as! SubmitFeedbackViewController
    }
    
    
    static func signUpViewController() -> SignUpViewController {
        return mainStoryboard().instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
    }
    
    static func settingsViewController() -> SettingsViewController {
        return settingsStoryboard().instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
    }
    
    static func followersAndFollowingViewController() -> FollowersAndFollowingViewController {
        return profileStoryboard().instantiateViewController(withIdentifier: "FollowersAndFollowingViewController") as! FollowersAndFollowingViewController
    }
    
    static func forceUpdateViewController() -> ForceUpdateViewController {
        return mainStoryboard().instantiateViewController(withIdentifier: "ForceUpdateViewController") as! ForceUpdateViewController
    }
    
    static func searchViewController() -> MainSearchViewController {
        return searchStoryboard().instantiateViewController(withIdentifier: "MainSearchViewController") as! MainSearchViewController
    }
    
    static func forgotPasswordViewController() -> ForgotPasswordViewController {
        return mainStoryboard().instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
    }
   
    static func createRecipeViewController() -> CreateRecipeViewController {
        return createStoryboard().instantiateViewController(withIdentifier: "CreateRecipeViewController") as! CreateRecipeViewController
    }
    
    static func EditProfileViewController() -> EditProfileViewController {
        return profileStoryboard().instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
    }
    
    static func GenericPickerViewController() -> GenericPickerViewController {
        return miscellaneousStoryboard().instantiateViewController(withIdentifier: "GenericPickerViewController") as! GenericPickerViewController
    }
    
    // MARK: - Navigation ViewControllers
    
    static func searchNavigationViewController() -> UINavigationController {
        return searchStoryboard().instantiateViewController(withIdentifier: "SearchNavigationController") as! UINavigationController
    }
    
    static func feedbackNavigationViewController() -> UINavigationController {
        return feedbackStoryboard().instantiateViewController(withIdentifier: "SubmitFeedbackNavViewController") as! UINavigationController
    }
    
    static func search1NavigationViewController() -> UINavigationController {
        return searchStoryboard().instantiateViewController(withIdentifier: "MainSearchNavViewController") as! UINavigationController
    }
    
    
    static func homeNavigationViewController() -> UINavigationController {
        return homeStoryboard().instantiateViewController(withIdentifier: "HomeNavigationController") as! UINavigationController
    }
    
    static func notificationNavigationViewController() -> UINavigationController {
        return notificationStoryboard().instantiateViewController(withIdentifier: "NotifiactionNavigationViewController") as! UINavigationController
    }
    
    static func profileNavigationViewController() -> UINavigationController {
        return profileStoryboard().instantiateViewController(withIdentifier: "ProfileNavigationViewController") as! UINavigationController
    }
    
    static func settingNavigationViewController() -> UINavigationController {
        return settingsStoryboard().instantiateViewController(withIdentifier: "SettingsNavViewController") as! UINavigationController
    }
    
    static func createRecipeNavigationViewController() -> UINavigationController {
        return createStoryboard().instantiateViewController(withIdentifier: "CreateNavRecipeViewController") as! UINavigationController
    }
    
    static func EditProfileNavigationViewController() -> UINavigationController {
        return profileStoryboard().instantiateViewController(withIdentifier: "EditProfileNavigationViewController") as! UINavigationController
    }

    
}
