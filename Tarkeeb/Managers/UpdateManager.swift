//
//  UpdateManager.swift
//  Tarkeeb
//
//  Created by Osama Azmat Khan on 10/5/18.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import UIKit

class UpdateManager {
    
    static let sharedManager = UpdateManager()
    
    var updateInfo: ForceUpdate?
    
    let appDelegate = UIApplication.shared.delegate as? AppDelegate
    let currentVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String
    
    init() {
        
    }
    
    func checkIfNewVersionAvailable() {
        APIManager.sharedManager.fetchForceUpdateDetail(apiCompletion: { (responseValue, error) in
            
            if (error == nil) {
                self.updateInfo = responseValue
                
                if self.shouldShowUpdateViewController() {
                    self.showUpdateViewController()
                }
                
            } else {
                
            }
        })
    
    }
    
    func shouldShowUpdateViewController() -> Bool {
        
        guard let _ = updateInfo else {
            return false
        }
        
        var updateAvaliable = false
        var shouldForceUpdate = false
        
        // update logic here
        
        updateAvaliable = updateInfo?.latestVersion?.compare(currentVersion!, options: .numeric) == .orderedDescending
        
        shouldForceUpdate = updateInfo?.minimumSupportedVersion?.compare(currentVersion!, options: .numeric) == .orderedDescending
        
        self.updateInfo?.shouldForceUpdate = shouldForceUpdate
        
        return updateAvaliable
    }
    
    
    func showUpdateViewController()  {
        let viewController = StoryboardRouter.forceUpdateViewController()
        viewController.forceUpdate = (updateInfo?.shouldForceUpdate)!
        
        appDelegate?.window?.rootViewController?.present(viewController, animated: true, completion: nil)
    }
    
}


