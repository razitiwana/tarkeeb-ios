//
//  APIManager.swift
//  Tarkeeb
//
//  Created by Razi Tiwana on 05/09/2018.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

private enum RequestType: String {
    case Request    = "Request"
    case Response   = "Response"
}

class APIManager {
//    Developement Link
//    private var baseURL = "http://tarkeebapi.tarkeeb.a2hosted.com/api/TarkeebController/"
    
//    Production Link
    private var baseURL = "http://tarkeebapiproduction.tarkeeb.a2hosted.com/api/TarkeebController/"
    

    // MARK: - Singleton
    static let sharedManager = APIManager()
    
    let appDelegate = UIApplication.shared.delegate as? AppDelegate
    
    private init() {
        
    }
    
    // MARK: - Authentication
    
    func login(with user: User , apiCompletion: @escaping (_ responseValue: User?, _ error: String?) -> Void) {
        let url = baseURL + "SignInUser"
        
        let params = Mapper().toJSON(user) as [String : AnyObject]
        
        self.postRequest(url: url, params: params, apiCompletion: { (responseValue, error) in
            
            if error == nil {
                let user = Mapper<User>().map(JSON: responseValue as! [String : Any])
                apiCompletion(user , nil)
            } else {
                apiCompletion(nil , error)
            }
        })
        
    }
    
    func signUp(with user: User, imageData: Data?, fileName: String?, apiCompletion: @escaping (_ responseValue: User?, _ error: String?) -> Void, progressHandler:@escaping (Progress) -> Void)  {
        let url = baseURL + "SignUpUser"
        
        let params = Mapper().toJSON(user) as [String : AnyObject]
        
        
        self.postMultiPartRequest(url: url, params: params, imageData: imageData, imageNameParam: "Picture", fileName: fileName, apiCompletion: { (responseValue, error) in
            
            if error == nil {
                let user = Mapper<User>().map(JSON: responseValue as! [String : Any])
                apiCompletion(user , nil)
            } else {
                apiCompletion(nil , error)
            }
            
        }, progressHandler: progressHandler)
    }
    
    func loginViaFacebook(with user: User , apiCompletion: @escaping (_ responseValue: User?, _ error: String?) -> Void) {
        let url = baseURL + "SignInViaFacebook"
        
        let params = Mapper().toJSON(user) as [String : AnyObject]
        
        self.postRequest(url: url, params: params, apiCompletion: { (responseValue, error) in
            
            if error == nil {
                let user = Mapper<User>().map(JSON: responseValue as! [String : Any])
                apiCompletion(user , nil)
            } else {
                apiCompletion(nil , error)
            }
        })
    }
    
    func resetPassword(with email: String , apiCompletion: @escaping (_ responseValue: String?, _ error: String?) -> Void) {
        let url = baseURL + "ForgotPassword"
        
        let params = ["email" : email] as [String : AnyObject]
        
        self.postRequest(url: url, params: params, apiCompletion: { (responseValue, error) in
            if error == nil {
                apiCompletion(responseValue as? String , nil)
            } else {
                apiCompletion(nil , error)
            }
        })
    }
    
    func changePassword(with currentPassword: String, newPassword: String, apiCompletion: @escaping (_ responseValue: String?, _ error: String?) -> Void) {
        let url = baseURL + "ChangePassword"
        
        let params = ["Id" : User.currentUser?.userID! as AnyObject,
                      "CurrentPassword" : currentPassword,
                      "NewPassword" : newPassword] as [String : AnyObject]
        
        self.postRequest(url: url, params: params, apiCompletion: { (responseValue, error) in
            if error == nil {
                apiCompletion(responseValue as? String , nil)
            } else {
                apiCompletion(nil , error)
            }
        })
    }
    
    
    // MARK: - Profile
    
    func fetchDetail(for user: User, apiCompletion: @escaping (_ responseValue: User?, _ error: String?) -> Void) {
        let url = baseURL + "GetUserDetail"
        
        var params = ["RequestingUserID" : User.currentUser?.userID! as AnyObject,
                      "userID" : user.userID!] as [String : AnyObject]
        
        if user.notificationID != nil {
            params = ["RequestingUserID" : User.currentUser?.userID! as AnyObject,
                      "userID" : user.userID!,
                      "UserNotificationID" : user.notificationID!
                ]  as [String : AnyObject]
        }
        
        let notificationID = user.notificationID
        
        self.postRequest(url: url, params: params, apiCompletion: { (responseValue, error) in
            if error == nil {
                let user = Mapper<User>().map(JSON: responseValue as! [String : Any])
                apiCompletion(user , nil)
                
                // Updating Notification count
                if notificationID != nil {
                    NotificationManager.sharedManager.fecthUnReadNotificationCount()
                }
            } else {
                apiCompletion(nil , error)
            }
        })
    }
    
    // MARK: - Edit Profile
    
    func editProfile(for user: User , apiCompletion: @escaping (_ responseValue: User?, _ error: String?) -> Void) {
        let url = baseURL + "EditUserProfile"
        
        let params = Mapper().toJSON(user) as [String : AnyObject]
        
        self.postRequest(url: url, params: params, apiCompletion: { (responseValue, error) in
            
            if error == nil {
                let user = Mapper<User>().map(JSON: responseValue as! [String : Any])
                apiCompletion(user , nil)
            } else {
                apiCompletion(nil , error)
            }
        })
    }
    
    func updateUserImage(with imageData: Data?, fileName: String?, apiCompletion: @escaping (_ responseValue: User?, _ error: String?) -> Void, progressHandler:@escaping (Progress) -> Void)  {
        let url = baseURL + "UpdateUserImage"
        let userID = User.currentUser?.userID
        let params = ["Id" : userID?.toString as AnyObject] as [String : AnyObject]
        
        self.postMultiPartRequest(url: url, params: params, imageData: imageData, imageNameParam: "Picture", fileName: fileName, apiCompletion: { (responseValue, error) in
            
            if error == nil {
                let user = Mapper<User>().map(JSON: responseValue as! [String : Any])
                apiCompletion(user , nil)
            } else {
                apiCompletion(nil , error)
            }
            
        }, progressHandler: progressHandler)
    }
    
    func updateUserCoverImage(with imageData: Data?, fileName: String?, apiCompletion: @escaping (_ responseValue: User?, _ error: String?) -> Void, progressHandler:@escaping (Progress) -> Void)  {
        let url = baseURL + "UpdateUserCoverImage"
        let userID = User.currentUser?.userID
        let params = ["Id" : userID?.toString as AnyObject] as [String : AnyObject]
        
        self.postMultiPartRequest(url: url, params: params, imageData: imageData, imageNameParam: "Picture", fileName: fileName, apiCompletion: { (responseValue, error) in
            
            if error == nil {
                let user = Mapper<User>().map(JSON: responseValue as! [String : Any])
                apiCompletion(user , nil)
            } else {
                apiCompletion(nil , error)
            }
            
        }, progressHandler: progressHandler)
    }
    
    
    // MARK: - Following
    
    func fetchFollowing(of user: User, startIndex: Int, count: Int,  apiCompletion: @escaping (_ responseValue: [User]?, _ error: String?) -> Void) {
        let url = baseURL + "GetUserFollowing"
        
        let params = ["RequestingUserID" : User.currentUser?.userID! as AnyObject,
                      "userID" : user.userID!,
                      "Index" : startIndex,
                      "Count" : count]  as [String : AnyObject]
        
        self.postRequest(url: url, params: params, apiCompletion: { (responseValue, error) in
            if error == nil {
                let users =  Mapper<User>().mapArray(JSONArray:responseValue as! [[String : Any]])
                apiCompletion(users , nil)
            } else {
                apiCompletion(nil , error)
            }
        })
    }
    
    func fetchFollowers(for user: User, startIndex: Int, count: Int,  apiCompletion: @escaping (_ responseValue: [User]?, _ error: String?) -> Void) {
        let url = baseURL + "GetUserFollowers"
        
        let params = ["RequestingUserID" : User.currentUser?.userID! as AnyObject,
                      "userID" : user.userID!,
                      "Index" : startIndex,
                      "Count" : count]  as [String : AnyObject]
        
        self.postRequest(url: url, params: params, apiCompletion: { (responseValue, error) in
            if error == nil {
                let users =  Mapper<User>().mapArray(JSONArray:responseValue as! [[String : Any]])
                apiCompletion(users , nil)
            } else {
                apiCompletion(nil , error)
            }
        })
    }
    
    func follow(user: User, apiCompletion: @escaping (_ responseValue: String?, _ error: String?) -> Void) {
        let url = baseURL + ((user.isFollowed) ? "UnFollowUser" : "FollowUser")
        
        let params = ["FollowerUserId" : User.currentUser?.userID! as AnyObject,
                      "FollowingUserId" : user.userID!]  as [String : AnyObject]
        
        self.postRequest(url: url, params: params, apiCompletion: { (responseValue, error) in
            if error == nil {
                apiCompletion(responseValue as? String , nil)
            } else {
                apiCompletion(nil , error)
            }
        })
    }
    
    // MARK: - Post
    
    func fetchAllPosts(withStartIndex startIndex: Int, count: Int,  apiCompletion: @escaping (_ responseValue: [Recipe]?, _ error: String?) -> Void) {
        let url = baseURL + "GetAllPosts"
        
        let params = ["userID" : User.currentUser?.userID! as AnyObject,
                      "RequestingUserID" : User.currentUser?.userID! as AnyObject,
                      "Index" : startIndex,
                      "Count" : count]  as [String : AnyObject]
        
        self.postRequest(url: url, params: params, apiCompletion: { (responseValue, error) in
            if error == nil {
                let posts =  Mapper<Recipe>().mapArray(JSONArray:responseValue as! [[String : Any]])
                apiCompletion(posts , nil)
            } else {
                apiCompletion(nil , error)
            }
        })
    }
    
    func fetchPosts(of user: User, startIndex: Int, count: Int,  apiCompletion: @escaping (_ responseValue: [Recipe]?, _ error: String?) -> Void) {
        let url = baseURL + "GetUserPosts"
        
        let params = ["RequestingUserID" : User.currentUser?.userID! as AnyObject,
                      "userID" : user.userID!,
                      "Index" : startIndex,
                      "Count" : count]  as [String : AnyObject]
        
        self.postRequest(url: url, params: params, apiCompletion: { (responseValue, error) in
            if error == nil {
                let posts =  Mapper<Recipe>().mapArray(JSONArray:responseValue as! [[String : Any]])
                apiCompletion(posts , nil)
            } else {
                apiCompletion(nil , error)
            }
        })
    }
    
    func fetchDetail(for post: Recipe, apiCompletion: @escaping (_ responseValue: Recipe?, _ error: String?) -> Void) {
        let url = baseURL + "GetInstructionsAndIngredientsOfPost"
        
        var params = ["RequestingUserID" : User.currentUser?.userID! as AnyObject,
                      "Id" : post.postID!]  as [String : AnyObject]
        
        if post.notificationID != nil {
            params = ["RequestingUserID" : User.currentUser?.userID! as AnyObject,
                      "Id" : post.postID!,
                      "UserNotificationID" : post.notificationID!
                ]  as [String : AnyObject]
        }
        
        let notificationID = post.notificationID
        
        self.postRequest(url: url, params: params, apiCompletion: { (responseValue, error) in
            if error == nil {
                let post = Mapper<Recipe>().map(JSON: responseValue as! [String : Any])
                apiCompletion(post , nil)
                
                // Updating Notification count
                if notificationID != nil {
                    NotificationManager.sharedManager.fecthUnReadNotificationCount()
                }
            } else {
                apiCompletion(nil , error)
            }
        })
    }
    
    func addNewPost(with post: Recipe, imageData: Data?, instructionArrayString: String?, ingredientArrayString: String?, fileName: String?, apiCompletion: @escaping (_ responseValue: String?, _ error: String?) -> Void, progressHandler:@escaping (Progress) -> Void)  {
        let url = baseURL + "AddNewPost"
        
        //let params = Mapper().toJSON(post) as [String : AnyObject]
        
        let userID = User.currentUser?.userID
        
        let params = [
            "Title": post.title as AnyObject,
            "Discription": post.discription as AnyObject,
            "TimeRequired": post.timeRequired as AnyObject,
            "Serving": post.servings as AnyObject,
            "Tags": "[]",
            "Instructions": instructionArrayString as AnyObject,
            "Ingredients": ingredientArrayString as AnyObject,
            "UserID": userID?.toString as AnyObject
            ] as [String : AnyObject]
        
        self.postMultiPartRequest(url: url, params: params, imageData: imageData, imageNameParam: "Picture", fileName: fileName, apiCompletion: { (responseValue, error) in

            if error == nil {
                apiCompletion(responseValue as? String , nil)
            } else {
                apiCompletion(nil , error)
            }

        }, progressHandler: progressHandler)
    }
    
    func addPostDetail(with post: Recipe , apiCompletion: @escaping (_ responseValue: Recipe?, _ error: String?) -> Void) {
        let url = baseURL + "AddPostDetails"
        
        let params = Mapper().toJSON(post) as [String : AnyObject]
        
        self.postRequest(url: url, params: params, apiCompletion: { (responseValue, error) in
            
            if error == nil {
                let post = Mapper<Recipe>().map(JSON: responseValue as! [String : Any])
                apiCompletion(post , nil)
            } else {
                apiCompletion(nil , error)
            }
        })
    }
    
    func addImageWithStep(with post: Recipe, instruction :Instruction , imageData: Data?, fileName: String?, apiCompletion: @escaping (_ responseValue: String?, _ error: String?) -> Void, progressHandler:@escaping (Progress) -> Void)  {
        let url = baseURL + "AddImageForInstruction"
        
        let params = ["UserId" : User.currentUser?.userID! as AnyObject,
                      "InstructionId" : instruction.instructionID!,
                      "RecipeId" : post.postID!,
                      "Number" : instruction.number!]  as [String : AnyObject]
        
        self.postMultiPartRequest(url: url, params: params, imageData: imageData, imageNameParam: "Picture", fileName: fileName, apiCompletion: { (responseValue, error) in
            
            if error == nil {
                apiCompletion(responseValue as? String , nil)
            } else {
                apiCompletion(nil , error)
            }
            
        }, progressHandler: progressHandler)
    }
    
    
    // MARK: - Edit Post
    
    func editPost(with post:Recipe, apiCompletion: @escaping (_ responseValue: Recipe?, _ error: String?) -> Void) {
        let url = baseURL + "EditPost"
        
        let params = Mapper().toJSON(post) as [String : AnyObject]
        
        self.postRequest(url: url, params: params, apiCompletion: { (responseValue, error) in
            if error == nil {
                let post = Mapper<Recipe>().map(JSON: responseValue as! [String : Any])
                apiCompletion(post , nil)
            } else {
                apiCompletion(nil , error)
            }
        })
    }
    
    func deletePost(with post:Recipe, apiCompletion: @escaping (_ responseValue: String?, _ error: String?) -> Void) {
        let url = baseURL + "DeleteUserPost"
        
        let params = ["Id" : post.postID!]  as [String : AnyObject]
        
        self.postRequest(url: url, params: params, apiCompletion: { (responseValue, error) in
            if error == nil {
                apiCompletion(responseValue as? String , nil)
            } else {
                apiCompletion(nil , error)
            }
        })
    }
    
    func updateImage(with post :Recipe, imageData: Data?, fileName: String?, apiCompletion: @escaping (_ responseValue: User?, _ error: String?) -> Void, progressHandler:@escaping (Progress) -> Void)  {
        let url = baseURL + "UpdatePostImage"
        
        let params = ["Id" : post.postID! as AnyObject] as [String : AnyObject]
        
        self.postMultiPartRequest(url: url, params: params, imageData: imageData, imageNameParam: "Picture", fileName: fileName, apiCompletion: { (responseValue, error) in
            
            if error == nil {
                let user = Mapper<User>().map(JSON: responseValue as! [String : Any])
                apiCompletion(user , nil)
            } else {
                apiCompletion(nil , error)
            }
            
        }, progressHandler: progressHandler)
    }
    
    
    // MARK: - ForceUpdate
    
    func fetchForceUpdateDetail(apiCompletion: @escaping (_ responseValue: ForceUpdate?, _ error: String?) -> Void) {
        let url = baseURL + "getVersionDetail"
        
        let params = ["Platform" : "iOS"]  as [String : AnyObject]
        
        self.postRequest(url: url, params: params, apiCompletion: { (responseValue, error) in
            if error == nil {
                let forceUpdate = Mapper<ForceUpdate>().map(JSON: responseValue as! [String : Any])
                apiCompletion(forceUpdate , nil)
            } else {
                apiCompletion(nil , error)
            }
        })
    }
    
    // MARK: - Search
    
    func fetchSearchResults(with query: String, apiCompletion: @escaping (_ responseValue: SearchResult?, _ error: String?) -> Void) {
        let url = baseURL + "GetSearchResults"
        
        let params = ["UserID" : User.currentUser?.userID! as AnyObject,
                      "SearchString" : query] as [String : AnyObject]
        
        self.postRequest(url: url, params: params, apiCompletion: { (responseValue, error) in
            if error == nil {
                let responseValueArray = responseValue as! [Any]
                
               let searchResult = Mapper<SearchResult>().map(JSON: responseValueArray.first as! [String : Any])
                apiCompletion(searchResult , nil)
            } else {
                apiCompletion(nil , error)
            }
        })
    }
    
    func fetchTrendingData(apiCompletion: @escaping (_ responseValue: SearchResult?, _ error: String?) -> Void) {
        let url = baseURL + "GetTrendingData"
        
        let params = ["Id" : User.currentUser?.userID! as AnyObject] as [String : AnyObject]
        
        self.postRequest(url: url, params: params, apiCompletion: { (responseValue, error) in
            if error == nil {
                let responseValueArray = responseValue as! [Any]
                
                let searchResult = Mapper<SearchResult>().map(JSON: responseValueArray.first as! [String : Any])
                apiCompletion(searchResult , nil)
            } else {
                apiCompletion(nil , error)
            }
        })
    }
    
    // MARK: - Save
    func fetchUserSavedRecipies(withStartIndex startIndex: Int, count: Int, apiCompletion: @escaping (_ responseValue: [Recipe]?, _ error: String?) -> Void) {
        let url = baseURL + "GetUserSavedRecipies"
        
        let params = ["userID" : User.currentUser?.userID! as AnyObject,
                      "Index" : startIndex,
                      "Count" : count]  as [String : AnyObject]
        
        self.postRequest(url: url, params: params, apiCompletion: { (responseValue, error) in
            if error == nil {
                let posts = Mapper<Recipe>().mapArray(JSONArray:responseValue as! [[String : Any]])
                apiCompletion(posts , nil)
            } else {
                apiCompletion(nil , error)
            }
        })
    }
    
    func savePost(with post:Recipe, apiCompletion: @escaping (_ responseValue: String?, _ error: String?) -> Void) {

        let url = baseURL + ((post.isSaved) ? "UnSaveAPost" : "SaveAPost")
        
        let params = ["RecipeID" : post.postID!,
                      "UserID" : User.currentUser?.userID! as AnyObject]  as [String : AnyObject]
        
        self.postRequest(url: url, params: params, apiCompletion: { (responseValue, error) in
            if error == nil {
                apiCompletion(responseValue as? String , nil)
            } else {
                apiCompletion(nil , error)
            }
        })
    }
    
    // MARK: - Like
    
    func fetchLikes(for post:Recipe, apiCompletion: @escaping (_ responseValue: [User]?, _ error: String?) -> Void) {
        let url = baseURL + "GetLikes"
        
        let params = ["RecipeID" : post.postID!,
                      "RequestingUserID" : User.currentUser?.userID! as AnyObject]  as [String : AnyObject]
       
        self.postRequest(url: url, params: params, apiCompletion: { (responseValue, error) in
            if error == nil {
                let users =  Mapper<User>().mapArray(JSONArray:responseValue as! [[String : Any]])
                apiCompletion(users , nil)
            } else {
                apiCompletion(nil , error)
            }
        })
    }
    
    func fetchLikePost(for user: User, startIndex: Int, count: Int, apiCompletion: @escaping (_ responseValue: [Recipe]?, _ error: String?) -> Void) {
        let url = baseURL + "GetTheLikedOfuser"
        
        let params = ["Id" : user.userID!,
                      "RequestingUserID" : User.currentUser?.userID! as AnyObject,
                      "Index" : startIndex,
                      "Count" : count]  as [String : AnyObject]
        
        self.postRequest(url: url, params: params, apiCompletion: { (responseValue, error) in
            if error == nil {
                let posts =  Mapper<Recipe>().mapArray(JSONArray:responseValue as! [[String : Any]])
                apiCompletion(posts , nil)
            } else {
                apiCompletion(nil , error)
            }
        })
    }
    
    // Will use the very service for unlike
    // just make sure that the the post in the arugument has updated value for isLiked
    func likePost(with post:Recipe, apiCompletion: @escaping (_ responseValue: String?, _ error: String?) -> Void) {
        let url = baseURL + ((post.isLiked) ? "DisLikeAPost" : "LikeAPost")
        
        let params = ["RecipeID" : post.postID!,
                      "UserID" : User.currentUser?.userID! as AnyObject]  as [String : AnyObject]
        
        self.postRequest(url: url, params: params, apiCompletion: { (responseValue, error) in
            if error == nil {
                apiCompletion(responseValue as? String , nil)
            } else {
                apiCompletion(nil , error)
            }
        })
    }
    
    // MARK: - Comments
    
    func addComment(with comment:Comment, apiCompletion: @escaping (_ responseValue: String?, _ error: String?) -> Void) {
        let url = baseURL + "AddComment"
        
        let params = ["Discription" : comment.content!,
                      "RecipeID" : comment.postID!,
                      "UserID" : User.currentUser?.userID! as AnyObject]  as [String : AnyObject]
        
        self.postRequest(url: url, params: params, apiCompletion: { (responseValue, error) in
            if error == nil {
                apiCompletion(responseValue as? String , nil)
            } else {
                apiCompletion(nil , error)
            }
        })
    }
    
    func fetchComments(for post: Recipe, startIndex: Int, count: Int, apiCompletion: @escaping (_ responseValue: [Comment]?, _ error: String?) -> Void) {
        let url = baseURL + "GetTheLikedOfuser"
        
        let params = ["RecipeID" : post.postID!,
                      "RequestingUserID" : User.currentUser?.userID! as AnyObject,
                      "Index" : startIndex,
                      "Count" : count]  as [String : AnyObject]
        
        self.postRequest(url: url, params: params, apiCompletion: { (responseValue, error) in
            if error == nil {
                let commnets =  Mapper<Comment>().mapArray(JSONArray:responseValue as! [[String : Any]])
                apiCompletion(commnets , nil)
            } else {
                apiCompletion(nil , error)
            }
        })
    }
    
    // MARK: - Share
    
    func sharePost(post: Recipe, apiCompletion: @escaping (_ responseValue: String?, _ error: String?) -> Void) {
        let url = baseURL + "ShareAPost"
        
        let params = ["RecipeID" : post.postID!,
                      "UserID" : User.currentUser?.userID! as AnyObject]  as [String : AnyObject]
        
        self.postRequest(url: url, params: params, apiCompletion: { (responseValue, error) in
            if error == nil {
                apiCompletion(responseValue as? String , nil)
            } else {
                apiCompletion(nil , error)
            }
        })
    }
    
    func sharePost(post: Recipe, with user: User, apiCompletion: @escaping (_ responseValue: String?, _ error: String?) -> Void) {
        let url = baseURL + "ShareAPost"
        
        let params = ["RecipeID" : post.postID!,
                      "SharedToUserIdFK" : user.userID!,
                      "SharedByUserIdFK" : User.currentUser?.userID! as AnyObject]  as [String : AnyObject]
        
        self.postRequest(url: url, params: params, apiCompletion: { (responseValue, error) in
            if error == nil {
                apiCompletion(responseValue as? String , nil)
            } else {
                apiCompletion(nil , error)
            }
        })
    }
    
    func fetchSharePosts(apiCompletion: @escaping (_ responseValue: [Recipe]?, _ error: String?) -> Void) {
        let url = baseURL + "GetAllSharedToUser"
        
        let params = ["UserId" : User.currentUser?.userID! as AnyObject]  as [String : AnyObject]
        
        self.postRequest(url: url, params: params, apiCompletion: { (responseValue, error) in
            if error == nil {
                let posts =  Mapper<Recipe>().mapArray(JSONArray:responseValue as! [[String : Any]])
                apiCompletion(posts , nil)
            } else {
                apiCompletion(nil , error)
            }
        })
    }
    
    
    // MARK: - Feedback
    
    func submitFeedback(feedback: Feedback, apiCompletion: @escaping (_ responseValue: String?, _ error: String?) -> Void) {
        let url = baseURL + "SubmitAfeedback"
        let params = ["SuggestionContent" : feedback.content!,
                      "UserID" : User.currentUser?.userID! as AnyObject]  as [String : AnyObject]
        
        self.postRequest(url: url, params: params, apiCompletion: { (responseValue, error) in
            if error == nil {
                apiCompletion(responseValue as? String , nil)
            } else {
                apiCompletion(nil , error)
            }
        })
    }
    
    
    // MARK: - Ingrdiants
    
    func fetchIngrediantsSuggestion(with query: String, apiCompletion: @escaping (_ responseValue: [Ingrediant]?, _ error: String?) -> Void) {
        let url = baseURL + "GetIngredientSuggestions"
        
        let params = ["SearchString" : query]  as [String : AnyObject]
        
        self.postRequest(url: url, params: params, apiCompletion: { (responseValue, error) in
            if error == nil {
                let ingrediants =  Mapper<Ingrediant>().mapArray(JSONArray:responseValue as! [[String : Any]])
                apiCompletion(ingrediants , nil)
            } else {
                apiCompletion(nil , error)
            }
        })
    }
    
    func fetchReletedIngrediants(for ingrediant: Ingrediant, apiCompletion: @escaping (_ responseValue: [Ingrediant]?, _ error: String?) -> Void) {
        let url = baseURL + "GetRelatedIngrediants"
        
        let params = ["Name" : ingrediant.name!]  as [String : AnyObject]
        
        self.postRequest(url: url, params: params, apiCompletion: { (responseValue, error) in
            if error == nil {
                let ingrediants =  Mapper<Ingrediant>().mapArray(JSONArray:responseValue as! [[String : Any]])
                apiCompletion(ingrediants , nil)
            } else {
                apiCompletion(nil , error)
            }
        })
    }
    
    
    // MARK: - Notification
    
    func fetchNotifications(for user: User, startIndex: Int, count: Int, apiCompletion: @escaping (_ responseValue: [Notification]?, _ error: String?) -> Void) {
        let url = baseURL + "GetAllNotifications"
        
        let params = ["userID" : user.userID!,
                      "Index" : startIndex,
                      "Count" : count]  as [String : AnyObject]
        
        self.postRequest(url: url, params: params, apiCompletion: { (responseValue, error) in
            if error == nil {
                let notifications =  Mapper<Notification>().mapArray(JSONArray:responseValue as! [[String : Any]])
                apiCompletion(notifications , nil)
            } else {
                apiCompletion(nil , error)
            }
        })
    }
    
    func fetchUnreadNotificationCount(for user: User, apiCompletion: @escaping (_ responseValue: Int?, _ error: String?) -> Void) {
        let url = baseURL + "GetUnreadNotificationCount"
        
        let params = ["userID" : user.userID!]  as [String : AnyObject]
        
        self.postRequest(url: url, params: params, apiCompletion: { (responseValue, error) in
            if error == nil {
                if let count = responseValue!["notificationsCount"] as? Int {
                    apiCompletion(count, nil)
                } else {
                     apiCompletion(nil , error)
                }
            } else {
                apiCompletion(nil , error)
            }
        })
    }
    
    func registerForPushNotification(withToken Token: String, apiCompletion: @escaping (_ responseValue: Bool, _ error: String?) -> Void) {
        let url = baseURL + "RegisterPushNotificationTokenForIOS"
        
        let deciveID = UIDevice.current.identifierForVendor!.uuidString
        
        let params = [
            "userID" : User.currentUser?.userID! as AnyObject,
            "deviceToken" : Token as AnyObject,
            "deviceID" : deciveID as AnyObject,
            "notificationCategory" : "" as AnyObject
            
            ] as [String : AnyObject]
        
        self.postRequest(url: url, params: params, apiCompletion: { (responseValue, error) in
            if error == nil {
                apiCompletion(true , nil)
            } else {
                apiCompletion(false , error)
            }
        })
    }
    
}

extension APIManager {
    
    private var defaultHeaders: [String: String] {
        return [
            "Content-Type": "application/json"
        ]
    }
    
    private func printRequest(requestType: RequestType, requestURL: String, value: AnyObject) {
        print(requestType.rawValue + " - " + requestURL + " : ")
        print(value)
    }
    
    private func printRequest(requestType: RequestType, requestURL: String) {
        print(requestType.rawValue + " - " + requestURL)
    }
    
    private func postRequest(url: String,  params: [String: AnyObject], apiCompletion: @escaping (_ responseValue: AnyObject?, _ error: String?) -> Void) {
        
        let paramsDict = dictionaryWithoutEmptyValues(dict: params)
        
        self.printRequest(requestType: RequestType.Request,
                          requestURL: url,
                          value: paramsDict as AnyObject)
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        Alamofire.request(url, method: .post, parameters: paramsDict, encoding: JSONEncoding.prettyPrinted, headers: defaultHeaders)
            .responseJSON { (response) in
                
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                
                guard let value = response.result.value else {
                    print("CONNECTION ERROR: \(response)")
                    apiCompletion(nil, response.error?.localizedDescription)
                    return
                }
                
                self.printRequest(requestType: RequestType.Response,
                                  requestURL: url,
                                  value: value as AnyObject)
                
                
                if let status = (value as AnyObject)["status"] as? Int {
                    if status == 200 {
                        
                        if let data = (value as AnyObject)["data"], type(of: data!) != NSNull.self   {
                            apiCompletion(data as AnyObject, nil)
                        } else {
                            let successMessage = (value as AnyObject)["message"] as? String
                            apiCompletion(successMessage as AnyObject, nil)
                        }
                    } else {
                        if status == Constants.bannedUser {
                            self.appDelegate?.logoutUser()
                        } else {
                            let message = (value as AnyObject)["message"] as? String
                            apiCompletion(nil, message)
                        }
                    }
                    
                }  else {
                    apiCompletion(nil, "Something went wrong")
                }
        }
    }
    
     private func postRequest(url: String,  params: [AnyObject], apiCompletion: @escaping (_ responseValue: AnyObject?, _ error: String?) -> Void) {
        
        var request = URLRequest(url: URL(string:url)!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = try! JSONSerialization.data(withJSONObject: params)
        
        self.printRequest(requestType: RequestType.Request,
                          requestURL: url,
                          value: params as AnyObject)
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        Alamofire.request(request)
            .responseJSON { response in
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                
                guard let value = response.result.value else {
                    print("CONNECTION ERROR: \(response)")
                    apiCompletion(nil, response.error?.localizedDescription)
                    return
                }
                
                self.printRequest(requestType: RequestType.Response,
                                  requestURL: url,
                                  value: value as AnyObject)
                
                
                if let status = (value as AnyObject)["status"] as? Int {
                    if status == 200 {
                        
                        if let data = (value as AnyObject)["data"], type(of: data!) != NSNull.self   {
                            apiCompletion(data as AnyObject, nil)
                        } else {
                            let successMessage = (value as AnyObject)["message"] as? String
                            apiCompletion(successMessage as AnyObject, nil)
                        }
                    } else {
                        let message = (value as AnyObject)["message"] as? String
                        apiCompletion(nil, message)
                    }
                    
                }  else {
                    apiCompletion(nil, "Something went wrong")
                }
        }
        
    }
    
    private func postMultiPartRequest(url: String, params: [String: AnyObject], imageData: Data? , imageNameParam :String, fileName: String?, apiCompletion: @escaping (_ responseValue: AnyObject?, _ error: String?) -> Void, progressHandler:@escaping (Progress) -> Void) {
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        //var userID = User.currentUser?.userID
        
        self.printRequest(requestType: RequestType.Request,
                          requestURL: url,
                          value: params as AnyObject)
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            if let data = imageData   {
                multipartFormData.append(data, withName: imageNameParam, fileName: fileName ?? "image.jpeg", mimeType: "image/jpeg")
            }
            for (key, value) in params {
                if let value = value as? String {
                    multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                }
            }
        }, to:url)
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    progressHandler(progress)
                })
                
                upload.responseJSON { response in
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    
                    guard let value = response.result.value else {
                        print("CONNECTION ERROR: \(response)")
                        apiCompletion(nil, response.error?.localizedDescription)
                        return
                    }
                    
                    self.printRequest(requestType: RequestType.Response,
                                      requestURL: url,
                                      value: value as AnyObject)
                    
                    
                    if let status = (value as AnyObject)["status"] as? Int {
                        if status == 200 {
                            
                            if let data = (value as AnyObject)["data"], type(of: data!) != NSNull.self   {
                                apiCompletion(data as AnyObject, nil)
                            } else {
                                let successMessage = (value as AnyObject)["message"] as? String
                                apiCompletion(successMessage as AnyObject, nil)
                            }
                        } else {
                            let message = (value as AnyObject)["message"] as? String
                            apiCompletion(nil, message)
                        }
                        
                    }  else {
                        apiCompletion(nil, "Something went wrong")
                    }
                }
                break
                
            case .failure(let error):
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                print("CONNECTION ERROR: \(error)")
                apiCompletion(nil, error.localizedDescription)
            
                break
            }
        }
        
    }
    
    private func getRequest(url: String, apiCompletion: @escaping (_ responseValue: AnyObject?, _ error: String?) -> Void) {
        
        self.printRequest(requestType: RequestType.Request,
                          requestURL: url)
        
        Alamofire
            .request(url, method: .get)
            .responseJSON { (response) in
                
                guard let value = response.result.value else {
                    apiCompletion(nil, "error_connection")
                    return
                }
                
                self.printRequest(requestType: RequestType.Response,
                                  requestURL: url,
                                  value: value as AnyObject)
                
                if response.result.isSuccess {
                    if response.response?.statusCode == 200 {
                        
                        apiCompletion(value as AnyObject, nil)
                        return
                        
                    }  else {
                        
                        if let errorDetail = (value as AnyObject)["code"] as? String {
                            apiCompletion(value as AnyObject, errorDetail)
                        }  else {
                            apiCompletion(value as AnyObject, "error_default")
                        }
                        return
                    }
                    
                } else   if response.result.isFailure {
                    
                    apiCompletion(value as AnyObject, "error_connection")
                    
                    return
                }
        }
    }
    
     private func dictionaryWithoutEmptyValues(dict: [String: AnyObject]) -> [String: AnyObject] {
        var newDictionary = [String: AnyObject]()
        for (key, value) in dict {
            if let val = value as? String, val.isEmpty {
                // ...
            } else {
                newDictionary[key] = value
            }
        }
        return newDictionary
    }
    
}
