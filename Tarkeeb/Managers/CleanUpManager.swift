//
//  CleanUpManager.swift
//  Tarkeeb
//
//  Created by Razi Tiwana on 05/10/2018.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import UIKit

class CleanUpManager {
    
    // MARK: - Singleton
    static let sharedManager = CleanUpManager()

    let appDelegate = UIApplication.shared.delegate as? AppDelegate
    
    
    // MARK: - Clean Up
    
    func cleanUp() {
        
    }
    
    private func clearDefaults() {
        appDelegate!.logoutUser()
    }
    
    private func cleanUp(onDate date: String) {
        let isAlreadyExecuted: Bool = UserDefaults.standard.bool(forKey: date)
        if !isAlreadyExecuted {
            UserDefaults.standard.set(true, forKey: date)
            UserDefaults.standard.synchronize()
            clearDefaults()
        }
    }
}
