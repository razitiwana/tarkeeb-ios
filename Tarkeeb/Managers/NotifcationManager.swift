//
//  NotifcationManager.swift
//  Tarkeeb
//
//  Created by Razi Tiwana on 05/10/2018.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import UIKit

class NotificationManager {
    
    // MARK: - Singleton
    static let sharedManager = NotificationManager()
    
    let appDelegate = UIApplication.shared.delegate as? AppDelegate
    
    
    // MARK: - Public Methods
    
    func fecthUnReadNotificationCount() {
        if let user = User.currentUser {
            APIManager.sharedManager.fetchUnreadNotificationCount(for: user) { (count, error) in
                
                if let count = count {
                    var user = User.currentUser!
                    user.unreadNotificationCount = String(count)
                    
                    if count == 0 {
                        user.unreadNotificationCount = nil
                    }
                    
                    User.currentUser = user
                    NotificationCenter.default.post(name: .notificationCountUpdated, object: nil)
                }
            }
        }
    }
    
    func registerTokenWithServer(token: String) {
        var shouldSentTokenToServer = false
        
        let tokenKey = "tokenKey"
        
        let oldToken = UserDefaults.standard.string(forKey: tokenKey)
        
        if oldToken == nil || oldToken != token {
            shouldSentTokenToServer = true
        }
        
        if shouldSentTokenToServer {
            APIManager.sharedManager.registerForPushNotification(withToken: token) { (status, error) in
                
                if error == nil {
                    UserDefaults.standard.set(token, forKey: tokenKey)
                }
            }
        }
    }
    
}
