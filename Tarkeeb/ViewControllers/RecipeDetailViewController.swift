 //
//  RecipeDetailViewController.swift
//  Tarkeeb
//
//  Created by Razi Tiwana on 08/09/2018.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import UIKit

enum RecipeDetailSections : Int  {
    case details
    case ingrediants
    case instructions
    static var count :Int {return RecipeDetailSections.instructions.hashValue + 1}
}

class RecipeDetailViewController: ViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var post : Recipe?
    
    private let refreshControl = UIRefreshControl()
    
    // MARK: - ViewController LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureTableView()
//        configurePost()
    
        showProgressHud()
        fetchRecipieDetail()
    }
    
    // MARK: - Private Methods

    func configurePost() {
        self.title = post?.title
    }
    
    func configureTableView() {
        // To remove the extra cells
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = UIColor.tableViewBackgroundColor()
        
        tableView.register(UINib(nibName: "RecipeDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "RecipeDetailTableViewCell")
        
        tableView.register(UINib(nibName: "RecipeIngrediantTableViewCell", bundle: nil), forCellReuseIdentifier: "RecipeIngrediantTableViewCell")
        
        tableView.register(UINib(nibName: "RecipeInstructionTableViewCell", bundle: nil), forCellReuseIdentifier: "RecipeInstructionTableViewCell")
        
        tableView.register(UINib(nibName: "NoRecordsFoundTableViewCell", bundle: nil), forCellReuseIdentifier: "NoRecordsFoundTableViewCell")
        
        
        // NO need for pull to refresh
//        // Setup refresh Control
//        tableView.refreshControl = refreshControl
//        refreshControl.addTarget(self, action: #selector(fetchRecipieDetail), for: .valueChanged)
        
        // Other wise the scroll inset are changeed when reloading data
        tableView.estimatedRowHeight = CGFloat(Constants.estimatedRecipeCellSize)
    }
    
    @objc func fetchRecipieDetail() {
        
        APIManager.sharedManager.fetchDetail(for: post!) { (post, error) in
            self.refreshControl.endRefreshing()
            self.dismissProgressHud()
            
            if error == nil {
                self.post = post
                self.tableView.reloadData()
                
            } else {
                self.showAlert(text: error!)
            }
        }
    }

}

extension RecipeDetailViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return RecipeDetailSections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var numberOfRows = 0
      
        for view in tableView.subviews {
            view.removeFromSuperview()
        }
        
        switch RecipeDetailSections(rawValue: section)! {
        case .details:
            numberOfRows = 1
            break
        case .ingrediants:
            numberOfRows = (post?.ingrediants?.count) ?? 0
            if post?.ingrediants?.count == 0 {
                numberOfRows = 1
            }
            break
        case .instructions:
            numberOfRows = (post?.instructions?.count) ?? 0
            if post?.instructions?.count == 0 {
                numberOfRows = 1
            }
            break
        }
       
        return numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: UITableViewCell?
        
        switch RecipeDetailSections(rawValue: indexPath.section)! {
        case .details:
            let detailCell = tableView.dequeueReusableCell(withIdentifier: "RecipeDetailTableViewCell") as! RecipeDetailTableViewCell
            
            detailCell.delegate = self
            detailCell.configureCell(with: post!)
            
            cell = detailCell
            
            break
        case .ingrediants:
            
            if (post?.ingrediants?.count)! > 0 {
                let ingrediantCell = tableView.dequeueReusableCell(withIdentifier: "RecipeIngrediantTableViewCell") as! RecipeIngrediantTableViewCell
                let ingrediant = post?.ingrediants![indexPath.row]
                
                ingrediantCell.configureCell(with: ingrediant!)
                
                let isFirstCell = indexPath.row == 0
                let isLastCell = indexPath.row == (post?.ingrediants?.count)! - 1
                
                ingrediantCell.defaultStateForBorders()
                
                if isFirstCell && isLastCell {
                    ingrediantCell.topBottomRounded()
                } else if isFirstCell {
                    ingrediantCell.topRounded()
                } else if isLastCell {
                    ingrediantCell.bottomRounded()
                }
                
                cell = ingrediantCell
            }
            else {
                let ingrediantCell = tableView.dequeueReusableCell(withIdentifier: "NoRecordsFoundTableViewCell") as! NoRecordsFoundTableViewCell
                ingrediantCell.descriptionLabel.text = "No ingredients added"
                ingrediantCell.containerView?.roundVeryLittle()
                ingrediantCell.contentView.backgroundColor = UIColor.tableViewBackgroundColor()
                cell = ingrediantCell
            }
           
            break
        case .instructions:
            
            if (post?.instructions?.count)! > 0 {
                
                let instructionCell = tableView.dequeueReusableCell(withIdentifier: "RecipeInstructionTableViewCell") as! RecipeInstructionTableViewCell
                let instruction = post?.instructions![indexPath.row]
                instructionCell.configureCell(with: instruction!)
                instructionCell.numberLabel.text = (indexPath.row + 1).toString
                
                cell = instructionCell
            }
            else {
                let instructionCell = tableView.dequeueReusableCell(withIdentifier: "NoRecordsFoundTableViewCell") as! NoRecordsFoundTableViewCell
                instructionCell.descriptionLabel.text = "No instructions added"
                instructionCell.containerView?.roundVeryLittle()
                instructionCell.contentView.backgroundColor = UIColor.tableViewBackgroundColor()
                cell = instructionCell
            }
            
            break
        }
    
        cell!.selectionStyle = .none
    
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return (section == 0) ? 0 : 40.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var sectionHeaderTitle: String?
        
        switch RecipeDetailSections(rawValue: section)! {
        case .details:
            sectionHeaderTitle = nil
          
        case .ingrediants:
            sectionHeaderTitle = (post?.ingrediants != nil )  ? "Ingrediants" : nil
        
            break
        case .instructions:
            sectionHeaderTitle = (post?.instructions != nil ) ? "Instructions" : nil
            break
        }
        
        if sectionHeaderTitle == nil { return nil }
        
        let label = UILabel(frame: CGRect(x: 16, y: 0, width: tableView.frame.size.width, height: 40.0))
        label.text = sectionHeaderTitle

        let containerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 40.0))
        
        containerView.backgroundColor = UIColor.tableViewBackgroundColor()
        containerView.addSubview(label)
        
        return containerView
    }
    
}

extension RecipeDetailViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

}

extension RecipeDetailViewController: RecipeTableViewCellDelegate {
    
    func like(post: Recipe) {
        let oldState = post.isLiked
        let oldnumberOfLikes = post.numberOflikes
        
        APIManager.sharedManager.likePost(with: post) { (sucessMessage, error) in
            if error != nil {
                self.post?.isLiked = oldState
                self.post?.numberOflikes = oldnumberOfLikes
                self.tableView.reloadData()
            } else {
                
            }
        }
        
        self.post?.isLiked = !post.isLiked
        self.post?.numberOflikes += oldState ? -1 : 1
        
        tableView.reloadData()
    }
    
    func share(post: Recipe) {
        showAlert(text: "Comming Soon!")
    }
    
    func save(post: Recipe) {
        //        showAlert(text: "Comming Soon!")
        
        let oldState = post.isLiked
        
        APIManager.sharedManager.savePost(with: post) { (sucessMessage, error) in
            if error != nil {
                self.post?.isSaved = oldState
                self.tableView.reloadData()
            } else {
                 self.showDoneProgressHud()
            }
        }
        
        self.post?.isSaved = !post.isSaved
        
        tableView.reloadData()
    }
    
    func openDetail(for user: User) {
        let viewController = StoryboardRouter.profileViewController()
        viewController.user = user
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
}
