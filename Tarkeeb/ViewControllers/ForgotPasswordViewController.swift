//
//  ForgotPasswordViewController.swift
//  Tarkeeb
//
//  Created by Osama Azmat Khan on 10/5/18.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import UIKit
import TransitionButton

class ForgotPasswordViewController: ViewController {
    
    
    // MARK: - Outlets
    
    @IBOutlet weak var informationLabel: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var resetPasswordButton: TransitionButton!
    @IBOutlet weak var cancelView: UIView!
    
    
    // MARK: - ViewController LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        confirgureView()
        addCancelGesture()
    }
    
    
    // MARK: - Private Methods
    
    func confirgureView(){
        emailTextField.defaultTextField()
        cancelView.roundVeryLittle(cornerRadius: 8)
        cancelView.addBorders()
    }
    
    func addCancelGesture() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onClickCancel(_:)))
        cancelView.addGestureRecognizer(tapGesture)
        cancelView.isUserInteractionEnabled = true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func validate() -> Bool{
        
        var validation = true
        if emailTextField.text! == ""{
            emailTextField.errorAnimation()
            validation = false
        }
        
        return validation
    }
    
    
    // MARK: - Action Methods
    
    @IBAction func onClickCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickResetPassword(_ sender: Any) {
        
        resetPasswordButton.startAnimation()
        if validate(){
            APIManager.sharedManager.resetPassword(with: emailTextField.text!) { (responseValue, error) in
                if error == nil{
                    
                    self.showAlert(text: responseValue!, completion: { (alertAction) in
                        self.resetPasswordButton.stopAnimation(animationStyle: .expand, completion: {
                            self.dismiss(animated: true, completion: nil)
                        })
                    })
                    
                  
                }
                else{
                    self.resetPasswordButton.stopAnimation(animationStyle: .shake, completion: {
                        self.showAlert(text: error!)
                    })
                }
            }
        } else {
              resetPasswordButton.stopAnimation(animationStyle: .normal, completion: nil)
        }
       
    }
    
}
