//
//  SettingsViewController.swift
//  Tarkeeb
//
//  Created by Razi Tiwana on 29/09/2018.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import UIKit

enum SettingsViewControllerSections: Int {
    case appVersion
    case links
    case invite
    case feedback
    case logout
    static var count :Int {return SettingsViewControllerSections.logout.hashValue + 1}
    
    // Not used unless we set titles for sections
    var sectionTitle: String {
        get {
            switch self {
            case .appVersion:
                return ""
            case .links:
                return ""
            case .invite:
                return ""
            case .feedback:
                return ""
            case .logout:
                return ""
            }
        }
    }
}

enum LinksSection: Int  {
    case aboutUs
    case terms
    case privacy
    case blogs
    static var count :Int {return LinksSection.blogs.hashValue + 1}
}

enum LogoutSection {
    case logout
}

class SettingsViewController: ViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Options"
        
        configureTableView()
    }
    
    func configureTableView() {
        // To remove the extra cells
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = UIColor.tableViewBackgroundColor()
    }
    
    func logout() {
        let alertController = UIAlertController(title: "Log out from " + (User.currentUser?.userName)! + "?", message: "", preferredStyle: .actionSheet)
        
        let logoutAction = UIAlertAction(title: "Log Out" , style: .destructive) { action in
            self.appDelegate?.logoutUser()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alertController.addAction(logoutAction)
        alertController.addAction(cancelAction)
        alertController.view.tintColor = .black
        
        present(alertController, animated: true, completion: nil)
    }
    
}

extension SettingsViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return SettingsViewControllerSections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var numberOfRows = 0
        
        switch SettingsViewControllerSections(rawValue: section)! {
            
        case .appVersion :
            numberOfRows = 1
            break
        case .links:
            numberOfRows = LinksSection.count
            break
        case .invite:
            numberOfRows = 1
            break
        case .feedback:
            numberOfRows = 1
            break
        case .logout:
            numberOfRows = 1
            break
        }
        return numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell.init(style: .value1, reuseIdentifier: "cell")
        
        switch SettingsViewControllerSections(rawValue: indexPath.section)! {
        case .appVersion:
            cell.textLabel?.text = "App Version"
            cell.detailTextLabel?.text = UpdateManager.sharedManager.currentVersion
            break
        case .links:
            switch LinksSection(rawValue: indexPath.row)! {
            case .aboutUs:
                cell.textLabel?.text = "About Us"
                break
            case .terms:
                cell.textLabel?.text = "Terms & Conditions"
                break;
            case .privacy:
                cell.textLabel?.text = "Privacy Policy"
                break;
            case .blogs:
                cell.textLabel?.text = "Blogs"
                break;
            }
            
            break
            
        case .invite:
            cell.textLabel?.text = "Invite Friends to join Beta!"
            cell.textLabel?.textColor = UIColor.appThemeColor()
            break
            
        case .feedback:
            cell.textLabel?.text = "Submit Feedback"
            break
            
        case .logout:
            cell.textLabel?.text = "Log Out"
            cell.textLabel?.textColor = .red
            break
        }
        
        return cell
    }
    
}

extension SettingsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch SettingsViewControllerSections(rawValue: indexPath.section)! {
        case .appVersion:
            break
        case .links:
            switch LinksSection(rawValue: indexPath.row)! {
            case .aboutUs:
                openURL(url: Constants.aboutURL)
                break
            case .terms:
                openURL(url: Constants.termsURL)
                break;
            case .privacy:
                openURL(url: Constants.privacyPolicy)
                break;
            case .blogs:
                openURL(url: Constants.blogs)
                break
            }
            
        case .invite:
            shareLink(url: Constants.inviteLink)
            
            break;
        case .feedback:
            let viewController = StoryboardRouter.feedbackNavigationViewController()
            self.present(viewController, animated: true)
            break
            
        case .logout:
            logout()
            break
        }
    }
    
    //    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    //        var height: CGFloat = 0
    //
    //        switch SettingsViewControllerSections(rawValue: section)! {
    //        case .links:
    //            break
    //        case .invite:
    //            height = 0.01
    //            break;
    //        case .feedback:
    //            break
    //        case .logout:
    //            break
    //        }
    //
    //        return height
    //    }
    
}
