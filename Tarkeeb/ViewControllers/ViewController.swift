//
//  ViewController.swift
//  Tarkeeb
//
//  Created by Razi Tiwana on 05/09/2018.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import UIKit
import MBProgressHUD
import PKHUD

class ViewController: UIViewController {

    var progressHUD : MBProgressHUD?
    var mbProgressHUD : MBProgressHUD?
    
    var isRootViewController: Bool {
        get {
            if let navigationController = navigationController {
                return navigationController.viewControllers.first == self
            }
            return false
        }
    }
    
    let appDelegate = UIApplication.shared.delegate as? AppDelegate
    
    // MARK: - ViewController Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        configureNavigationBar()
        
        addOnTapGesture()
        
        // Enable swipe back when no navigation bar
        
        if (navigationController?.viewControllers.count == 1) {
            navigationController?.interactivePopGestureRecognizer?.delegate = self
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Configurations
    
    func configureNavigationBar() {
    
        if let navigationController = navigationController {
            let textAttributes = [NSAttributedStringKey.foregroundColor:UIColor.appThemeColor()]
            navigationController.navigationBar.titleTextAttributes = textAttributes
            
            let backButton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            navigationItem.backBarButtonItem = backButton
        }
    }

    func clearNavigationBar()  {
        if let navigationController = navigationController  {
            navigationController.navigationBar.setBackgroundImage(UIImage(), for: .default)
            navigationController.navigationBar.shadowImage = UIImage()
            navigationController.navigationBar.isTranslucent = true
            
        }
    }
    
    func addOnTapGesture()  {
        
        for _ in view.allSubViewsOf(type: UITextField.self) {
            // If a text filed present only then add the on tap gesture
            let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
            view.addGestureRecognizer(tap)
            break;
        }
        
        for _ in view.allSubViewsOf(type: UITextView.self) {
            // If a text filed present only then add the on tap gesture
            let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
            view.addGestureRecognizer(tap)
            break;
        }
    }
    
    
    
    // MARK: - Action Methods
    
    @objc func handleTap() {
        view.endEditing(false)
    }
    
    
    // MARK: - Custom Alert View -
    
    func showAlert(text: String) {
        let alertController = UIAlertController(title: "Tarkeeb", message:
            text, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showAlertAndDismissOnCompletion(text: String) {
        let alertController = UIAlertController(title: "Tarkeeb", message:
            text, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default,handler: popViewController))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showAlertAndDismissModallyOnCompletion(text: String) {
        let alertController = UIAlertController(title: "Tarkeeb", message:
            text, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default,handler: dismissViewController))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showAlert(text: String, completion: ((UIAlertAction) -> Swift.Void)? = nil) {
        let alertController = UIAlertController(title: "Tarkeeb", message:
            text, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: completion))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func scrollToTop() {
        for tableView in view.allSubViewsOf(type: UITableView.self) {
            
            if tableView.numberOfSections > 0, tableView.numberOfRows(inSection: 0) > 0 {
                let indexPath = IndexPath(row: 0, section: 0)
                tableView.scrollToRow(at: indexPath, at: .top, animated: true)
            }
        }
    }
    
    
    // MARK: - Navigation -
    
    func popViewController(alert: UIAlertAction!) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func dismissViewController(alert: UIAlertAction!) {
        self.dismiss(animated: true, completion: nil)
    }
    

    // MARK: - Progress Hud -
    
    func showProgressHud() {
        mbProgressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        mbProgressHUD?.mode = MBProgressHUDMode.indeterminate
    }
    
    func showProgressHud(title: String) {
        mbProgressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        mbProgressHUD?.mode = MBProgressHUDMode.indeterminate
        mbProgressHUD?.label.text = title
    }
    
    func showProgressHud(progress: Float) {
        if progressHUD == nil {
            progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
            progressHUD?.mode = .annularDeterminate
        }
        
        progressHUD?.progress = progress
    }
    
    func dismissProgressHud() {
        progressHUD?.hide(animated: true)
        mbProgressHUD?.hide(animated: true)
        
        progressHUD = nil
        mbProgressHUD = nil
    }
    
    func showDoneProgressHud() {
        return
//        PKHUD.sharedHUD.contentView = PKHUDSuccessView()
//
//        PKHUD.sharedHUD.show()
//        PKHUD.sharedHUD.hide(afterDelay: 2.0) { success in
//            // Completion Handler
//        }
    }
    
    
    // MARK: - LoadingView
    func loadingView() -> UIView {
        let indicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 2, width: 40, height: 44))
        indicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        indicator.startAnimating()
        return indicator
    }
    
    
    // MARK: - Misc
    
    func openURL(url: String) {
        UIApplication.shared.open(URL(string: url)!, options: [:], completionHandler: nil)
    }
    
    func shareLink(url: String) {
        
        let firstActivityItem = "Tarkeeb"
        let secondActivityItem : NSURL = NSURL(string: url)!
        // If you want to put an image
        let image : UIImage = UIImage(named: "ic_beta_icon")!
        
        let activityViewController : UIActivityViewController = UIActivityViewController(
            activityItems: [firstActivityItem, secondActivityItem, image], applicationActivities: nil)
        
        // This lines is for the popover you need to show in iPad
//        activityViewController.popoverPresentationController?.sourceView = ()
        
        // This line remove the arrow of the popover to show in iPad
        activityViewController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.any
        activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 150, y: 150, width: 0, height: 0)
        
        // Anything you want to exclude
        activityViewController.excludedActivityTypes = [
            UIActivityType.postToWeibo,
            UIActivityType.print,
            UIActivityType.assignToContact,
            UIActivityType.saveToCameraRoll,
            UIActivityType.addToReadingList,
            UIActivityType.postToFlickr,
            UIActivityType.postToVimeo,
            UIActivityType.postToTencentWeibo
        ]
        
        self.present(activityViewController, animated: true, completion: nil)
        
    }
}


extension ViewController: UIGestureRecognizerDelegate {
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        
        if(navigationController!.viewControllers.count > 1) {
            return true
        }
        return false
    }
    
}
