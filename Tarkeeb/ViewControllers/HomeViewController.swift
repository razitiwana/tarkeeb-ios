//
//  HomeViewController.swift
//  Tarkeeb
//
//  Created by Razi Tiwana on 05/09/2018.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import UIKit

class HomeViewController: ViewController {
    
    @IBOutlet weak var tableView: UITableView!

    private var posts:[Recipe] = [Recipe]()
    private let refreshControl = UIRefreshControl()
    
    var fetchingMore = false
    var moreRemaining = true
    
    var startIndex = 0
    var count = Constants.countToFetchAtOneTime
    
    var firstTimeLoad = true
    // MARK: - ViewController LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addLogo()
        configureTableView()
        
        fetchAllPost()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        // On tap to root if some controoler has navigation bar hidden it will remain hidden
        navigationController?.setNavigationBarHidden(false, animated: false)
        
        super.viewWillAppear(animated)
        
        // Refresh Spinner was stuck when there were no Posts on the..
        // ..home screen after unfollowing all the people.
        if firstTimeLoad {
            firstTimeLoad = false
            refreshControl.beginRefreshing()
        }
    }
    
    
    // MARK: - Action Methods
    
    @IBAction func addAction(_ sender: UIBarButtonItem) {
        let viewNavController = StoryboardRouter.createRecipeNavigationViewController()
        (viewNavController.viewControllers.first as! CreateRecipeViewController).delegate = self
    
        self.present(viewNavController, animated: true)
    }
    
    @IBAction func feebackAction(_ sender: UIBarButtonItem) {
        let viewController = StoryboardRouter.feedbackNavigationViewController()
        self.present(viewController, animated: true)
    }
    
    @objc func logoTapped() {
        scrollToTop()
    }
    
    @objc func refresh() {
        self.startIndex = 0
        self.moreRemaining = true
        self.fetchingMore = false
        fetchAllPost()
    }

    // MARK: - Private Methods
    
    func configureTableView() {
        // To remove the extra cells
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = UIColor.tableViewBackgroundColor()
        
        tableView.register(UINib(nibName: "RecipeTableViewCell", bundle: nil), forCellReuseIdentifier: "RecipeTableViewCell")
        tableView.register(UINib(nibName: "LoadingTableViewCell", bundle: nil), forCellReuseIdentifier: "LoadingTableViewCell")
        
        // Setup refresh Control
        tableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        
        // Other wise the scroll inset are changeed when reloading data
        tableView.estimatedRowHeight = CGFloat(Constants.estimatedRecipeCellSize)
    }

    func showNoRecordsLabel()
    {
        let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
        noDataLabel.text          = "No recipes found"
        noDataLabel.textColor     = UIColor.black
        noDataLabel.textAlignment = .center
        noDataLabel.tag = 10
        tableView.addSubview(noDataLabel)
    }
    func hideNoRecordsLabel()
    {
        for view in tableView.subviews {
            if view.tag == 10 {
                view.removeFromSuperview()
            }
        }
    }
    
    func addLogo() {
        let logo = UIImage(named: "ic_logo_urdu")
        let imageView = UIImageView(image:logo)
        imageView.contentMode = .scaleAspectFit
        imageView.frame = CGRect(x: imageView.frame.origin.x, y: imageView.frame.origin.y, width: 320, height: 40)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(logoTapped))
        imageView.addGestureRecognizer(tapGesture)
        imageView.isUserInteractionEnabled = true
        
        self.navigationItem.titleView = imageView
    }
    
    func fetchAllPost() {
        hideNoRecordsLabel()
        APIManager.sharedManager.fetchAllPosts(withStartIndex: startIndex, count: count) { (posts, error) in
            
            let isPullToRefresh = self.refreshControl.isRefreshing
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                  self.refreshControl.endRefreshing()
            }
            
            if error == nil, let posts = posts {
                if self.startIndex > 0 {
                    self.posts.append(contentsOf: posts)
                }
                else {
                    self.posts = posts
                }
                
                // If the count of the fectehed posts is lesss than the count send then that means there are no more items left to fetch from data base
                self.moreRemaining = (posts.count) >= self.count
                
                let previousContentOfset = self.tableView.contentOffset
                
                self.fetchingMore = false
                
                self.tableView.reloadData()
                
                // if isPullToRefresh then it would be stuck in the pulled state if the service responds quickly
                if posts.count > 0 && !isPullToRefresh {
                    self.tableView.setContentOffset(previousContentOfset, animated: true)
                }
                else {
                    if posts.count > 0 {
                        self.hideNoRecordsLabel()
                    }
                    else {
                        self.showNoRecordsLabel()
                    }
                }
            } else {
                self.showAlert(text: error!)
            }
            
            self.tableView.tableFooterView = nil
        }
    }
    
    func beginBatchFetch() {
        print("Fetching")
        
        fetchingMore = true
        self.startIndex += self.count
        
        self.fetchAllPost()
    }
}

extension HomeViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var numberOfSections = 0
        
        if self.posts.count > 0 || self.refreshControl.isRefreshing {
            numberOfSections = 1
            hideNoRecordsLabel()
        }
        
        return numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RecipeTableViewCell") as! RecipeTableViewCell
        
        let post = posts[indexPath.row]
        
        cell.configureCell(with: post)
        
//        cell.timeRequiredLabel.text = String.localizedStringWithFormat("%d/%d",indexPath.row + 1, posts.count)
        cell.delegate = self
        cell.selectionStyle = .none
        
        return cell
    }
}

extension HomeViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let post = posts[indexPath.row]
        
        let viewController = StoryboardRouter.recipeDetailViewController()
        viewController.post = post
        
        navigationController?.pushViewController(viewController, animated: true)
        
        posts[indexPath.row].postViews += 1
        tableView.reloadData()
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.posts.count >= count && self.moreRemaining {
            
            let currentOffset = scrollView.contentOffset.y;
            let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
            
            // if the there are elements less than size of tableview
            if (scrollView.contentSize.height > self.tableView.frame.size.height) {
                
                // In some cases the curent offset is greater than the maximum one which makes no sense, so a hack would be to add this check
                 // The 100 if the user pulls the table view it should still load
                if maximumOffset + 100 > currentOffset {
                    // Change 10.0 to adjust the distance from bottom
                    if (maximumOffset - currentOffset <= 10.0 ) {
                        if !self.fetchingMore {
                            tableView.tableFooterView = loadingView()
                            scrollView.setContentOffset(CGPoint(x: 0, y: maximumOffset + 100), animated: true)
                            
                            self.beginBatchFetch()
                        }
                    }}
            }
        }
    }
}

extension HomeViewController: RecipeTableViewCellDelegate {
    
    func like(post: Recipe) {
        
        for index in 0..<posts.count {
            if post.postID == posts[index].postID {
                let oldState = posts[index].isLiked
                let oldnumberOfLikes = posts[index].numberOflikes
                
                APIManager.sharedManager.likePost(with: posts[index]) { (sucessMessage, error) in
                    if error != nil {
                        self.posts[index].isLiked = oldState
                        self.posts[index].numberOflikes = oldnumberOfLikes
                        self.tableView.reloadData()
                    } else {
                        
                    }
                }
                
                posts[index].isLiked = !posts[index].isLiked
                
                posts[index].numberOflikes += oldState ? -1 : 1
                
                tableView.reloadData()
                break
            }
        }
    }
    
    func share(post: Recipe) {
        showAlert(text: "Comming Soon!")
    }
    
    func save(post: Recipe) {
        
        for index in 0..<posts.count {
            if post.postID == posts[index].postID {
                let oldState = posts[index].isSaved
                
                APIManager.sharedManager.savePost(with: posts[index]) { (sucessMessage, error) in
                    if error != nil {
                        self.posts[index].isSaved = oldState
                        self.tableView.reloadData()
                    } else {
                        self.showDoneProgressHud()
                    }
                }
                
                posts[index].isSaved = !posts[index].isSaved
                tableView.reloadData()
                break
            }
        }
    }
    
    func openDetail(for user: User) {
        let viewController = StoryboardRouter.profileViewController()
        viewController.user = user
        
        navigationController?.pushViewController(viewController, animated: true)
    }

}

extension HomeViewController: CreateRecipeViewControllerDelegate {
    func recipeAdded() {
        scrollToTop()
        refresh()
    }
    
}
