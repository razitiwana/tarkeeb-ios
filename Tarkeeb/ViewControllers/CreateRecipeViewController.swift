//
//  CreateRecipeViewController.swift
//  Tarkeeb
//
//  Created by Razi Tiwana on 15/10/2018.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import UIKit
import Photos
import TransitionButton

protocol CreateRecipeViewControllerDelegate {
    func recipeAdded()
}

enum CreateRecipeIngredientsSection: Int {
    case ingredients
    case addRow
    static var count :Int {return CreateRecipeIngredientsSection.addRow.hashValue + 1}
}

enum CreateRecipeInstructionsSection: Int {
    case instructions
    case addRow
    static var count :Int {return CreateRecipeInstructionsSection.addRow.hashValue + 1}
}

enum CreateRecipeMainInfoSections: Int {
    case addPicture
    case details
    static var count :Int {return CreateRecipeMainInfoSections.details.hashValue + 1}
}

enum CreateRecipeViewControllerSections: Int {
    case recipe
    case ingredients
    case instructions
    static var count :Int {return CreateRecipeViewControllerSections.instructions.hashValue + 1}
}

class CreateRecipeViewController: ViewController {

    
    // MARK: - Outlets
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var postRecipeTransitionButton: TransitionButton!
    
    
    // MARK: - Variables
    
    var mainSelectedSection = CreateRecipeViewControllerSections.recipe
    var recipeDetailsSection = CreateRecipeMainInfoSections.addPicture
    
    var recipeToPost: Recipe = Recipe()
    
    private var recipeIngredients:[Ingrediant] = []
    private var recipeInstructions:[Instruction] = []
    
    var instructionDictionaryArray = ""
    var ingredientDictionaryArray = ""
    
    var emptyIngredient: Ingrediant!
    var emptyInstruction: Instruction!
    
    private var sectionIndexToBeUpdated = 0
    private var dynamicHeight = 0.0
    
    var imagePicker = UIImagePickerController()
    var selectedImage: UIImage?
    
    var delegate: CreateRecipeViewControllerDelegate?
    
    // MARK: - ViewController LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        configureTableView()
        configureTransitionButtons()
       
        setupEmptyIngredientsAndInstructions()
        
        self.backgroundView.backgroundColor = UIColor.tableViewBackgroundColor()
        
        segmentedControl.removeBorder()
        segmentedControl.addUnderlineForSelectedSegment(numberofSegments: 3, width: UIScreen.main.bounds.width)
    }
    
    
    // MARK: - Private Methods
    
    func setupEmptyIngredientsAndInstructions() {
        emptyIngredient = Ingrediant()
        emptyIngredient?.name = ""
        emptyIngredient?.quantity = ""
        
        emptyInstruction = Instruction()
        emptyInstruction?.content = ""
        
        recipeInstructions.append(emptyInstruction!)
        recipeIngredients.append(emptyIngredient!)
    }
    
    func configureTableView() {
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = UIColor.tableViewBackgroundColor()
        
        tableView.register(UINib(nibName: "CreateRecipeInfoTableViewCell", bundle: nil), forCellReuseIdentifier: "CreateRecipeInfoTableViewCell")
        tableView.register(UINib(nibName: "CreateRecipeSingleLineTextTableViewCell", bundle: nil), forCellReuseIdentifier: "CreateRecipeSingleLineTextTableViewCell")
        tableView.register(UINib(nibName: "CreateRecipeMultiLineTextTableViewCell", bundle: nil), forCellReuseIdentifier: "CreateRecipeMultiLineTextTableViewCell")
        tableView.register(UINib(nibName: "AddRowTableViewCell", bundle: nil), forCellReuseIdentifier: "AddRowTableViewCell")
        
        tableView.estimatedRowHeight = 44.0
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    func configureTransitionButtons() {
        postRecipeTransitionButton.dropShadow()
        postRecipeTransitionButton.setTitle("Post Recipe", for: .normal)
        postRecipeTransitionButton.setTitleColor(UIColor.appThemeColor(), for: .normal)
        postRecipeTransitionButton.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: .medium)
        postRecipeTransitionButton.spinnerColor = UIColor.appThemeColor()
    }

    
    // MARK: - Action Methods
    
    @IBAction func closeAction(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func segmentedControlDidChange(_ sender: UISegmentedControl) {
        segmentedControl.changeUnderlinePosition()
        
        mainSelectedSection = CreateRecipeViewControllerSections(rawValue: segmentedControl.selectedSegmentIndex)!
        tableView.reloadData()
        if tableView.numberOfSections > 0, tableView.numberOfRows(inSection: 0) > 0 {
            let indexPath = IndexPath(row: 0, section: 0)
            tableView.scrollToRow(at: indexPath, at: .top, animated: true)
        }
    }
   
    @IBAction func onCLickAddNewPost(_ button: TransitionButton) {
        button.startAnimation()
        postRecipe()
    }
    
    
    // MARK: - Formatter Methods
    
    func formatInstructions () -> Bool
    {
        //recipeToPost.instructions = [Instruction]()
        instructionDictionaryArray = ""
        if (recipeInstructions.count) > 0 {
            var instructionNumber = 1
            for var instruction in recipeInstructions {
                
                if (instruction.content?.replacingOccurrences(of: " ", with: "") == "" || instruction.content == nil){
                    
                    self.postRecipeTransitionButton.stopAnimation()
                    self.segmentedControl.selectedSegmentIndex = 2
                    segmentedControlDidChange(self.segmentedControl)
                    tableView.reloadData()
                    self.showAlert(text: "Content of the added instruction should be added")
                    
                    return false
                }
                else {
                    var instructionDictionary: [String: Any] {
                        return ["title": "",
                                "Description": instruction.content!,
                                "number": instructionNumber
                        ]
                    }
                    
                    if instructionNumber < recipeInstructions.count
                    {
                        instructionDictionaryArray += "\(instructionDictionary),".replacingOccurrences(of: "[", with: "{").replacingOccurrences(of: "]", with: "}")
                    }
                    else
                    {
                        instructionDictionaryArray += "\(instructionDictionary)".replacingOccurrences(of: "[", with: "{").replacingOccurrences(of: "]", with: "}")
                    }
                    
                    instruction.number = instructionNumber
                    instructionNumber += 1
                }
            }
        }
        instructionDictionaryArray = "[" + instructionDictionaryArray + "]"
        return true
    }
    
    func formatIngredients () -> Bool
    {
        //recipeToPost.ingrediants = [Ingrediant]()
        ingredientDictionaryArray = ""
        if (recipeIngredients.count) > 0 {
            var ingredientNumber = 1
            for ingredient in recipeIngredients {
                
                if (ingredient.name?.replacingOccurrences(of: " ", with: "") == "" || ingredient.name == nil || ingredient.quantity?.replacingOccurrences(of: " ", with: "") == "" || ingredient.quantity == nil){
                    
                    self.postRecipeTransitionButton.stopAnimation()
                    self.segmentedControl.selectedSegmentIndex = 1
                    segmentedControlDidChange(self.segmentedControl)
                    tableView.reloadData()
                    self.showAlert(text: "All info regarding the added ingredient should be filled")
                    
                    return false
                }
                else {
                    var ingredientArray: [String: Any] {
                        return ["Name": ingredient.name!,
                                "Quantity": ingredient.quantity!
                        ]
                    }
                    
                    if ingredientNumber < recipeIngredients.count
                    {
                        ingredientDictionaryArray += "\(ingredientArray),".replacingOccurrences(of: "[", with: "{").replacingOccurrences(of: "]", with: "}")
                    }
                    else
                    {
                        ingredientDictionaryArray += "\(ingredientArray)".replacingOccurrences(of: "[", with: "{").replacingOccurrences(of: "]", with: "}")
                    }
                    ingredientNumber += 1
                }
               
            }
            ingredientDictionaryArray = "[" + ingredientDictionaryArray + "]"
        }
        
        return true
    }
    
    
    // MARK: - Validation
    
    func validate() -> Bool
    {
        if recipeToPost.title == nil || recipeToPost.title == "" {
            self.postRecipeTransitionButton.stopAnimation()
            mainSelectedSection = CreateRecipeViewControllerSections.recipe
            self.segmentedControl.selectedSegmentIndex = 0
            segmentedControlDidChange(self.segmentedControl)
            tableView.reloadData()
            self.showAlert(text: "Recipe Title must be added")
            return false
        }
        else if recipeToPost.timeRequired == nil || recipeToPost.timeRequired == "" {
            self.postRecipeTransitionButton.stopAnimation()
            self.segmentedControl.selectedSegmentIndex = 0
            segmentedControlDidChange(self.segmentedControl)
            tableView.reloadData()
            self.showAlert(text: "Time Required to make the dish must be added")
            return false
        }
        else if recipeToPost.discription == nil || recipeToPost.discription == "" {
            self.postRecipeTransitionButton.stopAnimation()
            self.segmentedControl.selectedSegmentIndex = 0
            segmentedControlDidChange(self.segmentedControl)
            tableView.reloadData()
            self.showAlert(text: "Recipe Description must be added")
            return false
        }
        else if selectedImage == nil {
            self.postRecipeTransitionButton.stopAnimation()
            self.segmentedControl.selectedSegmentIndex = 0
            segmentedControlDidChange(self.segmentedControl)
            tableView.reloadData()
            self.showAlert(text: "Recipe Picture should be added")
            return false
        }
        else if recipeIngredients.count < 1 || ((recipeIngredients[0].name == "" || recipeIngredients[0].name == nil) && (recipeIngredients[0].quantity == "" || recipeIngredients[0].quantity == nil))
        {
            self.postRecipeTransitionButton.stopAnimation()
            self.segmentedControl.selectedSegmentIndex = 1
            segmentedControlDidChange(self.segmentedControl)
            tableView.reloadData()
            self.showAlert(text: "Atleast one Ingredient must be added")
            return false
        }
        else if recipeInstructions.count < 1 || recipeInstructions[0].content == "" || recipeInstructions[0].content == nil{
            self.postRecipeTransitionButton.stopAnimation()
            self.segmentedControl.selectedSegmentIndex = 2
            segmentedControlDidChange(self.segmentedControl)
            tableView.reloadData()
            self.showAlert(text: "Atleast one Instruction must be added")
            return false
        }
        else {
           return true
        }
    }
    
    
    // MARK: - Service Call
    
    @available(iOS 11.0, *)
    func postRecipe()
    {
        
        if validate() && formatIngredients() && formatInstructions(){
            
            let data = UIImagePNGRepresentation(selectedImage!)
            APIManager.sharedManager.addNewPost(with: recipeToPost, imageData: data, instructionArrayString: instructionDictionaryArray, ingredientArrayString: ingredientDictionaryArray, fileName: "picture.jpg", apiCompletion: { (responseValue, error) in
                
                if error == nil {
                    self.postRecipeTransitionButton.stopAnimation(animationStyle: .expand, completion: {
                        if self.delegate != nil {
                            self.delegate?.recipeAdded()
                        }
                        self.dismiss(animated: true, completion: nil)
                    })
                } else {
                    self.postRecipeTransitionButton.errorAnimation()
                }
                
            }, progressHandler: {(progress) in
                if #available(iOS 11.0, *) {
                    print(progress.fileCompletedCount)
                } else {
                    // Fallback on earlier versions
                }
            })
        }
        
        
    }
    
}

extension CreateRecipeViewController: UITableViewDataSource {
    
    
    // MARK: - Main Table View DataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        var numberOfSections = 0
        
        if (mainSelectedSection ==  CreateRecipeViewControllerSections.recipe)
        {
         numberOfSections = 2
        }
        else if (mainSelectedSection ==  CreateRecipeViewControllerSections.ingredients)
        {
            numberOfSections = 2
        }
        else {
            numberOfSections = 2
        }
        
        return numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var numberOfRows = 0
        
        if (mainSelectedSection ==  CreateRecipeViewControllerSections.recipe)
        {
            switch CreateRecipeMainInfoSections(rawValue: section)! {
            case .addPicture:
                
                numberOfRows = 1
                break
            case .details:
                numberOfRows = 4
                break
            }
        }
        else if (mainSelectedSection ==  CreateRecipeViewControllerSections.ingredients)
        {
            switch CreateRecipeIngredientsSection(rawValue: section)! {
            case .ingredients:
                numberOfRows = (recipeIngredients.count)
                break
            case .addRow:
                numberOfRows = 1
                break
            }
        }
        else {
            switch CreateRecipeInstructionsSection(rawValue: section)! {
            case .instructions:
                
                if (recipeInstructions != nil)
                {
                    numberOfRows = (recipeInstructions.count)
                }
                else
                {
                    numberOfRows = 0
                }
                
                break
            case .addRow:
                numberOfRows = 1
                break
            }
        }
        return numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cellToReturn: UITableViewCell?
        
        if (mainSelectedSection ==  CreateRecipeViewControllerSections.recipe) {
            switch CreateRecipeMainInfoSections(rawValue: indexPath.section)! {
            case .addPicture:
                let cell = tableView.dequeueReusableCell(withIdentifier: "CreateRecipeInfoTableViewCell") as! CreateRecipeInfoTableViewCell
                
                if selectedImage != nil {
                    cell.selectedImageView.image = selectedImage
                }
                
                cell.delegate = self
                cellToReturn = cell
                
                break
            case .details:
                
                if(indexPath.row == 0) {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "CreateRecipeMultiLineTextTableViewCell") as! CreateRecipeMultiLineTextTableViewCell
                    
                    if recipeToPost.title != nil {
                        cell.mainTextView.text = recipeToPost.title
                    }
                    else {
                        cell.mainTextView.text = "Recipe Title"
                        cell.mainTextView.textColor = UIColor.lightGray
                    }
                    
                    cell.delegate = self
                    cell.crossButton.isHidden = true
                    cell.hideTextField()
                   
                    cell.indexToSend = indexPath.row
                    cell.sectionToSend = indexPath.section
                    cellToReturn = cell
                }
                else if (indexPath.row == 1) {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "CreateRecipeSingleLineTextTableViewCell") as! CreateRecipeSingleLineTextTableViewCell
                    cell.delegate = self
                    cell.indexToSend = indexPath.row
                    
                    if recipeToPost.timeRequired != nil
                    {
                        cell.mainTextField.text = recipeToPost.timeRequired
                    }
                    else
                    {
                        cell.mainTextField.attributedPlaceholder = NSAttributedString(string: "Time Required", attributes: nil)
                    }
                    cell.crossButton.isHidden = true
                    cell.layoutIfNeeded()
                    cellToReturn = cell
                }
                else if (indexPath.row == 2) {
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "CreateRecipeSingleLineTextTableViewCell") as! CreateRecipeSingleLineTextTableViewCell
                    cell.delegate = self
                    cell.mainTextField.keyboardType = UIKeyboardType.numberPad
                    cell.indexToSend = indexPath.row
                    
                    if recipeToPost.servings != nil {
                        cell.mainTextField.text = recipeToPost.servings
                    }
                    else {
                        cell.mainTextField.attributedPlaceholder = NSAttributedString(string: "Servings", attributes: nil)
                    }
                    
                    cell.crossButton.isHidden = true
                    cell.layoutIfNeeded()
                    cellToReturn = cell
                    
                }
                else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "CreateRecipeMultiLineTextTableViewCell") as! CreateRecipeMultiLineTextTableViewCell
                    
                    if recipeToPost.discription != nil
                    {
                        cell.mainTextView.text = recipeToPost.discription
                    }
                    else
                    {
                        cell.mainTextView.text = "Recipe Description"
                        cell.mainTextView.textColor = UIColor.lightGray
                    }
                    cell.crossButton.isHidden = true
                    cell.delegate = self
                    cell.hideTextField()
                    cell.indexToSend = indexPath.row
                    cell.sectionToSend = indexPath.section
                    cellToReturn = cell
                }
                break
            }
        }
        else if (mainSelectedSection ==  CreateRecipeViewControllerSections.ingredients)
        {
            switch CreateRecipeIngredientsSection(rawValue: indexPath.section)! {
            case .ingredients:
                let cell = tableView.dequeueReusableCell(withIdentifier: "CreateRecipeMultiLineTextTableViewCell") as! CreateRecipeMultiLineTextTableViewCell
                cell.textLabel?.text = ""
                cell.textLabel?.toolbarPlaceholder = "Add Ingredient"
                cell.delegate = self
                cell.indexToSend = indexPath.row
                cell.sectionToSend = indexPath.section
                cell.showTextfield()
                cell.rightTextField.attributedPlaceholder = NSAttributedString(string: "Quantity", attributes: nil)
                
                if (recipeIngredients.count) <= 1 {
                    cell.crossButton.isHidden = true
                }
                else {
                    cell.crossButton.isHidden = false
                }
                
                if (recipeIngredients[indexPath.row].name == nil || recipeIngredients[indexPath.row].name == "")
                {
                    cell.mainTextView.text = "Name"
                    cell.mainTextView.textColor = UIColor.lightGray
                }
                else
                {
                    cell.mainTextView.textColor = UIColor.black
                    cell.mainTextView.text = recipeIngredients[indexPath.row].name
                    cell.rightTextField.text = recipeIngredients[indexPath.row].quantity
                }
                
                cellToReturn = cell
                break
            case .addRow:
                let cell = tableView.dequeueReusableCell(withIdentifier: "AddRowTableViewCell") as! AddRowTableViewCell
                cell.delegate = self
                cellToReturn = cell
                break
            }
        }
        else {
            switch CreateRecipeInstructionsSection(rawValue: indexPath.section)! {
            case .instructions:
                let cell = tableView.dequeueReusableCell(withIdentifier: "CreateRecipeMultiLineTextTableViewCell") as! CreateRecipeMultiLineTextTableViewCell
                cell.textLabel?.text = ""
                cell.textLabel?.toolbarPlaceholder = "Add Ingredient"
                cell.delegate = self
                cell.indexToSend = indexPath.row
                cell.sectionToSend = indexPath.section
                cell.hideTextField()
                
                if (recipeInstructions.count) <= 1 {
                    cell.crossButton.isHidden = true
                }
                else {
                    cell.crossButton.isHidden = false
                }
                
                if (recipeInstructions[indexPath.row].content == nil || recipeInstructions[indexPath.row].content == "")
                {
                    cell.mainTextView.text = "Instruction"
                    cell.mainTextView.textColor = UIColor.lightGray
                }
                else
                {
                    cell.mainTextView.textColor = UIColor.black
                    cell.mainTextView.text = recipeInstructions[indexPath.row].content
                }
                
                cellToReturn = cell
                break
            case .addRow:
                let cell = tableView.dequeueReusableCell(withIdentifier: "AddRowTableViewCell") as! AddRowTableViewCell
                cell.delegate = self
                cellToReturn = cell
                break
            }
        }
        
        cellToReturn?.selectionStyle = .none
        return cellToReturn!
    }
    
}

extension CreateRecipeViewController: UITableViewDelegate {
    
    
    // MARK: - Main Table View Delegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
}

extension CreateRecipeViewController: CreateRecipeMultiLineTextTableViewCellDelegate {
    
    
    // MARK: - Multi Line Text Cell Delegate
    
    func updateTextView(mainTextView: UITextView, rightTextField: UITextField, index: Int, section: Int) {
        // Disabling animations gives us our desired behaviour
        UIView.setAnimationsEnabled(false)
        /* These will causes table cell heights to be recaluclated,
         without reloading the entire cell */
        tableView.beginUpdates()
        
        
        if (mainSelectedSection ==  CreateRecipeViewControllerSections.recipe)
        {
            switch CreateRecipeMainInfoSections(rawValue: section)! {
            case .addPicture:
                break
            case .details:
                
                if index == 0 {
                    recipeToPost.title = mainTextView.text!
                }
                else {
                    recipeToPost.discription = mainTextView.text!
                }
                
                break
            }
        }
        
        if (mainSelectedSection ==  CreateRecipeViewControllerSections.ingredients)
        {
            recipeIngredients[index].name = mainTextView.text!
            recipeIngredients[index].quantity = rightTextField.text!
        }
        
        if (mainSelectedSection ==  CreateRecipeViewControllerSections.instructions) {
            recipeInstructions[index].content = mainTextView.text!
        }
        
        tableView.endUpdates()
        // Re-enable animations
        UIView.setAnimationsEnabled(true)
        
        let indexPath = IndexPath(row: index, section: section)
        tableView.scrollToRow(at: indexPath, at: .bottom, animated: false)
        
        
        
    }
    
    func removeRow(index: Int) {
        if (mainSelectedSection ==  CreateRecipeViewControllerSections.ingredients)
        {
            self.recipeIngredients.remove(at: index)
            self.tableView.reloadData()
        }
        else {
            self.recipeInstructions.remove(at: index)
            self.tableView.reloadData()
        }
    }
    
}

extension CreateRecipeViewController: AddRowTableViewCellDelegate {
    
    
    // MARK: - Add Row Cell Delegate
    
    func addRow() {
        if (mainSelectedSection ==  CreateRecipeViewControllerSections.ingredients)
        {
            //self.recipeIngredients.insert(emptyIngredient!, at: ((recipeIngredients.count) - 1))
            
            self.recipeIngredients.append(emptyIngredient!)
            self.tableView.reloadData()
        }
        else
        {
            //self.recipeInstructions.insert(emptyInstruction!, at: ((recipeInstructions.count) - 1))
            
            self.recipeInstructions.append(emptyInstruction!)
            self.tableView.reloadData()
        }
    }
}

extension CreateRecipeViewController: CreateRecipeSingleLineTextTableViewCellDelegate {
   
    
    // MARK: - Single Line Text Cell Delegate
    
    func updateSingleLineCell(mainTextField: UITextField, index: Int) {
        if index == 1 {
            recipeToPost.timeRequired = mainTextField.text!
        }
        
        if index == 2 {
            if let servings = Int(mainTextField.text!){
                recipeToPost.servings = "\(servings)"
            }
        }
    }
    
    func removeRowSingleLineTextField(index: Int) {
        if (mainSelectedSection ==  CreateRecipeViewControllerSections.ingredients)
        {
            self.recipeIngredients.remove(at: index)
            self.tableView.reloadData()
        }
        if (mainSelectedSection ==  CreateRecipeViewControllerSections.instructions)
        {
            self.recipeInstructions.remove(at: index)
            self.tableView.reloadData()
        }
    }
    
}

extension CreateRecipeViewController: CreateRecipeInfoTableViewCellDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate  {
    
    
    // MARK: - Picker View Controller Delegate
    
    func pickImage() {
        selectedImage = nil
        
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Button capture")
            
            checkPermission()
            
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum;
            imagePicker.allowsEditing = false
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    
    //To Get User's permission for the picture
    func checkPermission() {
        let photoAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        
        switch photoAuthorizationStatus {
        case .authorized: print("Access is granted by user")
        case .notDetermined: PHPhotoLibrary.requestAuthorization({
            (newStatus) in print("status is \(newStatus)")
            if newStatus == PHAuthorizationStatus.authorized {
               print("success") }
            })
            
        case .restricted:  print("User do not have access to photo album.")
        case .denied:  print("User has denied the permission.")
            }
            
        }
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        self.dismiss(animated: true, completion: { () -> Void in
            
        })
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        selectedImage = chosenImage
        self.tableView.reloadData()
    }
    
    
    
}
