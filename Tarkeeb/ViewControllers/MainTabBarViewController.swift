//
//  MainTabBarViewController.swift
//  Tarkeeb
//
//  Created by Razi Tiwana on 05/09/2018.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import UIKit

class MainTabBarViewController: UITabBarController {

    private var shouldSelectIndex = -1

    
    // MARK: - ViewController LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addTabs()
        delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(notificationCountUpdated), name:.notificationCountUpdated, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Notification Methods
    
    @objc func notificationCountUpdated() {
       tabBar.items?[2].badgeValue = User.currentUser?.unreadNotificationCount
    }

    // MARK: - Private Methods
    
    func addTabs() {

        let homeController = StoryboardRouter.homeNavigationViewController()
        let homeItem = UITabBarItem(title: "", image: UIImage(named: "ic_home_unselected"), selectedImage:  UIImage(named: "ic_home_selected"))
        homeController.tabBarItem = homeItem
        homeController.title = "Home"
        
        let searchController = StoryboardRouter.searchNavigationViewController()
        let searchItem = UITabBarItem(title: "", image: UIImage(named: "ic_search_unselected"), selectedImage:  UIImage(named: "ic_search_selected"))
        searchController.tabBarItem = searchItem
        searchController.title = "Search"
        
        let notificationController = StoryboardRouter.notificationNavigationViewController()
        let notificationItem = UITabBarItem(title: "", image: UIImage(named: "ic_notification_unselected"), selectedImage:  UIImage(named: "ic_notification_selected"))
        notificationController.tabBarItem = notificationItem
        notificationController.title = "Notification"
        
        let profileController = StoryboardRouter.profileNavigationViewController()
        let profileItem = UITabBarItem(title: "", image: UIImage(named: "ic_profile_unselected"), selectedImage:  UIImage(named: "ic_profile_selected"))
        profileController.tabBarItem = profileItem
        profileController.title = User.currentUser?.userName
        
        let controllers = [homeController, searchController, notificationController, profileController]
        
        self.viewControllers = controllers
    }
}

extension MainTabBarViewController: UITabBarControllerDelegate {
   
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        shouldSelectIndex = tabBarController.selectedIndex
        return true
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if shouldSelectIndex == tabBarController.selectedIndex {
            if let splitViewController = viewController as? UISplitViewController {
                if let navController = splitViewController.viewControllers[0] as? UINavigationController {
                    if navController.viewControllers.count == 1 {
                        let viewController = navController.viewControllers.first as? ViewController
                        viewController?.scrollToTop()
                    } else {
//                        navController.popToRootViewController(animated: true)
                    }
                }
            }
            
            if let navController = viewController as? UINavigationController {
                if navController.viewControllers.count == 1 {
                    let viewController = navController.viewControllers.first as? ViewController
                    viewController?.scrollToTop()
                } else {
                    //                        navController.popToRootViewController(animated: true)
                }
            }
            
            
            
        }
    }
}
