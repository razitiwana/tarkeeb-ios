//
//  SubmitFeedbackViewController.swift
//  Tarkeeb
//
//  Created by Osama Azmat Khan on 9/21/18.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import UIKit
import SwiftyGif
import TransitionButton

class SubmitFeedbackViewController: UIViewController {

    
    // MARK: - Outlets
    
    @IBOutlet weak var cancelView: UIView!
    @IBOutlet weak var submitTransitionButton: TransitionButton!
    @IBOutlet weak var transitionButtonView: UIView!
    @IBOutlet weak var feedbackTextView: UITextView!
    @IBOutlet weak var subHeadingLabel: UILabel!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var containerView: UIView!
    
    
    // MARK: - Variables
    
    var feedback = Feedback()
    
    
    // MARK: - ViewController LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func configureView() {
        feedbackTextView.roundVeryLittle()
        
        let gif = UIImage(gifName: "ic_feedback.gif")
        
        self.backgroundImageView.setGifImage(gif)
//    self.backgroundImageView.startAnimatingGif()
        
        submitTransitionButton.setTitle("Submit", for: .normal)
        submitTransitionButton.setTitleColor(UIColor.appThemeColor(), for: .normal)
        submitTransitionButton.titleLabel?.font = UIFont.systemFont(ofSize: 23, weight: .medium)
        submitTransitionButton.spinnerColor = UIColor.appThemeColor()
        
        cancelView.roundVeryLittle(cornerRadius: 8)
        cancelView.addBorders()
        
        submitTransitionButton.addTarget(self, action: #selector(buttonAction(_:)), for: .touchUpInside)
        
        let cancelViewTapGesture = UITapGestureRecognizer(target: self, action: #selector(cancelViewTapped))
        cancelView.addGestureRecognizer(cancelViewTapGesture)
        cancelView.isUserInteractionEnabled = true
        
    }
    
    
    // MARK: - Actions
    
    @objc func cancelViewTapped() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelAction(_ button: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func buttonAction(_ button: TransitionButton) {
        button.startAnimation()
        if self.feedbackTextView.text! != "" {
            self.feedback?.content = feedbackTextView.text!
            submitFeedback()
        } else {
            feedbackTextView.errorAnimation()
            button.stopAnimation(animationStyle: .normal, completion: nil)
        }
    }
    
    
    // MARK: - Service Calls
    
    func submitFeedback() {
        
        APIManager.sharedManager.submitFeedback(feedback: self.feedback!)
        { (feedback, error) in
            if error == nil {
                self.submitTransitionButton.stopAnimation(animationStyle: .expand, completion:
                {
                    self.dismiss(animated: true, completion: nil)
                })
            } else {
                self.submitTransitionButton.stopAnimation(animationStyle: .shake, completion: nil)
            }
        }
    }
    
}
