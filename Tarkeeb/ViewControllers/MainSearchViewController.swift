//
//  MainSearchViewController.swift
//  Tarkeeb
//
//  Created by Razi Tiwana on 05/09/2018.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import UIKit

enum SearchResultsType: Int {
    case posts
    case users
}


class MainSearchViewController: ViewController {

    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    
    var user = User.currentUser
    var searchResultsType = SearchResultsType.posts
    var posts:[Recipe] = [Recipe]()
    var users:[User] = [User]()
    
    var searchControllerNav: UINavigationController?
    var searchtext = ""
    var searchPressed = false
    
    var selectedIndex = SearchResultsType.posts
    var count = Constants.countToFetchAtOneTime
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
      
    }
    
    func configureTableView() {
        
        // To remove the extra cells
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = UIColor.tableViewBackgroundColor()
        
        tableView.register(UINib(nibName: "RecipeSearchTableViewCell", bundle: nil), forCellReuseIdentifier: "RecipeSearchTableViewCell")
        tableView.register(UINib(nibName: "FollowersAndFollowingTableViewCell", bundle: nil), forCellReuseIdentifier: "FollowersAndFollowingTableViewCell")
        
        // Other wise the scroll inset are changeed when reloading data
        tableView.estimatedRowHeight = CGFloat(Constants.estimatedRecipeCellSize)
        
        segmentedControl.removeBorder()
        
        segmentedControl.addUnderlineForSelectedSegment(numberofSegments: 2, width: UIScreen.main.bounds.width)
    }
    
    func showNoRecordsLabel()
    {
        switch selectedIndex {
        case .posts:
                let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
                noDataLabel.text          = "No recipes found"
                noDataLabel.textColor     = UIColor.black
                noDataLabel.textAlignment = .center
                tableView.backgroundView  = noDataLabel
                tableView.separatorStyle  = .none
            break
            
        case .users:
                let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
                noDataLabel.text          = "No accounts found"
                noDataLabel.textColor     = UIColor.black
                noDataLabel.textAlignment = .center
                tableView.backgroundView  = noDataLabel
                tableView.separatorStyle  = .none
            break
            
        }
    }
    func hideNoRecordsLabel()
    {
        tableView.backgroundView = nil
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }

    @IBAction func segmentedControlDidChange(_ sender: UISegmentedControl) {
        
        segmentedControl.changeUnderlinePosition()
       
        selectedIndex = SearchResultsType(rawValue: segmentedControl.selectedSegmentIndex)!
        tableView.reloadData()
        if tableView.numberOfSections > 0, tableView.numberOfRows(inSection: 0) > 0 {
            let indexPath = IndexPath(row: 0, section: 0)
            tableView.scrollToRow(at: indexPath, at: .top, animated: true)
        }
    }
    
    func fetchResults() {
        hideNoRecordsLabel()
        APIManager.sharedManager.fetchSearchResults(with: searchtext) { (searchResults, error) in
            if error == nil {
                self.users = (searchResults?.chefs)!
                self.posts = (searchResults?.recipies)!
                
                if (self.posts.count == 0)
                {
                    self.showNoRecordsLabel()
                }
                
                if (self.users.count == 0)
                {
                    self.showNoRecordsLabel()
                }
                
                self.tableView.reloadData()
            } else {
                self.showAlert(text: error!)
            }
        }
        
    }
    
}

extension MainSearchViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var numberOfSections = 0
        
        switch selectedIndex {
        case .posts:
            if self.posts.count > 0 {
                numberOfSections = 1
                //tableView.backgroundView = nil
            }
            break
            
        case .users:
            if self.users.count > 0 {
                numberOfSections = 1
                //tableView.backgroundView = nil
            }
            break
            
        }
        
        return numberOfSections
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var numberOfRows = 0
        
        switch selectedIndex {
        case .posts:
            numberOfRows = self.posts.count
            break
            
        case .users:
            numberOfRows = self.users.count
            break
            
        }
        return numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        var cellToReturn: UITableViewCell?
        
        switch selectedIndex {
        case .posts:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "RecipeSearchTableViewCell") as! RecipeSearchTableViewCell
            let selectedPost = posts[indexPath.row]
            cell.configureCell(with: selectedPost)
            cellToReturn = cell
            
            break
            
        case .users:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "FollowersAndFollowingTableViewCell") as! FollowersAndFollowingTableViewCell
            let selectedUser = users[indexPath.row]
            cell.configureCell(with: selectedUser)
            cellToReturn = cell
            cell.delegate = self
            
            break
        }
        
        cellToReturn?.selectionStyle = .none
        
        return cellToReturn!
    }
}

extension MainSearchViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch selectedIndex {
        case .posts:
            let selectedPost = posts[indexPath.row]
            posts[indexPath.row].postViews += 1
            
            let viewController = StoryboardRouter.recipeDetailViewController()
            viewController.post = selectedPost
            
            searchControllerNav?.pushViewController(viewController, animated: true)
            
            tableView.reloadData()
            
            break
            
        case .users:
            let selectedUser = users[indexPath.row]
            
            let viewController = StoryboardRouter.profileViewController()
            viewController.user = selectedUser
            viewController.navigationController?.setNavigationBarHidden(true, animated: false)
            
            searchControllerNav?.pushViewController(viewController, animated: true)
            
            tableView.reloadData()
            
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}

extension MainSearchViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        searchControllerNav = searchController.presentingViewController?.navigationController
        
        if searchController.isActive{
            searchtext = searchController.searchBar.text!
            if searchtext.count >= 3 {
                self.fetchResults()
                tableView.reloadData()
            }
            
            if searchPressed && searchtext.count > 0 {
                self.fetchResults()
                searchPressed = false
                tableView.reloadData()
            }
        }
        else {
            searchtext = ""
            self.fetchResults()
            tableView.reloadData()
        }
       
    }
    
}


extension MainSearchViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchPressed = true
        searchBar.text = searchBar.text!
    }
    
    
}

extension MainSearchViewController: FollowersAndFollowingTableViewCellDelegate {
    func openDetails(user: User) {
        let viewController = StoryboardRouter.profileViewController()
        viewController.user = user
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func follow(user: User) {
        if user.isFollowed {
            
            let alertController = UIAlertController(title: "Are You sure you want to unfollow " + user.userName! + "?", message: "", preferredStyle: .actionSheet)
            
            let unfollowAction = UIAlertAction(title: "Unfollow" , style: .destructive) { unfollowAction in
                self.changeFollowState(user: user)
            }
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            
            alertController.addAction(unfollowAction)
            alertController.addAction(cancelAction)
            alertController.view.tintColor = .black
            
            present(alertController, animated: true, completion: nil)
        } else {
            changeFollowState(user: user)
        }
    }
    
    func changeFollowState(user: User) {
        let oldState = user.isFollowed
        
        let selectedIndex = users.indices.filter { (index) -> Bool in
            return users[index].userID == user.userID
        }
        
        APIManager.sharedManager.follow(user: user) { (sucessMessage, error) in
            if error != nil {
                
                self.users[selectedIndex.first!].isFollowed = oldState
                
                self.tableView.reloadData()
            } else {
            }
        }
        
        self.user?.isFollowed = true
        
        users[selectedIndex.first!].isFollowed = !oldState
        
        tableView.reloadData()
    }
    
}

