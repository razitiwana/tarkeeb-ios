//
//  NotificationViewController.swift
//  Tarkeeb
//
//  Created by Osama Azmat khan on 05/09/2018.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import UIKit

// TODO: - Osama
// Fetch Data from Service
// Design cells
// Populate data in fields
// Add pagination
// Add progress hud
// Open profile when makes sense
// And Open post when makes sense
// Chnage the color a little for the notification that are read
// Keep in Mind all the conventions followed in the app

// Point Regarding Source control
// Only Commit changes to the files relating to Notification, discard changes from files that should not have been changed
// If you have opned a storyboard for reviw or accident it might be changed even if you didn't made any change. A capital "M" signifies changed. just right click on the file -> Source Control -> Discard change

class NotificationViewController: ViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private var notifications: [Notification] = [Notification]()
    
    private let refreshControl = UIRefreshControl()
    
    var fetchingMore = false
    var moreRemaining = true
    
    var startIndex = 0
    var count = Constants.countToFetchAtOneTime
    
    // MARK: - ViewController Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Notifications"
        
        configureTableView()
        
        fetchAllNotifications()
        refreshControl.beginRefreshing()
    }
    
    // MARK: - Private Methods
    
    func configureTableView() {
        // To remove the extra cells
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = UIColor.tableViewBackgroundColor()
        
        tableView.register(UINib(nibName: "NotificationTableViewCell", bundle: nil), forCellReuseIdentifier: "NotificationTableViewCell")
        tableView.register(UINib(nibName: "LoadingTableViewCell", bundle: nil), forCellReuseIdentifier: "LoadingTableViewCell")
        
        // Setup refresh Control
        tableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        
        // Other wise the scroll inset are changeed when reloading data
        tableView.estimatedRowHeight = CGFloat(Constants.estimatedNotificationCellSize)
    }
    
    func showNoRecordsLabel()
    {
        let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
        noDataLabel.text          = "No notifications found"
        noDataLabel.textColor     = UIColor.black
        noDataLabel.textAlignment = .center
        noDataLabel.tag = 10
        tableView.addSubview(noDataLabel)
    }
    func hideNoRecordsLabel()
    {
        for view in tableView.subviews {
            if view.tag == 10 {
                view.removeFromSuperview()
            }
        }
    }
    
    @objc func refresh() {
        self.startIndex = 0
        self.moreRemaining = true
        self.fetchingMore = false
        fetchAllNotifications()
    }
    
    func fetchAllNotifications() {
        hideNoRecordsLabel()
        APIManager.sharedManager.fetchNotifications(for: User.currentUser!, startIndex: self.startIndex, count: self.count) { (notifications, error) in
            
            let isPullToRefresh = self.refreshControl.isRefreshing
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                self.refreshControl.endRefreshing()
            }
            
            if error == nil, let notifications = notifications {
                
                if self.startIndex > 0 {
                    self.notifications.append(contentsOf: notifications)
                }
                else {
                    self.notifications = notifications
                }
               
                // If the count of the fectehed posts is lesss than the count send then that means there are no more items left to fetch from data base
                self.moreRemaining = (notifications.count) >= self.count
                
                self.fetchingMore = false
                
                let previousContentOfset = self.tableView.contentOffset
                
                self.tableView.reloadData()
                
                // if isPullToRefresh then it would be stuck in the pulled state if the service responds quickly
                if notifications.count > 0 && !isPullToRefresh {
                    self.tableView.setContentOffset(previousContentOfset, animated: true)
                }
                else {
                    if notifications.count > 0 {
                        self.hideNoRecordsLabel()
                    }
                    else {
                        self.showNoRecordsLabel()
                    }
                }
            } else {
                self.showAlert(text: error!)
            }
            
             self.tableView.tableFooterView = nil
        }
        
    }
    
    func beginBatchFetch() {
        print("Fetching")
        
        fetchingMore = true
        self.startIndex += self.count
        
        self.fetchAllNotifications()
    }
}

extension NotificationViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        var numberOfSections = 0
        
            if self.notifications.count > 0 {
                numberOfSections = 1
                self.hideNoRecordsLabel()
            }
        
        return numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationTableViewCell") as! NotificationTableViewCell
        let notification = notifications[indexPath.row]
        cell.configureCell(with: notification)
        cell.selectionStyle = .none
        
        return cell
    }
   
}

extension NotificationViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let notification = notifications[indexPath.row]
        
        if notification.notificationType == NotificationType.follow {
            let viewController = StoryboardRouter.profileViewController()
            viewController.user = notification.user
            viewController.user?.notificationID = notification.notificationID
            
            navigationController?.pushViewController(viewController, animated: true)
        }
        else if notification.notificationType == NotificationType.like {
            var post = Recipe()
            post.postID = notification.postID
            post.title = notification.postTitle
            post.notificationID = notification.notificationID
            
            let viewController = StoryboardRouter.recipeDetailViewController()
            viewController.post = post
            navigationController?.pushViewController(viewController, animated: true)
        }

        notifications[indexPath.row].isRead = true
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.notifications.count >= count && self.moreRemaining {
            
            let currentOffset = scrollView.contentOffset.y;
            let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
            
            // if the there are elements less than size of tableview
            if (scrollView.contentSize.height > self.tableView.frame.size.height) {
                
                // In some cases the curent offset is greater than the maximum one which makes no sense, so a hack would be to add this check
                // The 100 if the user pulls the table view it should still load
                if maximumOffset + 100 > currentOffset {
                    // Change 10.0 to adjust the distance from bottom
                    if (maximumOffset - currentOffset <= 10.0 ) {
                        if !self.fetchingMore {
                            tableView.tableFooterView = loadingView()
                            scrollView.setContentOffset(CGPoint(x: 0, y: maximumOffset + 44), animated: true)
                            
                            self.beginBatchFetch()
                        }
                    }}
            }
        }
    }
    
}
