//
//  GenericPickerViewController.swift
//  Tarkeeb
//
//  Created by Osama Azmat Khan on 12/10/18.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import UIKit

protocol GenericPickerViewControllerDelegate {
    func didSelectValue(selectedValue: String)
    func cancel()
}

class GenericPickerViewController: ViewController {

    
    // MARK: - Outlets
    
    @IBOutlet var containerView: UIView!
    @IBOutlet weak var presentationView: UIView!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    
    
    // MARK: - Variables
    
    var delegate: GenericPickerViewControllerDelegate?
    var stringArray: [String] = [""]
    var pickerViewTitle: String?
    var doneButtonTitle: String?
    
    
    // MARK: - ViewController LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        configureData()
    }
    
    
    // MARK: - Configure Methods
    
    func configureView() {
        
        presentationView.roundVeryLittle()
       
        let valueSelectedGesture = UITapGestureRecognizer(target: self, action: #selector(selectValue))
        acceptButton.addGestureRecognizer(valueSelectedGesture)
        acceptButton.isUserInteractionEnabled = true
        
        let cancelViewTapGesture = UITapGestureRecognizer(target: self, action: #selector(cancelSelection))
        containerView.addGestureRecognizer(cancelViewTapGesture)
        containerView.isUserInteractionEnabled = true
    }
    
    func configureData() {
        
        if pickerViewTitle != nil {
            titleLabel.text = pickerViewTitle!
        }
        
        if doneButtonTitle != nil {
            acceptButton.setTitle(doneButtonTitle,for: .normal)
        }
        
    }
    
    
    // MARK: - Action Methods
    
    @objc func cancelSelection() {
        if self.delegate != nil {
            self.delegate?.cancel()
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func selectValue() {
        let selectedRow = pickerView.selectedRow(inComponent: 0)
        let selectedValueString = stringArray[selectedRow]
        if self.delegate != nil {
            self.delegate?.didSelectValue(selectedValue: selectedValueString)
        }
        self.dismiss(animated: true, completion: nil)
    }
    

}

extension GenericPickerViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    
    // MARK: - PickerView Delegate, DataSource
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        let numberOfComponentsLocal = 1
        
        return numberOfComponentsLocal
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        let numberOfRows = self.stringArray.count
        
        return numberOfRows
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.stringArray[row]
    }
}



















