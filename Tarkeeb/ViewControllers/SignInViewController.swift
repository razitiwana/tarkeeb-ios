//
//  SignInViewController.swift
//  Tarkeeb
//
//  Created by Razi Tiwana on 05/09/2018.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import UIKit

import UIKit
import TransitionButton
import FBSDKCoreKit
import FBSDKLoginKit

class SignInViewController: ViewController {
    
    
    // MARK: - Outlets
    
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginWithFacebookButton: TransitionButton!
    
    @IBOutlet weak var signupTransitionButton: TransitionButton!
    @IBOutlet weak var loginTransitionButton: TransitionButton!
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var signInContainerView: UIView!
    
    @IBOutlet weak var fbButtonImageView: UIImageView!
    
    var facebookToken: String?
    
    // MARK: - ViewController LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureSignIn()
        configureTransitionButtons()
    }
    
    
    // MARK: - Action Methods
    
    @IBAction func signInAction(_ button: TransitionButton) {
        sendLogInRequest()
    }
    
    @IBAction func signUpAction(_ button: TransitionButton) {
        button.startAnimation()
        presentSignUpViewController()
    }
    
    @IBAction func faceBookAction(_ button: TransitionButton) {
        button.startAnimation()
        facebookLogin()
        fbButtonImageView.isHidden = true
    }
    
    @IBAction func forgotAction(_ button: UIButton) {
        let viewConController = StoryboardRouter.forgotPasswordViewController()
        self.present(viewConController, animated: true)
    }
    
    
    // MARK: - Private Methods
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func configureSignIn() {
        userNameTextField.defaultTextField()
        passwordTextField.defaultTextField()
        loginWithFacebookButton.round()
    }
    
    func configureTransitionButtons() {
        loginTransitionButton.round()
        loginTransitionButton.dropShadow()
        loginTransitionButton.cornerRadius1 = loginTransitionButton.frame.height / 2
        loginTransitionButton.setTitle("Login", for: .normal)
        loginTransitionButton.setTitleColor(UIColor.appThemeColor(), for: .normal)
        loginTransitionButton.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: .medium)
        loginTransitionButton.spinnerColor = UIColor.appThemeColor()
        
        signupTransitionButton.round()
        signupTransitionButton.dropShadow()
        signupTransitionButton.cornerRadius1 = signupTransitionButton.frame.height / 2
        signupTransitionButton.spinnerColor = UIColor.appThemeColor()
        
        loginWithFacebookButton.cornerRadius1 = loginWithFacebookButton.frame.height / 2
        
    }
    
    func presentHomeViewController() {
        appDelegate?.presentHomeViewController()
    }
    
    func presentSignUpViewController() {
        self.signupTransitionButton.stopAnimation(animationStyle: .expand, completion: {
            let viewController = StoryboardRouter.signUpViewController()
            self.present(viewController, animated: true)
        })
    }
    
    func sendLogInRequest() {
        var user = User()
        
        // here we will take the vaues from textfields
        if validate() {
            loginTransitionButton.startAnimation()
            
            user.userName = userNameTextField.text!
            user.password = passwordTextField.text!
            
            APIManager.sharedManager.login(with: user) { (user, error) in
                
                if error == nil {
                    User.currentUser = user
                    self.signInContainerView.bringSubview(toFront: self.loginTransitionButton)
                    self.loginTransitionButton.stopAnimation(animationStyle: .expand, completion: {
                        self.presentHomeViewController()
                    })
                } else {
                    self.loginTransitionButton.stopAnimation(animationStyle: .shake, completion: {
                        self.showAlert(text: (error)!)
                    })
                }
            }
        }
    }
    
    func validate() -> Bool {
        
        var isValid = true
        if userNameTextField.text! == "" {
            userNameTextField.errorAnimation()
            isValid = false
        }
        else if passwordTextField.text! == "" {
            passwordTextField.errorAnimation()
            isValid = false
        }
        
  //      if !isValid {
    //        loginTransitionButton.stopAnimation(animationStyle: .normal, completion: nil)
      //  }
        
        return isValid
    }
    
    
    // MARK: - FaceBook Login  Methods
    
    func facebookLogin() {
        let loginManager = FBSDKLoginManager.init()
        
        let loginPermissions = ["public_profile", "email", "user_friends"]
        
        loginManager.logIn(withReadPermissions: loginPermissions, from: self) { (result, error) in
            
            if let error = error {
                self.loginWithFacebookButton.stopAnimation(animationStyle: .shake, completion: {
                    self.showAlert(text: error.localizedDescription)
                    self.fbButtonImageView.isHidden = false
                    self.view.bringSubview(toFront: self.fbButtonImageView)
                })
                print("Facebook Login Error: ",  error)
                
            } else {
                if let token = result?.token {
                    
                    self.facebookToken = token.tokenString
                    
                    loginManager.logOut()
                    
                    self.fetchFacebookUserDetails()
                } else {
                    // cancel pressed
                    self.loginWithFacebookButton.stopAnimation(animationStyle: .shake, completion: {
                        self.fbButtonImageView.isHidden = false
                        self.view.bringSubview(toFront: self.fbButtonImageView)
                    })
                }
            }
        }
    }
    
    func fetchFacebookUserDetails() {
        
        let req = FBSDKGraphRequest(graphPath: "me", parameters:["fields": "id, name, first_name, last_name, email, gender, picture.type(large)"], tokenString: self.facebookToken!, version: nil, httpMethod: "GET")
        
        _ = req?.start(completionHandler: { (connection, result, error) in
            
            if(error == nil)
            {
                print("result \(result ?? "")")
                
                let info = result as! NSDictionary
                
                var newUser: User = User()
                
                if let imageURL = ((info.value(forKey: "picture") as? NSDictionary)?.value(forKey: "data") as? NSDictionary)?.value(forKey: "url") as? String {
                    newUser.userPicture = imageURL
                }
                
                if let id = info.value(forKey: "id") as? String {
                    newUser.facebookUserID = id
                }
                
                if let first_name = info.value(forKey: "first_name") as? String {
                    newUser.firstName = first_name
                }
                
                if let last_name = info.value(forKey: "last_name") as? String {
                    newUser.lastName = last_name
                }
                
                if let email = info.value(forKey: "email") as? String {
                    newUser.email = email
                }
                
                if let gender = info.value(forKey: "gender") as? String {
                    if let gender = Gender(rawValue: gender) {
                         newUser.gender = gender
                    }
                }
                
                newUser.facebookAccessToken = self.facebookToken
                
                self.loginViaFacebook(with: newUser)
                
            }
            else
            {
                self.showAlert(text: (error?.localizedDescription)!)
            }
        })
        
    }
    
    
    func loginViaFacebook(with user: User) {
        
        APIManager.sharedManager.loginViaFacebook(with: user) { (user, error) in
            self.dismissProgressHud()
            
            if error == nil {
                User.currentUser = user
                self.containerView.bringSubview(toFront: self.loginWithFacebookButton)
                
                self.loginWithFacebookButton.stopAnimation(animationStyle: .expand, completion: {
                    self.presentHomeViewController()
                    
                })
            } else {
                self.loginWithFacebookButton.stopAnimation(animationStyle: .shake, completion: {
                    self.showAlert(text: (error)!)
                    self.fbButtonImageView.isHidden = false
                    self.view.bringSubview(toFront: self.fbButtonImageView)
                })
            }
        }
    }
}

extension SignInViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Try to find next responder
        let nextTextField = view.allSubViewsOf(type: UITextField.self).filter{$0.tag == textField.tag + 1}.first
        
        if let nextField = nextTextField {
            nextField.becomeFirstResponder()
        } else {
            // Not found, so remove keyboard.
            textField.resignFirstResponder()
            signInAction(loginTransitionButton)
        }
        // Do not add a line break
        return true
    }
}
