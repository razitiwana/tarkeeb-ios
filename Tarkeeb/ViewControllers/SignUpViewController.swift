//
//  SignUpViewController.swift
//  Tarkeeb
//
//  Created by Osama Azmat Khan on 9/28/18.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import UIKit
import TransitionButton
import ObjectMapper

class SignUpViewController: ViewController {

    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var signupButton: TransitionButton!
    @IBOutlet weak var cancelView: UIView!
    @IBOutlet weak var termsSwitch: UISwitch!
    
    
    // MARK: - ViewController LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureSignUp()
        addCancelGesture()
    }
    
    
    // MARK: - Action Methods
    
    @objc @IBAction func onClickCrossButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func signUpAction(_ button: TransitionButton) {
        sendSignUpRequest()
    }
    
    @IBAction func termsAction(_ sender: UIButton) {
        openURL(url: Constants.termsURL)
    }
    
    @IBAction func privacyPolicyAction(_ sender: UIButton) {
        openURL(url: Constants.privacyPolicy)
    }
    
    
    // MARK: - Private Methods
    
    func configureSignUp() {
        usernameTextField.defaultTextField()
        nameTextField.defaultTextField()
        emailTextField.defaultTextField()
        passwordTextField.defaultTextField()
        confirmPasswordTextField.defaultTextField()
        
        configureTransitionButtons()
        
        cancelView.roundVeryLittle(cornerRadius: 8)
        cancelView.addBorders()
    }
    
    func configureTransitionButtons() {
        signupButton.round()
        signupButton.dropShadow()
        signupButton.cornerRadius1 = signupButton.frame.height / 2
        signupButton.spinnerColor = UIColor.appThemeColor()
    }
    
    func addCancelGesture() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onClickCrossButton(_:)))
        cancelView.addGestureRecognizer(tapGesture)
        cancelView.isUserInteractionEnabled = true
    }
    
    func presentHomeViewController() {
        let application = UIApplication.shared.delegate as? AppDelegate
        application?.presentHomeViewController()
    }
    
    func validateSignUp() -> Bool {
    
        var isValid = true
        if usernameTextField.text! == "" {
            usernameTextField.errorAnimation()
            isValid = false
        }
        else if nameTextField.text! == "" {
            nameTextField.errorAnimation()
            isValid = false
        }
        else if emailTextField.text! == "" {
            emailTextField.errorAnimation()
            isValid = false
        }
        else if passwordTextField.text! == "" {
            passwordTextField.errorAnimation()
            isValid = false
        }
        else if confirmPasswordTextField.text! == "" {
            confirmPasswordTextField.errorAnimation()
            isValid = false
        }
        else if passwordTextField.text! != confirmPasswordTextField.text! {
            confirmPasswordTextField.errorAnimation()
            showAlert(text: "Passwords don't match")
            isValid = false
        } else if !termsSwitch.isOn {
            showAlert(text: "You must agree with Terms and Privacy policy")
            isValid = false
        }
        
        return isValid
    }
    
    
    func sendSignUpRequest() {
        var user = User()
        
        // here we will take the vaues from textfields
        if validateSignUp() {
            signupButton.startAnimation()
            
            usernameTextField.defaultTextField()
            nameTextField.defaultTextField()
            emailTextField.defaultTextField()
            passwordTextField.defaultTextField()
            confirmPasswordTextField.defaultTextField()
            
            user.userName = usernameTextField.text!
            user.firstName = nameTextField.text!
            user.email = emailTextField.text!
            user.password = passwordTextField.text!
            
            APIManager.sharedManager.signUp(with: user, imageData: nil, fileName: "", apiCompletion: { (responseValue, error) in
                
                if error == nil {
                    User.currentUser = responseValue
                    self.signupButton.stopAnimation(animationStyle: .expand, completion: {
                        self.presentHomeViewController()
                    })
                } else {
                    self.signupButton.stopAnimation(animationStyle: .shake, completion: {
                        self.showAlert(text: (error)!)
                    })
                }
                
                self.dismissProgressHud()
                
            }, progressHandler: { progress in
//                self.showProgressHud(progress: Float(progress.fractionCompleted))
            })
        }
    }
}

extension SignUpViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Try to find next responder
        let nextTextField = view.allSubViewsOf(type: UITextField.self).filter{$0.tag == textField.tag + 1}.first
        
        if let nextField = nextTextField {
            nextField.becomeFirstResponder()
        } else {
            // Not found, so remove keyboard.
            textField.resignFirstResponder()
            sendSignUpRequest()
        }
        // Do not add a line break
        return true
    }
}
