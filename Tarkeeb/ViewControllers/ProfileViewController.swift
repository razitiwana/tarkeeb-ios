//
//  ProfileViewController.swift
//  Tarkeeb
//
//  Created by Razi Tiwana on 05/09/2018.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import UIKit

enum UserProfilePostType: Int {
    case userPosts
    case savedPosts
}

enum ProfileViewControllerSections: Int {
    case details
    case posts
    static var count :Int {return ProfileViewControllerSections.posts.hashValue + 1}
}

class ProfileViewController: ViewController {

    var user = User.currentUser {
        didSet {
            self.title = user?.userName
        }
    }
    
    var postType = UserProfilePostType.userPosts
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var optionsBarButtonItem: UIBarButtonItem!
    
    
    private var posts:[Recipe] = [Recipe]()
    
    private var savedPosts:[Recipe] = [Recipe]()
    private var userPosts:[Recipe] = [Recipe]()
    
    private let refreshControl = UIRefreshControl()
    
    var shadowImage: UIImage?
    var backgroundImage: UIImage?
    
    var fetchingMore = false
    
    var moreSavedPostRemaining = true
    var moreUserPostRemaining = true
    
    var userPostStartIndex = 0
    var savedPostStartIndex = 0
    
    var count = Constants.countToFetchAtOneTime
    
    
    // MARK: - ViewController LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
         
        configureView()
        fetchPosts()
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        navigationController?.setNavigationBarHidden(true, animated: false)
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
//        navigationController?.setNavigationBarHidden(false, animated: false)
        super.viewWillDisappear(animated)
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    
    // MARK: - Action Methods
    
    @IBAction func onClickOptionBarButton(_ sender: UIBarButtonItem) {
        openSettings()
    }
    
    
    // MARK: - Private Methods
    
    func configureView() {
        configureTableView()
        
//        self.navigationItem.setHidesBackButton(true, animated: false);
        
        if !(user!.isOwnProfile) {
            self.navigationItem.rightBarButtonItem = nil
        }
        
        if let user = user {
            self.title = user.userName
        }
    }
        
    func configureTableView() {
        // To remove the extra cells
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = UIColor.tableViewBackgroundColor()
        
        tableView.register(UINib(nibName: "RecipeTableViewCell", bundle: nil), forCellReuseIdentifier: "RecipeTableViewCell")
        
         tableView.register(UINib(nibName: "ProfileDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "ProfileDetailTableViewCell")
        
        // Setup refresh Control
        tableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
        
        
        // Other wise the scroll inset are changeed when reloading data
        tableView.estimatedRowHeight = CGFloat(Constants.estimatedRecipeCellSize)
    }
    
    func showNoRecordsLabel(isPullToRefresh: Bool = false) {
        hideNoRecordsLabel()
        
        if (posts.count != 0 ) {
            return
        }
        
        let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: tableView.bounds.maxY/2, width: tableView.bounds.size.width, height: tableView.bounds.size.height/2))
        noDataLabel.backgroundColor = .clear
        noDataLabel.text          = ""
        noDataLabel.textColor     = UIColor.black
        noDataLabel.textAlignment = .center
        noDataLabel.tag = 10
        
        switch postType {
        case .userPosts:
            noDataLabel.text          = "No recipes found"
            break
        case .savedPosts:
            noDataLabel.text          = "Your saved recipes will appear here"
            break
        }
        
        tableView.addSubview(noDataLabel)
        tableView.separatorStyle  = .none
    }
    
    func hideNoRecordsLabel() {
        for view in tableView.subviews {
            if view.tag == 10 {
                view.removeFromSuperview()
            }
        }
    }
    
    @objc func pullToRefresh() {
        userPostStartIndex = 0
        savedPostStartIndex = 0
        
        moreUserPostRemaining = true
        moreSavedPostRemaining = true
        
        fetchingMore = false
        fetchPosts()
    }
    
    func fetchPosts() {
        fetchUserDetail()
        
        switch postType {
        case .userPosts:
            fetchUserPosts()
            break
        case .savedPosts:
            fetchSavedPosts()
            break
        }
    }
    
    @objc func fetchUserPosts() {
        hideNoRecordsLabel()
        tableView.tableFooterView = loadingView()
        
        APIManager.sharedManager.fetchPosts(of: user!, startIndex: userPostStartIndex, count: count) { (posts, error) in
            let isPullToRefresh = self.refreshControl.isRefreshing
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                self.refreshControl.endRefreshing()
            }
            
            if error == nil, let posts = posts {
                if self.userPostStartIndex > 0 {
                    self.userPosts.append(contentsOf: posts)
                }
                else {
                    self.userPosts = posts
                }
                
                self.posts = self.userPosts
                
                // If the count of the fectehed posts is lesss than the count send then that means there are no more items left to fetch from data base
                self.moreUserPostRemaining = (posts.count) >= self.count
                
                let previousContentOfset = self.tableView.contentOffset
                
                self.fetchingMore = false
                
                self.tableView.reloadSections([ProfileViewControllerSections.posts.rawValue], with: .automatic)
                
                // if isPullToRefresh then it would be stuck in the pulled state if the service responds quickly
                if posts.count > 0 && !isPullToRefresh {
                    self.tableView.setContentOffset(previousContentOfset, animated: true)
                }
                
                self.showNoRecordsLabel(isPullToRefresh: isPullToRefresh)
            } else {
                self.showAlert(text: error!)
            }
            
            self.tableView.tableFooterView = nil
        }
    }
    
    @objc func fetchSavedPosts() {
        hideNoRecordsLabel()
        tableView.tableFooterView = loadingView()
        
        APIManager.sharedManager.fetchUserSavedRecipies(withStartIndex: savedPostStartIndex, count: count) { (posts, error) in
            let isPullToRefresh = self.refreshControl.isRefreshing
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                self.refreshControl.endRefreshing()
            }
            
            if error == nil, let posts = posts {
                if self.savedPostStartIndex > 0 {
                    self.savedPosts.append(contentsOf: posts)
                }
                else {
                    self.savedPosts = posts
                }
                
                self.posts = self.savedPosts
                
                // If the count of the fectehed posts is lesss than the count send then that means there are no more items left to fetch from data base
                self.moreSavedPostRemaining = (posts.count) >= self.count
                
                let previousContentOfset = self.tableView.contentOffset
                
                self.fetchingMore = false
                
                self.tableView.reloadSections([ProfileViewControllerSections.posts.rawValue], with: .automatic)
                
                // if isPullToRefresh then it would be stuck in the pulled state if the service responds quickly
                if posts.count > 0 && !isPullToRefresh {
                    self.tableView.setContentOffset(previousContentOfset, animated: true)
                }
                
                self.showNoRecordsLabel(isPullToRefresh: isPullToRefresh)
            } else {
                self.showAlert(text: error!)
            }
            
            self.tableView.tableFooterView = nil
        }
    }
    
    func fetchUserDetail() {
        APIManager.sharedManager.fetchDetail(for: user!) { (user, error) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
//                self.refreshControl.endRefreshing()
            }
            
            if error == nil {
                
                if (user?.isOwnProfile)! {
                    User.currentUser = user
                }
                
                self.user = user
                self.tableView.reloadData()
                
            } else {
                self.showAlert(text: error!)
            }
        }
    }
    
    func beginBatchFetch() {
        print("Fetching")
        
        fetchingMore = true
        
        switch postType {
        case .userPosts:
            userPostStartIndex += count
            break
        case .savedPosts:
            savedPostStartIndex += count
            break
        }
        
        fetchPosts()
    }
}

extension ProfileViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return ProfileViewControllerSections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var numberOfRows = 0
        
        switch ProfileViewControllerSections(rawValue: section)! {
        case .details:
            numberOfRows = 1
            break
        case .posts:
            if posts.count > 0 {
                numberOfRows = (posts.count)
//               hideNoRecordsLabel()
            }
//            else {
//                showNoRecordsLabel()
//            }
           
            break
        }
        return numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cellToReturn: UITableViewCell?
        
        switch ProfileViewControllerSections(rawValue: indexPath.section)! {
        case .details:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileDetailTableViewCell") as! ProfileDetailTableViewCell
            
            cell.configureCell(with: user!)
            
            // As of now hiding the back view because the back button is moved to the navigation bar
            // in case own profile and its on root
            cell.backView.isHidden = true //isRootViewController
            cell.backImageView.isHidden = true // isRootViewController
            
            cell.delegate = self
            cellToReturn = cell
           
            break
        case .posts:
            let cell = tableView.dequeueReusableCell(withIdentifier: "RecipeTableViewCell") as! RecipeTableViewCell
            
            let selectedPost = posts[indexPath.row]
            
            cell.configureCell(with: selectedPost)
            cell.delegate = self
            cellToReturn = cell
            break
        }
        
        cellToReturn?.selectionStyle = .none
        
        return cellToReturn!
    }
}

extension ProfileViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch ProfileViewControllerSections(rawValue: indexPath.section)! {
        case .details:
            break
        case .posts:
            let selectedPost = posts[indexPath.row]
            posts[indexPath.row].postViews += 1
            
            let viewController = StoryboardRouter.recipeDetailViewController()
            viewController.post = selectedPost
            
            navigationController?.pushViewController(viewController, animated: true)
            
            tableView.reloadData()
            break
        }
      
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let moreRemaining = (self.postType == .userPosts) ? moreUserPostRemaining : moreSavedPostRemaining
        
        if self.posts.count >= count && moreRemaining {
            
            let currentOffset = scrollView.contentOffset.y;
            let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
            
            // if the there are elements less than size of tableview
            if (scrollView.contentSize.height > self.tableView.frame.size.height) {
                
                // In some cases the curent offset is greater than the maximum one which makes no sense, so a hack would be to add this check
                // The 100 if the user pulls the table view it should still load
                if maximumOffset + 100 > currentOffset {
                    // Change 10.0 to adjust the distance from bottom
                    if (maximumOffset - currentOffset <= 10.0 ) {
                        if !self.fetchingMore {
                            tableView.tableFooterView = loadingView()
                            scrollView.setContentOffset(CGPoint(x: 0, y: maximumOffset + 100), animated: true)
                            
                            self.beginBatchFetch()
                        }
                    }}
            }
        }
    }
    
}

extension ProfileViewController: RecipeTableViewCellDelegate {
    
    func like(post: Recipe) {
        
        for index in 0..<posts.count {
            if post.postID == posts[index].postID {
                let oldState = posts[index].isLiked
                let oldnumberOfLikes = posts[index].numberOflikes
                
                APIManager.sharedManager.likePost(with: posts[index]) { (sucessMessage, error) in
                    if error != nil {
                        self.posts[index].isLiked = oldState
                        self.posts[index].numberOflikes = oldnumberOfLikes
                        
                        switch self.postType {
                        case .userPosts:
                            self.userPosts[index].isLiked = oldState
                            self.userPosts[index].numberOflikes = oldnumberOfLikes
                            break
                        case .savedPosts:
                            self.savedPosts[index].isLiked = oldState
                            self.savedPosts[index].numberOflikes = oldnumberOfLikes
                            break
                        }
                        
                        self.tableView.reloadSections([ProfileViewControllerSections.posts.rawValue], with: .automatic)
                    } else {
                        
                    }
                }
                
                posts[index].isLiked = !posts[index].isLiked
                posts[index].numberOflikes += oldState ? -1 : 1
                
                // Also changeing the state of the arrays
                switch self.postType {
                case .userPosts:
                    userPosts[index].isLiked = !userPosts[index].isLiked
                    userPosts[index].numberOflikes += oldState ? -1 : 1
                    break
                case .savedPosts:
                    savedPosts[index].isLiked = !savedPosts[index].isLiked
                    savedPosts[index].numberOflikes += oldState ? -1 : 1
                    break
                }
                
                tableView.reloadSections([ProfileViewControllerSections.posts.rawValue], with: .automatic)
                break
            }
        }
    }
    
    func share(post: Recipe) {
        showAlert(text: "Comming Soon!")
    }
    
    func save(post: Recipe) {
        
        for index in 0..<posts.count {
            if post.postID == posts[index].postID {
                let oldState = posts[index].isSaved
                
                APIManager.sharedManager.savePost(with: posts[index]) { (sucessMessage, error) in
                    if error != nil {
                        self.posts[index].isSaved = oldState
                        
                        switch self.postType {
                        case .userPosts:
                            self.userPosts[index].isSaved = oldState
                            break
                        case .savedPosts:
                            self.savedPosts[index].isSaved = oldState
                            break
                        }
                        
                        self.tableView.reloadSections([ProfileViewControllerSections.posts.rawValue], with: .automatic)
                    } else {
                        self.showDoneProgressHud()
                    }
                }
                
                posts[index].isSaved = !posts[index].isSaved
                
                switch postType {
                case .userPosts:
                    userPosts[index].isSaved = !userPosts[index].isSaved
                    break
                case .savedPosts:
                    savedPosts[index].isSaved = !savedPosts[index].isSaved
                    break
                }
                
                tableView.reloadSections([ProfileViewControllerSections.posts.rawValue], with: .automatic)
                break
            }
        }
    }
    
    func openDetail(for user: User) {
        let viewController = StoryboardRouter.profileViewController()
        viewController.user = user
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func openSettings() {
        let viewController = StoryboardRouter.settingsViewController()
        navigationController?.pushViewController(viewController, animated: true)
    }
}

extension ProfileViewController: ProfileDetailTableViewCellDelegate {
    
    func openFollowing(of user: User) {
        
        let viewController = StoryboardRouter.followersAndFollowingViewController()
        viewController.sectionToDisplay = FollowType.following
        viewController.user = user
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func openFollower(of user: User) {
        
        let viewController = StoryboardRouter.followersAndFollowingViewController()
        viewController.sectionToDisplay = FollowType.followers
         viewController.user = user
        
        navigationController?.pushViewController(viewController, animated: true)

    }
    
    func openCaps(of user: User) {
        
    }
    
    func openEdit() {
        let navController = StoryboardRouter.EditProfileNavigationViewController()
        self.present(navController, animated: true)
    }
    
    func changeCover(for user: User) {
        
    }
    
    func changeProfile(for user: User) {
        
    }
    
    func follow(user: User) {
        let oldState = user.isFollowed
        
        APIManager.sharedManager.follow(user: user) { (sucessMessage, error) in
            if error != nil {
                self.user?.isFollowed = oldState
                self.tableView.reloadData()
            } else {
            }
        }
        
        self.user?.isFollowed = true
        tableView.reloadData()
    }
    
    func unFollow(user: User) {
        let alertController = UIAlertController(title: "Are You sure you want to unfollow " + user.userName! + "?", message: "", preferredStyle: .actionSheet)
        
        let unfollowAction = UIAlertAction(title: "Unfollow" , style: .destructive) { unfollowAction in
            let oldState = user.isFollowed
            
            APIManager.sharedManager.follow(user: user) { (sucessMessage, error) in
                if error != nil {
                    self.user?.isFollowed = oldState
                    self.tableView.reloadData()
                } else {
                }
            }
            
            self.user?.isFollowed = false
            self.tableView.reloadData()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alertController.addAction(unfollowAction)
        alertController.addAction(cancelAction)
        alertController.view.tintColor = .black
            
        present(alertController, animated: true, completion: nil)
    }
    
    func back() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func postSelected(with type: UserProfilePostType) {
        if postType != type {
            postType = type
            
            switch postType {
            case .userPosts:
                if userPosts.count == 0 {
                    fetchUserPosts()
                }
                // there may be local changes like increase of likes , saved state and liked state
                savedPosts = posts
                posts = userPosts
                
                break
            case .savedPosts:
                if savedPosts.count == 0 {
                    fetchSavedPosts()
                }
                // there may be local changes like increase of likes , saved state and liked state
                userPosts = posts
                posts = savedPosts
                
                break
            }
            
            hideNoRecordsLabel()
            
            tableView.reloadSections([ProfileViewControllerSections.posts.rawValue], with: .automatic)
        }
    }
}
