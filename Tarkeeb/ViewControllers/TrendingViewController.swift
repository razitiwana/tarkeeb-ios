//
//  TrendingViewController.swift
//  Tarkeeb
//
//  Created by Razi Tiwana on 16/09/2018.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import UIKit

enum TrendingViewControllerRows : Int  {
    case trendingRecipes
    case trendingChefs
    static var count :Int {return TrendingViewControllerRows.trendingChefs.hashValue + 1}
}

class TrendingViewController: ViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var trendingRecipes: [Recipe] = []
    var trendingChefs: [User] = []
    
    private let refreshControl = UIRefreshControl()
    
    // MARK: - ViewController LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureSearchBar()
        configureTableView()
        configurerefreshControl()
        
        fetchTrendingData()
//
    }
    
    // On tap to root if some controoler has navigation bar hidden it will remain hidden
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: false)
        super.viewWillAppear(animated)
        refreshControl.beginRefreshing()
    }

    
    // MARK: - Private Methods
    
    func configureTableView() {
        // To remove the extra cells
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = UIColor.tableViewBackgroundColor()
        
        // Other wise the scroll inset are changeed when reloading data
        tableView.estimatedRowHeight = 44.0
    }
    
    func configurerefreshControl() {
        // Setup refresh Control
        tableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(fetchTrendingData), for: .valueChanged)
    }
    
    func configureSearchBar() {
        
        let viewController = StoryboardRouter.searchViewController()
        //let navController = UINavigationController(rootViewController: viewController)
        //navController.navigationBar.isHidden = true
        let searchController = UISearchController(searchResultsController: viewController)

        searchController.searchBar.delegate = viewController
        searchController.searchResultsUpdater = viewController
//        searchController.hidesNavigationBarDuringPresentation = false
        searchController.dimsBackgroundDuringPresentation = false
//        searchController.searchBar.delegate = viewController
        
        self.definesPresentationContext = true
        
        if #available(iOS 11.0, *) {
            navigationItem.searchController = searchController
            navigationItem.hidesSearchBarWhenScrolling = false
        } else {
            // Fallback on earlier versions
             self.tableView.tableHeaderView = searchController.searchBar
        }
    }
    
    @objc func fetchTrendingData() {
        
        APIManager.sharedManager.fetchTrendingData() { (searchResults, error) in
            self.refreshControl.endRefreshing()
            
            if error == nil {
                self.trendingRecipes = (searchResults?.recipies)!
                self.trendingChefs = (searchResults?.chefs)!
                self.tableView.reloadData()
                self.removeRefreshButton()
            } else {
                self.showAlert(text: error!)
                self.addRefreshButton()
            }
        }
    }
    
    func addRefreshButton() {
        let refresh = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(TrendingViewController.fetchTrendingData))
        self.navigationItem.rightBarButtonItem = refresh
    }
    
    func removeRefreshButton() {
        self.navigationItem.rightBarButtonItem = nil
    }
    
}

extension TrendingViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TrendingViewControllerRows.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cellToReturn: TrendingTableViewCell?
        
        switch TrendingViewControllerRows(rawValue: indexPath.row)! {
        case .trendingRecipes:
            
            if trendingRecipes.count != 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "TrendingTableViewCellRecipes") as! TrendingTableViewCell
                cell.collectionView.backgroundView = nil
                cell.trendingRecipes = trendingRecipes
                cell.cellType = .trendingRecipes
                cell.reloadData()
                cellToReturn = cell
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "TrendingTableViewCellRecipes") as! TrendingTableViewCell
                let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
                noDataLabel.text          = "No recipes found"
                noDataLabel.textColor     = UIColor.black
                noDataLabel.textAlignment = .center
                cell.collectionView.backgroundView  = noDataLabel
                cellToReturn = cell
                //tableView.separatorStyle  = .none
            }
           
            break
        case .trendingChefs:
            if trendingChefs.count != 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "TrendingTableViewCellChefs") as! TrendingTableViewCell
                cell.collectionView.backgroundView = nil
                cell.trendingChefs = trendingChefs
                cell.cellType = .trendingChefs
                cell.delegateTendingChefCollectionViewCell = self
                cell.reloadData()
                cellToReturn = cell
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "TrendingTableViewCellRecipes") as! TrendingTableViewCell
                let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
                noDataLabel.text          = "No chefs found"
                noDataLabel.textColor     = UIColor.black
                noDataLabel.textAlignment = .center
                cell.collectionView.backgroundView  = noDataLabel
                cellToReturn = cell
                //tableView.separatorStyle  = .none
            }
            
            break
        }
        
        cellToReturn?.delegate = self
        cellToReturn!.selectionStyle = .none
        
        return cellToReturn!
    }
}

extension TrendingViewController: UITableViewDelegate {
    
}

extension TrendingViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        print("dasdas")
    }
}

extension TrendingViewController: RecipeTableViewCellDelegate {
    
    func like(post: Recipe) {
        
        for index in 0..<trendingRecipes.count {
            if post.postID == trendingRecipes[index].postID {
                let oldState = trendingRecipes[index].isLiked
                
                APIManager.sharedManager.likePost(with: trendingRecipes[index]) { (sucessMessage, error) in
                    if error != nil {
                        self.trendingRecipes[index].isLiked = oldState
                        self.trendingRecipes[index].numberOflikes += -1
                        self.tableView.reloadData()
                    } else {
                        
                    }
                }
                
                trendingRecipes[index].isLiked = !trendingRecipes[index].isLiked
                
                trendingRecipes[index].numberOflikes += oldState ? -1 : 1
                
                tableView.reloadData()
                break
            }
        }
    }
    
    func openDetail(for post: Recipe) {
        let viewController = StoryboardRouter.recipeDetailViewController()
        viewController.post = post
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func openDetail(for user: User) {
        let viewController = StoryboardRouter.profileViewController()
        viewController.user = user
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
}


extension TrendingViewController: TrendingChefCollectionViewCellDelegate {
    
    func follow(user: User) {
        if user.isFollowed {
            
            let alertController = UIAlertController(title: "Are You sure you want to unfollow " + user.userName! + "?", message: "", preferredStyle: .actionSheet)
            
            let unfollowAction = UIAlertAction(title: "Unfollow" , style: .destructive) { unfollowAction in
                self.changeFollowState(user: user)
            }
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            
            alertController.addAction(unfollowAction)
            alertController.addAction(cancelAction)
            alertController.view.tintColor = .black
            
            present(alertController, animated: true, completion: nil)
        } else {
            changeFollowState(user: user)
        }
    }
    
    func changeFollowState(user: User) {
        let oldState = user.isFollowed
        
        
        let selectedIndex = trendingChefs.indices.filter { (index) -> Bool in
            return trendingChefs[index].userID == user.userID
        }
        
        APIManager.sharedManager.follow(user: user) { (sucessMessage, error) in
            if error != nil {
                self.trendingChefs[selectedIndex.first!].isFollowed = oldState
                self.tableView.reloadData()
            } else {
            }
        }
        
        //self.user?.isFollowed = true
        trendingChefs[selectedIndex.first!].isFollowed = !oldState
        
        tableView.reloadData()
    }
    
}
