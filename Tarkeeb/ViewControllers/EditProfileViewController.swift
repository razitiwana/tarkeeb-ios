//
//  EditProfileViewController.swift
//  Tarkeeb
//
//  Created by Osama Azmat Khan on 12/8/18.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import UIKit
import Photos
import TransitionButton

enum EditProfileMainSections: Int {
    case topHeader
    case name
    case userName
    case bio
    case location
    case gender
    case email
    static var count :Int {return EditProfileMainSections.email.hashValue + 1}
}

enum EditProfilePictures: Int {
    case profilePicture
    case coverPicture
    static var count :Int {return EditProfilePictures.coverPicture.hashValue + 1}
}

class EditProfileViewController: ViewController {
    
    // MARK: - Outlets
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var editProfileTransitionButton: TransitionButton!
    
    
    // MARK: - Variables
    
    var user: User?
    var imagePicker = UIImagePickerController()
    var selectedProfileImage: UIImage?
    var selectedCoverImage: UIImage?
    var pictureTypeSelected: EditProfilePictures = EditProfilePictures.profilePicture
    
    var profilePictureUploaded: Bool = true
    var coverImageUploaded: Bool = true
    var userInfoUpdated: Bool = true
    
    // MARK: - ViewController LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let user = User.currentUser {
            self.user = user
        }
        
        configureTableView()
        configureTransitionButton()

    }
    
    
    // MARK: - Configure Methods
    
    func configureTransitionButton() {
        editProfileTransitionButton.dropShadow()
        editProfileTransitionButton.setTitle("Update", for: .normal)
        editProfileTransitionButton.setTitleColor(UIColor.appThemeColor(), for: .normal)
        editProfileTransitionButton.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: .medium)
        editProfileTransitionButton.spinnerColor = UIColor.appThemeColor()
        editProfileTransitionButton.cornerRadius1 = editProfileTransitionButton.frame.height / 2
        
        editProfileTransitionButton.addTarget(self, action: #selector(buttonAction(_:)), for: .touchUpInside)
    }
    
    func configureTableView() {
        
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = UIColor.tableViewBackgroundColor()
        
        tableView.register(UINib(nibName: "EditProfileTopHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "EditProfileTopHeaderTableViewCell")
        tableView.register(UINib(nibName: "EditProfileNameTableViewCell", bundle: nil), forCellReuseIdentifier: "EditProfileNameTableViewCell")
        tableView.register(UINib(nibName: "EditProfileUserNameTableViewCell", bundle: nil), forCellReuseIdentifier: "EditProfileUserNameTableViewCell")
        tableView.register(UINib(nibName: "EditProfileBioTableViewCell", bundle: nil), forCellReuseIdentifier: "EditProfileBioTableViewCell")
        tableView.register(UINib(nibName: "EditProfileLocationTableViewCell", bundle: nil), forCellReuseIdentifier: "EditProfileLocationTableViewCell")
        tableView.register(UINib(nibName: "EditProfileTopHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "EditProfileTopHeaderTableViewCell")
        tableView.register(UINib(nibName: "EditProfileGenderTableViewCell", bundle: nil), forCellReuseIdentifier: "EditProfileGenderTableViewCell")
        tableView.register(UINib(nibName: "EditProfileEmailTableViewCell", bundle: nil), forCellReuseIdentifier: "EditProfileEmailTableViewCell")
        
        // Other wise the scroll inset are changeed when reloading data
        tableView.estimatedRowHeight = CGFloat(Constants.estimatedRecipeCellSize)
        
    }
    
    
    // MARK: - Private Methods
    
    func getGenderArray() -> [String] {
        let arrayGender: [String] = [Gender.male.rawValue,
                                     Gender.female.rawValue,
                                     Gender.notSpecified.rawValue,
                                     Gender.other.rawValue]
        return arrayGender
    }
    
    func dispatchAllUpdates() {
        self.editProfileTransitionButton.startAnimation()
        
        if selectedProfileImage != nil {
            profilePictureUploaded = false
            updateProfileImage()
        }
        
        if selectedCoverImage != nil {
            coverImageUploaded = false
            updateCoverImage()
        }
        
        if (self.user?.fullName() != User.currentUser?.fullName() || self.user?.userBio != User.currentUser?.userBio ||
            self.user?.location != User.currentUser?.location || self.user?.gender != User.currentUser?.gender ||
            self.user?.email != User.currentUser?.email) {
            userInfoUpdated = false
            updateUser()
        }
        
        //fetchUserDetail()
    }
    
    
    // MARK: - Action Methods
    
    @IBAction func buttonAction(_ button: TransitionButton) {
        dispatchAllUpdates()
    }
    
    @IBAction func onClickCancelBarButtonItem(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Service Calls
    
    func fetchUserDetail() {
        APIManager.sharedManager.fetchDetail(for: user!) { (user, error) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                //                self.refreshControl.endRefreshing()
            }
            
            if error == nil {
                
                if (user?.isOwnProfile)! {
                    User.currentUser = user
                }
                
                self.tableView.reloadData()
            } else {
                self.showAlert(text: error!)
            }
        }
    }
    
    func updateCoverImage() {
        
        let data = UIImagePNGRepresentation(selectedCoverImage!)
        
        APIManager.sharedManager.updateUserCoverImage(with: data, fileName: "CoverImage.jpg", apiCompletion: { (responseValue, error) in
            
            if error == nil {
                self.coverImageUploaded = true
                if self.coverImageUploaded && self.userInfoUpdated {
                    self.editProfileTransitionButton.stopAnimation(animationStyle: .expand, completion: {
                        self.dismiss(animated: true, completion: nil)
                    })
                }
            } else {
                self.profilePictureUploaded = false
                self.showAlert(text: "Error:" + "\(error)")
                self.editProfileTransitionButton.stopAnimation(animationStyle: .shake, completion: nil)
            }
            
        }, progressHandler: {(progress) in
            if #available(iOS 11.0, *) {
                print(progress.fileCompletedCount ?? 0)
            } else {
                // Fallback on earlier versions
            }
        })
        
    }
    
    func updateProfileImage() {
        
        let data = UIImagePNGRepresentation(selectedProfileImage!)
        
        APIManager.sharedManager.updateUserImage(with: data, fileName: "ProfilePicture.jpg", apiCompletion: { (responseValue, error) in
            
            if error == nil {
                 self.profilePictureUploaded = true
                if self.profilePictureUploaded && self.userInfoUpdated {
                    self.editProfileTransitionButton.stopAnimation(animationStyle: .expand, completion: {
                        self.dismiss(animated: true, completion: nil)
                    })
                }
            } else {
                self.profilePictureUploaded = false
                self.showAlert(text: "Error:" + "\(String(describing: error))")
                self.editProfileTransitionButton.stopAnimation(animationStyle: .shake, completion: nil)
            }
            
        }, progressHandler: {(progress) in
            if #available(iOS 11.0, *) {
                print(progress.fileCompletedCount ?? 0)
            } else {
                // Fallback on earlier versions
            }
        })
        
    }
    
    func updateUser() {
        APIManager.sharedManager.editProfile(for: self.user!) {
            (responseValue, error) in
            
            if error == nil {
                self.userInfoUpdated = true
                if self.profilePictureUploaded && self.coverImageUploaded {
                    self.editProfileTransitionButton.stopAnimation(animationStyle: .expand, completion: {
                        self.fetchUserDetail()
                        self.dismiss(animated: true, completion: nil)
                    })
                }
            } else {
                self.userInfoUpdated = false
                self.showAlert(text: "Error:" + "\(String(describing: error))")
                self.editProfileTransitionButton.stopAnimation(animationStyle: .shake, completion: nil)
            }
        }
    }
}

extension EditProfileViewController: UITableViewDataSource {
    
    
    // MARK: - Main Table View DataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return EditProfileMainSections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var numberOfRows = 0
        switch EditProfileMainSections(rawValue: section)! {
        case .topHeader:
            numberOfRows = 1
            break
        case .name:
            numberOfRows = 1
            break
        case .userName:
            numberOfRows = 1
            break
        case .bio:
            numberOfRows = 1
            break
        case .location:
            numberOfRows = 1
            break
        case .gender:
            numberOfRows = 1
            break
        case .email:
            numberOfRows = 1
            break
        }
        return numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellToReturn: UITableViewCell?
        
        switch EditProfileMainSections(rawValue: indexPath.section)! {
        case .topHeader:
            let cell = tableView.dequeueReusableCell(withIdentifier: "EditProfileTopHeaderTableViewCell") as! EditProfileTopHeaderTableViewCell
            
            if selectedProfileImage != nil {
                cell.profilePictureImageView.image = self.selectedProfileImage
            }
            else {
                if let userPicture = user?.userPicture {
                    cell.profilePictureImageView.kf.setImage(with: URL(string:userPicture), placeholder: UIImage(named:"ic_profile_placeholder"), options: nil, progressBlock: nil, completionHandler: nil)
                }
                else {
                     cell.profilePictureImageView.image = UIImage(named:"ic_profile_placeholder")
                }
            }
        
            if selectedCoverImage != nil {
                cell.coverPictureImageView.image = self.selectedCoverImage
            }
            else {
                if let coverPicture = user?.coverImage {
                    cell.coverPictureImageView.kf.setImage(with: URL(string:coverPicture), placeholder: UIImage(named:"ic_cover_placeholder"), options: nil, progressBlock: nil, completionHandler: nil)
                }
                else {
                    cell.coverPictureImageView.image = UIImage(named:"ic_cover_placeholder")
                }
            }
            
            
            cell.delegate = self
            cellToReturn = cell
            
            break
        case .name:
            let cell = tableView.dequeueReusableCell(withIdentifier: "EditProfileNameTableViewCell") as! EditProfileNameTableViewCell
            
            if self.user?.firstName != nil {
                cell.configureTextField(name: user?.firstName, selectedTextFieldLocal: EditProfileNameTableViewCellSelectedTextField.firstName )
            }
            
            if self.user?.lastName != nil {
                cell.configureTextField(name: user?.lastName, selectedTextFieldLocal: EditProfileNameTableViewCellSelectedTextField.lastName )
            }
            
            cell.delegate = self
            cellToReturn = cell
            break
        case .userName:
            let cell = tableView.dequeueReusableCell(withIdentifier: "EditProfileUserNameTableViewCell") as! EditProfileUserNameTableViewCell
            
            if user?.userName != nil {
                cell.configureTextField(userName: user?.userName)
            }
            cell.textField?.isEnabled = false
            cell.delegate = self
            cellToReturn = cell
            break
        case .bio:
            let cell = tableView.dequeueReusableCell(withIdentifier: "EditProfileBioTableViewCell") as! EditProfileBioTableViewCell
            
            if user?.userBio != nil {
                cell.mainTextView.text = user?.userBio
            }
            else
            {
                cell.mainTextView.text = "User Bio"
                cell.mainTextView.textColor = UIColor.lightGray
            }
            
            cell.delegate = self
            cell.sectionToSend = indexPath.section
            cellToReturn = cell
            break
        case .location:
            let cell = tableView.dequeueReusableCell(withIdentifier: "EditProfileLocationTableViewCell") as! EditProfileLocationTableViewCell
            
            if user?.location != nil {
                cell.configureTextField(location: user?.location)
            }
            
            cell.delegate = self
            cellToReturn = cell
            break
        case .gender:
            let cell = tableView.dequeueReusableCell(withIdentifier: "EditProfileGenderTableViewCell") as! EditProfileGenderTableViewCell
            
            if user?.gender != nil {
                cell.configureTextField(gender: user?.gender.rawValue)
            }
            
            cell.delegate = self
            cellToReturn = cell
            break
        case .email:
            let cell = tableView.dequeueReusableCell(withIdentifier: "EditProfileEmailTableViewCell") as! EditProfileEmailTableViewCell
            
            if user?.email != nil {
                cell.configureTextField(email: user?.email)
            }
            
            cell.delegate = self
            cellToReturn = cell
            break
        }
        
        cellToReturn?.selectionStyle = .none
        return cellToReturn!
    }
    
}

extension EditProfileViewController: UITableViewDelegate {
    
    
    // MARK: - Main Table View Delegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
}

extension EditProfileViewController: EditProfileTopHeaderTableViewCellDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate  {
    
    
    // MARK: - Image Picker Delegate
    
    func pickCoverImage() {
        
        pictureTypeSelected = EditProfilePictures.coverPicture
        selectedCoverImage = nil
        
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Button capture")
            
            checkPermission()
            
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum;
            imagePicker.allowsEditing = false
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func pickProfileImage() {
        pictureTypeSelected = EditProfilePictures.profilePicture
        selectedProfileImage = nil
        
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Button capture")
            
            checkPermission()
            
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum;
            imagePicker.allowsEditing = false
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func checkPermission() {
        let photoAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        
        switch photoAuthorizationStatus {
        case .authorized: print("Access is granted by user")
        case .notDetermined: PHPhotoLibrary.requestAuthorization({
            (newStatus) in print("status is \(newStatus)")
            if newStatus == PHAuthorizationStatus.authorized {
                print("success") }
        })
            
        case .restricted:  print("User do not have access to photo album.")
        case .denied:  print("User has denied the permission.")
        }
        
    }
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        self.dismiss(animated: true, completion: { () -> Void in
            
        })
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        if pictureTypeSelected == EditProfilePictures.profilePicture {
            selectedProfileImage = chosenImage
        }
        else {
            selectedCoverImage = chosenImage
        }
        self.tableView.reloadData()
    }
    
}

extension EditProfileViewController: EditProfileGenderTableViewCellDelegate {
    
    
    // MARK: - Gender Cell Delegate
    
    func openGenderPickerView(delegate: GenericPickerViewControllerDelegate) {
        let viewController = StoryboardRouter.GenericPickerViewController()
        viewController.doneButtonTitle = "Done"
        viewController.pickerViewTitle = "Gender"
        viewController.stringArray = getGenderArray()
        viewController.delegate = delegate
        self.present(viewController, animated: true)
    }
    
    func updateGender(gender: String) {
        
        switch Gender(rawValue: gender)! {
        case .male:
            self.user?.gender = Gender.male
            break
        case .female:
            self.user?.gender = Gender.female
            break
        case .notSpecified:
            self.user?.gender = Gender.notSpecified
            break
        case .other:
            self.user?.gender = Gender.other
            break
        }
        
        self.tableView.reloadData()
    }
    
}

extension EditProfileViewController: EditProfileBioTableViewCellDelegate {
    
    
    // MARK: - Bio Text View Delegate
    
    func updateTextView(bioText: String, section: Int) {
        // Disabling animations gives us our desired behaviour
        UIView.setAnimationsEnabled(false)
        /* These will causes table cell heights to be recaluclated,
         without reloading the entire cell */
        tableView.beginUpdates()
        
        self.user?.userBio = bioText
        
        tableView.endUpdates()
        // Re-enable animations
        UIView.setAnimationsEnabled(true)
        
        let indexPath = IndexPath(row: 0, section: section)
        tableView.scrollToRow(at: indexPath, at: .bottom, animated: false)
    }
    
}

extension EditProfileViewController: EditProfileEmailTableViewCellDelegate, EditProfileNameTableViewCellDelegate, EditProfileUserNameTableViewCellDelegate, EditProfileLocationTableViewCellDelegate {
    
    
    // MARK: - Text Fields Delegates
    
    func updateFirstName(text: String) {
        self.user?.firstName = text
    }
    
    func updateLastName(text: String) {
        self.user?.lastName = text
    }
    
    func updateUserName(text: String) {
        self.user?.userName = text
    }
    
    func updateEmail(text: String) {
        self.user?.email = text
    }

    func updateLocation(text: String) {
        self.user?.location = text
    }
    
}





