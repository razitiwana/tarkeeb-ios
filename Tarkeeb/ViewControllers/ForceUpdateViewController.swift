//
//  ForceUpdateViewController.swift
//  Tarkeeb
//
//  Created by Osama Azmat Khan on 10/5/18.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import UIKit

class ForceUpdateViewController: ViewController {

    
    // MARK: - Outlets
    
    @IBOutlet weak var cancelView: UIView!
    @IBOutlet weak var updateButton: UIButton!
    
    
    // MARK: - Variables
    
    var updateInfo: ForceUpdate?
    var forceUpdate = false
    
    
    // MARK: - ViewController LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        confirgureView()
        addCancelGesture()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    // MARK: - Private Methods
    
    func confirgureView() {
        cancelView.roundVeryLittle(cornerRadius: 8)
        cancelView.addBorders()
        cancelView.isHidden = forceUpdate
    }
    
    func addCancelGesture() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onClickCancelButton(_:)))
        cancelView.addGestureRecognizer(tapGesture)
        cancelView.isUserInteractionEnabled = true
    }
    
   
    // MARK: - Action Methods
    
    @IBAction func onClickUpdateButton(_ sender: Any) {
        // App Store URL.
//        let appStoreLink = "https://itunes.apple.com/us/app/apple-store/id375380948?mt=8"
        let appStoreLink = "https://beta.itunes.apple.com/v1/app/1444540969"
        
        /* First create a URL, then check whether there is an installed app that can
         open it on the device. */
        if let url = URL(string: appStoreLink), UIApplication.shared.canOpenURL(url) {
            // Attempt to open the URL.
            UIApplication.shared.open(url, options: [:], completionHandler: {(success: Bool) in
                if success {
                    print("Launching \(url) was successful")
                }})
        }

    }
    
    @IBAction func onClickCancelButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
