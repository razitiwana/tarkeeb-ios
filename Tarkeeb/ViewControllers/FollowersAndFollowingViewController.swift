//
//  FollowersAndFollowingViewController.swift
//  Tarkeeb
//
//  Created by Osama Azmat Khan on 9/29/18.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import UIKit

enum FollowType : Int  {
    case followers
    case following
    static var count :Int {return FollowType.followers.hashValue + 1}
}


class FollowersAndFollowingViewController: ViewController {
    
    
    // MARK: - Outlets
    
    @IBOutlet weak var tableView: UITableView!
    

    // MARK: - Variables
    
    private var followers: [User] = [User]()
    private var following: [User] = [User]()
    private let refreshControl = UIRefreshControl()
    
    var user: User?
    
    var sectionToDisplay = FollowType.followers
    var fetchingMore = false
    var moreRemaining = true
    var startIndex = 0
    var count = Constants.countToFetchAtOneTime
    
    
    // MARK: - ViewController LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if sectionToDisplay == FollowType.followers{
            self.title = "Followers"
        }
        else if sectionToDisplay == FollowType.following{
            self.title = "Following"
        }
        
        if sectionToDisplay == FollowType.followers{
            self.fetchAllFollowers()
        }
        else if sectionToDisplay == FollowType.following{
            self.fetchAllFollowing()
        }
        
        configureTableView()
        refreshControl.beginRefreshing()
    }
    
    
    // MARK: - Private Methods
    
    func configureTableView() {
        // To remove the extra cells
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = UIColor.tableViewBackgroundColor()
        
        tableView.register(UINib(nibName: "FollowersAndFollowingTableViewCell", bundle: nil), forCellReuseIdentifier: "FollowersAndFollowingTableViewCell")
        
        // Setup refresh Control
        tableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        
        // Other wise the scroll inset are changeed when reloading data
        tableView.estimatedRowHeight = CGFloat(Constants.estimatedNotificationCellSize)
    }
    
    @objc func refresh() {
        if sectionToDisplay == FollowType.followers{
            self.startIndex = 0
            self.moreRemaining = true
            self.fetchingMore = false
            fetchAllFollowers()
        }
        else if sectionToDisplay == FollowType.following {
            self.startIndex = 0
            self.moreRemaining = true
            self.fetchingMore = false
            fetchAllFollowing()
        }
    }
    
    func beginBatchFetch() {
        
        if sectionToDisplay == FollowType.followers{
            print("Fetching Followers")
            
            fetchingMore = true
            self.startIndex += self.count
            
            self.fetchAllFollowers()
        }
        else{
            print("Fetching Following")
            
            fetchingMore = true
            self.startIndex += self.count
            
            self.fetchAllFollowing()
        }
        
    }
    
    
    // MARK: - Api Calls
    
    func fetchAllFollowers() {
        
        APIManager.sharedManager.fetchFollowers(for: user!, startIndex: self.startIndex, count: self.count) { (followers, error) in
            
            let isPullToRefresh = self.refreshControl.isRefreshing
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                self.refreshControl.endRefreshing()
            }
            
            if error == nil, let followers = followers {
                
                if self.startIndex > 0 {
                    self.followers.append(contentsOf: followers)
                }
                else {
                    self.followers = followers
                }
                
                // If the count of the fectehed posts is lesss than the count send then that means there are no more items left to fetch from data base
                self.moreRemaining = (followers.count) >= self.count
                
                self.fetchingMore = false
                
                let previousContentOfset = self.tableView.contentOffset
                
                self.tableView.reloadData()
                
                // if isPullToRefresh then it would be stuck in the pulled state if the service responds quickly
                if followers.count > 0 && !isPullToRefresh {
                    self.tableView.setContentOffset(previousContentOfset, animated: true)
                }
            } else {
                self.showAlert(text: error!)
            }
            self.tableView.tableFooterView = nil
        }
        
    }
    
    func fetchAllFollowing() {
        
        APIManager.sharedManager.fetchFollowing(of: user!, startIndex: self.startIndex, count: self.count) { (following, error) in
            
            let isPullToRefresh = self.refreshControl.isRefreshing
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                self.refreshControl.endRefreshing()
            }
            
            if error == nil, let following = following {
                if self.startIndex > 0 {
                    self.following.append(contentsOf: following)
                }
                else {
                    self.following = following
                }
                
                // If the count of the fectehed posts is lesss than the count send then that means there are no more items left to fetch from data base
                self.moreRemaining = (following.count) >= self.count
                self.fetchingMore = false
                let previousContentOfset = self.tableView.contentOffset
                self.tableView.reloadData()
                
                // if isPullToRefresh then it would be stuck in the pulled state if the service responds quickly
                if following.count > 0 && !isPullToRefresh {
                    self.tableView.setContentOffset(previousContentOfset, animated: true)
                }
            } else {
                self.showAlert(text: error!)
            }
            self.tableView.tableFooterView = nil
        }
    }
    
}

extension FollowersAndFollowingViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var numberOfRows = 0
        if sectionToDisplay == FollowType.followers{
            numberOfRows = followers.count
        }
        if sectionToDisplay == FollowType.following{
            numberOfRows = following.count
        }
        
        return numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FollowersAndFollowingTableViewCell") as! FollowersAndFollowingTableViewCell
        
        cell.delegate = self
        
        if sectionToDisplay == FollowType.followers {
            let followerUser = followers[indexPath.row]
            cell.configureCell(with: followerUser)
        }
        else if sectionToDisplay == FollowType.following {
            let followingUser = following[indexPath.row]
            cell.configureCell(with: followingUser)
        }
        
         cell.selectionStyle = .none
        
        return cell
    }
    
}

extension FollowersAndFollowingViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedUser: User?
        
        if sectionToDisplay == .followers {
            selectedUser = followers[indexPath.row]
        } else {
            selectedUser = following[indexPath.row]
        }
        
        openDetails(user: selectedUser!)

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if sectionToDisplay == FollowType.following {
            if self.following.count >= count && self.moreRemaining {
                
                let currentOffset = scrollView.contentOffset.y;
                let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
                
                // if the there are elements less than size of tableview
                if (scrollView.contentSize.height > self.tableView.frame.size.height) {
                    
                    // In some cases the curent offset is greater than the maximum one which makes no sense, so a hack would be to add this check
                    // The 100 if the user pulls the table view it should still load
                    if maximumOffset + 100 > currentOffset {
                        // Change 10.0 to adjust the distance from bottom
                        if (maximumOffset - currentOffset <= 10.0 ) {
                            if !self.fetchingMore {
                                tableView.tableFooterView = loadingView()
                                scrollView.setContentOffset(CGPoint(x: 0, y: maximumOffset + 44), animated: true)
                                
                                self.beginBatchFetch()
                            }
                        }}
                }
            }
        }
        else {
            if self.followers.count >= count && self.moreRemaining {
                
                let currentOffset = scrollView.contentOffset.y;
                let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
                
                // if the there are elements less than size of tableview
                if (scrollView.contentSize.height > self.tableView.frame.size.height) {
                    
                    // In some cases the curent offset is greater than the maximum one which makes no sense, so a hack would be to add this check
                    // The 100 if the user pulls the table view it should still load
                    if maximumOffset + 100 > currentOffset {
                        // Change 10.0 to adjust the distance from bottom
                        if (maximumOffset - currentOffset <= 10.0 ) {
                            if !self.fetchingMore {
                                tableView.tableFooterView = loadingView()
                                scrollView.setContentOffset(CGPoint(x: 0, y: maximumOffset + 44), animated: true)
                                
                                self.beginBatchFetch()
                            }
                        }}
                }
            }
        }
        
        
    }
    
}

extension FollowersAndFollowingViewController: FollowersAndFollowingTableViewCellDelegate {
    func openDetails(user: User) {
        let viewController = StoryboardRouter.profileViewController()
        viewController.user = user
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func follow(user: User) {
        if user.isFollowed {
            
            let alertController = UIAlertController(title: "Are You sure you want to unfollow " + user.userName! + "?", message: "", preferredStyle: .actionSheet)
        
            let unfollowAction = UIAlertAction(title: "Unfollow" , style: .destructive) { unfollowAction in
             self.changeFollowState(user: user)
            }
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            
            alertController.addAction(unfollowAction)
            alertController.addAction(cancelAction)
            alertController.view.tintColor = .black
            
            present(alertController, animated: true, completion: nil)
        } else {
            changeFollowState(user: user)
        }
    }
    
    func changeFollowState(user: User) {
        let oldState = user.isFollowed
        
        let concernedArray = (sectionToDisplay == FollowType.following) ? following : followers
        
        let selectedIndex = concernedArray.indices.filter { (index) -> Bool in
            return concernedArray[index].userID == user.userID
        }
        
        APIManager.sharedManager.follow(user: user) { (sucessMessage, error) in
            if error != nil {
                if self.sectionToDisplay == FollowType.following {
                    self.following[selectedIndex.first!].isFollowed = oldState
                } else {
                    self.followers[selectedIndex.first!].isFollowed = oldState
                }
                
                self.tableView.reloadData()
            } else {
            }
        }
        
        self.user?.isFollowed = true
        
        if sectionToDisplay == FollowType.following {
            following[selectedIndex.first!].isFollowed = !oldState
        } else {
            followers[selectedIndex.first!].isFollowed = !oldState
        }
        
        tableView.reloadData()
    }
    
}
