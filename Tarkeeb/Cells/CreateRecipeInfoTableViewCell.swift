//
//  CreateRecipeInfoTableViewCell.swift
//  Tarkeeb
//
//  Created by Osama Azmat Khan on 11/19/18.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import UIKit

protocol CreateRecipeInfoTableViewCellDelegate {
    func pickImage()
}

class CreateRecipeInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var selectedImageView: UIImageView!

    var delegate: CreateRecipeInfoTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.roundVeryLittle()
        
        let singleTapRecipePicture = UITapGestureRecognizer(target: self, action: #selector(onCLickSelectImageButton(_:)))
        selectedImageView.isUserInteractionEnabled = true
        selectedImageView.addGestureRecognizer(singleTapRecipePicture)
    }
    
    
    @IBAction func onCLickSelectImageButton(_ sender: Any) {
        if self.delegate != nil {
            self.delegate?.pickImage()
        }
    }
}
