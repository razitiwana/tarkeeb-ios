//
//  SearchViewHeaderTableViewCell.swift
//  Tarkeeb
//
//  Created by Osama Azmat Khan on 10/8/18.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import UIKit

class SearchViewHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var segemtedControl: UISegmentedControl!
    
    override func awakeFromNib() {
        super.awakeFromNib()
     
        segemtedControl.backgroundColor = .tableViewBackgroundColor()
         //var delegate: ProfileDetailTableViewCellDelegate?
    }
    
    @IBAction func segmentedControlDidChange(_ sender: UISegmentedControl) {
        self.segemtedControl.changeUnderlinePosition()
    }
    
    
    public func configureCell() {
        
            segemtedControl.setEnabled(false, forSegmentAt: 1)
            segemtedControl.addUnderlineForSelectedSegment(numberofSegments: 1, width: self.bounds.width)
        
    }
    
}
