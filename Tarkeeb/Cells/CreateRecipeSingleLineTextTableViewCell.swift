//
//  CreateRecipeSingleLineTextTableViewCell.swift
//  Tarkeeb
//
//  Created by Osama Azmat Khan on 11/19/18.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import UIKit


protocol CreateRecipeSingleLineTextTableViewCellDelegate {
    func removeRowSingleLineTextField(index: Int)
    func updateSingleLineCell(mainTextField: UITextField, index: Int)
}


class CreateRecipeSingleLineTextTableViewCell: UITableViewCell {

    
    @IBOutlet weak var crossButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var mainTextField: UITextField!
    @IBOutlet weak var circularView: UIView!
    
    var indexToSend = 0
    
    var delegate: CreateRecipeSingleLineTextTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.roundVeryLittle()
        circularView.round()
        circularView.addAppThemeBorder()
        
        mainTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)

    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if self.delegate != nil {
            self.delegate?.updateSingleLineCell(mainTextField: textField, index: indexToSend)
        }
    }
    
    @IBAction func onClickCrossButton(_ sender: Any) {
        if self.delegate != nil {
            self.delegate?.removeRowSingleLineTextField(index: indexToSend)
        }
    }
    
}

extension CreateRecipeSingleLineTextTableViewCell: UITextFieldDelegate {
    
    
}
