//
//  EditProfileBioTableViewCell.swift
//  Tarkeeb
//
//  Created by Osama Azmat Khan on 12/8/18.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import UIKit

protocol EditProfileBioTableViewCellDelegate {
    func updateTextView(bioText: String, section: Int)
}

class EditProfileBioTableViewCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var mainTextView: UITextView!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var selectedView: UIView!
    
    var delegate: EditProfileBioTableViewCellDelegate?
    var sectionToSend = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        mainTextView.delegate = self
        configureView()
    }
    
    func configureView() {
        self.hideSelectedView()
        containerView.roundTopOnly()
    }
    
    public func configureTextView(with textForLabel: String) {
        if textForLabel != "" {
            mainTextView.text = textForLabel
        }
        else {
            mainTextView.text = ""
        }
    }
    
    func showSelectedView() {
        UIView.transition(with: selectedView, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.selectedView.isHidden = false
        })
        //selectedView.isHidden = false
    }
    
    func hideSelectedView() {
        UIView.transition(with: selectedView, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.selectedView.isHidden = true
        })
        //selectedView.isHidden = true
    }
    
    
}

extension EditProfileBioTableViewCell: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if mainTextView.textColor == UIColor.lightGray {
            mainTextView.text = nil
            mainTextView.textColor = UIColor.black
        }
        self.showSelectedView()
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if self.delegate != nil {
            self.delegate?.updateTextView(bioText: mainTextView.text!, section: sectionToSend)
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        self.hideSelectedView()
    }
    
}

