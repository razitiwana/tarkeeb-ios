//
//  RecipeIngrediantTableViewCell.swift
//  Tarkeeb
//
//  Created by Razi Tiwana on 08/09/2018.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import UIKit
import Kingfisher

class RecipeIngrediantTableViewCell: UITableViewCell {

    // MARK: - IBOutlet
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var circularView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    
    @IBOutlet weak var bottomConstratint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        backgroundColor = .tableViewBackgroundColor()
        circularView.addAppThemeBorder()
        circularView.round()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
     // MARK: - Public Methods
    
    func topRounded() {
        containerView.roundTopOnly()
    }
    
    func bottomRounded() {
        containerView.roundBottomOnly()
        bottomConstratint.constant = 0
    }
    
    func topBottomRounded() {
        containerView.roundTopBottom()
        bottomConstratint.constant = 0
    }
    
    func defaultStateForBorders() {
        containerView.defaultState()
        bottomConstratint.constant = 0
    }
    
    public func configureCell(with ingrediant: Ingrediant) {
        nameLabel.text = ingrediant.name
        quantityLabel.text = ingrediant.quantity
    }
    
}
