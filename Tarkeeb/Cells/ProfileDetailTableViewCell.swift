//
//  ProfileDetailTableViewCell.swift
//  Tarkeeb
//
//  Created by Razi Tiwana on 17/09/2018.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import UIKit

protocol ProfileDetailTableViewCellDelegate {
    func openFollowing(of user: User)
    func openFollower(of user: User)
    func openCaps(of user: User)
    func openEdit()
    
    func changeCover(for user: User)
    func changeProfile(for user: User)
    func follow(user: User)
    func unFollow(user: User)
    func back()
    func postSelected(with type: UserProfilePostType)
}

class ProfileDetailTableViewCell: UITableViewCell {

    // MARK: - IBOutlet
    @IBOutlet weak var segemtedControl: UISegmentedControl!
    
    @IBOutlet weak var followingView: UIView!
    @IBOutlet weak var followerView: UIView!
    @IBOutlet weak var capsView: UIView!
    @IBOutlet weak var backView: UIView!
    
    @IBOutlet weak var followButton: UIButton!
    
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userBioLabel: UILabel!
    
    @IBOutlet weak var followerLabel: UILabel!
    @IBOutlet weak var followingLabel: UILabel!

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var backImageView: UIImageView!
    
    @IBOutlet weak var cap1ImageView: UIImageView!
    @IBOutlet weak var cap2ImageView: UIImageView!
    @IBOutlet weak var cap3ImageView: UIImageView!
    
    @IBOutlet weak var verifiedImageView: UIImageView!
    
    var user: User?
    var delegate: ProfileDetailTableViewCellDelegate?
    
    var isAlreadySet = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        addGestures()
        
        profileImageView.round()
        profileImageView.addBorders()
        
        followButton.round()
        followButton.borders(for: [.all], width: 2, color: .white)
        
        backImageView.roundVeryLittle()
        backView.roundVeryLittle(cornerRadius: 4)
        backView.blurView()
        segemtedControl.backgroundColor = .tableViewBackgroundColor()
    }

    func addGestures()  {
//        let followingViewTapGesture = UITapGestureRecognizer(target: self, action: #selector(followingViewTapped))
//        followingView.addGestureRecognizer(followingViewTapGesture)
//        followingView.isUserInteractionEnabled = true
//        
//        let followersViewTapGesture = UITapGestureRecognizer(target: self, action: #selector(followersViewTapped))
//        followerView.addGestureRecognizer(followersViewTapGesture)
//        followerView.isUserInteractionEnabled = true
        
        let cappsViewTapGesture = UITapGestureRecognizer(target: self, action: #selector(capsViewTapped))
        capsView.addGestureRecognizer(cappsViewTapGesture)
        capsView.isUserInteractionEnabled = true
        
        let backViewTapGesture = UITapGestureRecognizer(target: self, action: #selector(backViewTapped))
        backView.addGestureRecognizer(backViewTapGesture)
        backView.isUserInteractionEnabled = true
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if let user = self.user {
            if !isAlreadySet {
                isAlreadySet = true
                segemtedControl.removeLinefromView()
                configureCell(with: user)
            }
            
        }
    }
    
    
    // MARK: - Action Methods
    
    @IBAction func followAction(_ sender: UIButton) {
        let isFollowingUser = (user?.isFollowed)!
        
        if self.delegate != nil {
            if (user?.isOwnProfile)! {
                self.delegate?.openEdit()
            } else if isFollowingUser {
                self.delegate?.unFollow(user: user!)
            } else {
                self.delegate?.follow(user: user!)
            }
        }
    }
    
    @IBAction func segmentedControlDidChange(_ sender: UISegmentedControl) {
        self.segemtedControl.changeUnderlinePosition()
        
        if self.delegate != nil {
            self.delegate?.postSelected(with: UserProfilePostType(rawValue: segemtedControl.selectedSegmentIndex)!)
        }
    }
    
    @objc func followingViewTapped() {
        if self.delegate != nil {
            self.delegate?.openFollowing(of: user!)
        }
    }
    
    @objc func followersViewTapped() {
        if self.delegate != nil {
            self.delegate?.openFollower(of: user!)
        }
    }
    
    @objc func capsViewTapped() {
        if self.delegate != nil {
            self.delegate?.openCaps(of: user!)
        }
    }
    
    @objc func backViewTapped() {
        if self.delegate != nil {
            self.delegate?.back()
        }
    }
    
    
    // MARK: - Public Methods
    
    public func configureCell(with user: User) {
        self.user = user
        
        if !user.isOwnProfile {
            segemtedControl.setEnabled(false, forSegmentAt: 1)
            segemtedControl.setWidth(0.1, forSegmentAt: 1)
            segemtedControl.addUnderlineForSelectedSegment(numberofSegments: 1, width: self.bounds.width)
            
            let myRecipies = "Recipes | " + user.totalRecipes.formatKandM()
            segemtedControl.setTitle(myRecipies, forSegmentAt: 0)
            
            let isFollowingUser = user.isFollowed
            
            followButton.removeBlurView()
            
            if isFollowingUser {
                followButton.setTitle("Following", for: .normal)
                followButton.backgroundColor = .clear
                followButton.blurView()
            } else {
                followButton.setTitle("Follow", for: .normal)
                followButton.backgroundColor = UIColor.appThemeColor()
                
            }
            
        } else {
            let myRecipies = "My Recipes | " + user.totalRecipes.formatKandM()
            segemtedControl.setTitle(myRecipies, forSegmentAt: 0)
            
            let saved = "Saved | " + user.totalSavedRecipes.formatKandM()
            segemtedControl.setTitle(saved, forSegmentAt: 1)
            
            segemtedControl.addUnderlineForSelectedSegment(numberofSegments: 2, width: self.bounds.width)
            
            followButton.setTitle("Edit Profile", for: .normal)
            followButton.backgroundColor = UIColor.appThemeColor()
        }
        
        userNameLabel.text = user.fullName()

        followerLabel.text = user.numberOfFollwers.formatKandM()
        followingLabel.text = user.numberOfFollwing.formatKandM()
       
        userBioLabel.text = user.userBio

        verifiedImageView.isHidden = !(user.isVerified)
        
        if let userPicture = user.userPicture {
            profileImageView.kf.setImage(with: URL(string:userPicture), placeholder: UIImage(named:"ic_profile_placeholder"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        
        if let coverPicture = user.coverImage {
            coverImageView.kf.setImage(with: URL(string:coverPicture), placeholder: UIImage(named:"ic_cover_placeholder"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        
        if let cap = user.chefCaps?.cap1 {
            cap1ImageView.kf.setImage(with: URL(string:cap), placeholder: UIImage(named: "ic_chefcap_default"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        if let cap = user.chefCaps?.cap2 {
            cap2ImageView.kf.setImage(with: URL(string:cap), placeholder: UIImage(named: "ic_chefcap_default"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        if let cap = user.chefCaps?.cap3 {
            cap3ImageView.kf.setImage(with: URL(string:cap), placeholder: UIImage(named: "ic_chefcap_default"), options: nil, progressBlock: nil, completionHandler: nil)
        }
    }
}
