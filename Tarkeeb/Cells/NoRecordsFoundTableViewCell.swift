//
//  NoRecordsFoundTableViewCell.swift
//  Tarkeeb
//
//  Created by Osama Azmat Khan on 10/12/18.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import UIKit

class NoRecordsFoundTableViewCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
