//
//  RecipeSearchTableViewCell.swift
//  Tarkeeb
//
//  Created by Osama Azmat Khan on 10/8/18.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import UIKit

class RecipeSearchTableViewCell: UITableViewCell {

    @IBOutlet weak var totalLikesContainerView: UIView!
    @IBOutlet weak var likesContainerView: UIView!
    @IBOutlet weak var totalLikesLabel: UILabel!
    @IBOutlet weak var isLikedImageView: UIImageView!
    @IBOutlet weak var userIsVerifiedImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var userProfileImageView: UIImageView!
    @IBOutlet weak var userDetailsView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var recipeTitleLabel: UILabel!
    @IBOutlet weak var recipeImageView: UIImageView!
    
    var post: Recipe?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        backgroundColor = .tableViewBackgroundColor()
        userProfileImageView.round()
        containerView.roundVeryLittle()
        recipeImageView.roundVeryLittle()
        totalLikesContainerView.round()
    }
    
    
    public func configureCell(with post: Recipe) {
        self.post = post
        
        if post.user != nil {
            usernameLabel.text = post.user?.userName
            recipeTitleLabel.text = post.title
            likesContainerView.isHidden = false
            
            if let userPicture = post.user?.userPicture {
                userProfileImageView.kf.setImage(with: URL(string:userPicture), placeholder: UIImage(named:"ic_profile_placeholder"), options: nil, progressBlock: nil, completionHandler: nil)
            }
            
            if let postPicture = post.picture {
                recipeImageView.kf.setImage(with: URL(string:postPicture), placeholder: UIImage(named:"ic_recipe_placeholder"), options: nil, progressBlock: nil, completionHandler: nil)
            }
            
            let likeImage = (post.isLiked) ? "ic_heart_selected" : "ic_heart_unselected"
            isLikedImageView.image = UIImage(named: likeImage)
            
//            if post.numberOflikes > 0 {
//                totalLikesLabel.text = post.numberOflikes.formatKandM()
//            } else {
//                likesContainerView.isHidden = true
//            }
            
            likesContainerView.isHidden = true
            
            
            userIsVerifiedImageView.isHidden = !(post.user?.isVerified)!
        }
    }
    
    
}
