//
//  RecipeInstructionTableViewCell.swift
//  Tarkeeb
//
//  Created by Razi Tiwana on 08/09/2018.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import UIKit
import Kingfisher

class RecipeInstructionTableViewCell: UITableViewCell {

    // MARK: - IBOutlet
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var circularView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        backgroundColor = .tableViewBackgroundColor()
        
        circularView.addAppThemeBorder()
        circularView.round()
        
        containerView.roundVeryLittle()
        containerView.addBorders()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    // MARK: - Public Methods
    
    public func configureCell(with instruction: Instruction) {
        titleLabel.text = instruction.content
//        descriptionLabel.text = instruction.content
    }
}
