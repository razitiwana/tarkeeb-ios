//
//  EditProfileEmailTableViewCell.swift
//  Tarkeeb
//
//  Created by Osama Azmat Khan on 12/8/18.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

protocol EditProfileEmailTableViewCellDelegate {
    func updateEmail(text: String)
}

class EditProfileEmailTableViewCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var mainSkyFloatingView: UIView!
    @IBOutlet weak var selectedView: UIView!
    
    var textField: SkyFloatingLabelTextField?
    var delegate: EditProfileEmailTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        configureView()
        configureTextField(email: nil)
    }
    
    func configureView() {
        containerView.roundTopOnly()
        
        self.hideSelectedView()

        textField = SkyFloatingLabelTextField(frame: CGRect(x: 0, y: 0, width: self.mainSkyFloatingView.frame.width, height: 50))
        textField?.placeholder = "Email"
        textField?.title = "Email"
        textField?.tintColor = UIColor.appThemeColor()
        textField?.selectedTitleColor = UIColor.appThemeColor()
        textField?.selectedLineColor = UIColor.appThemeColor()
        textField?.selectedLineHeight = 0
        textField?.lineHeight = 0
        textField?.delegate = self
        self.mainSkyFloatingView.addSubview(textField!)
    }
    
    func configureTextField(email: String?) {
        
        if email != nil {
            textField?.text = email!
        }
        else {
            textField?.text = ""
        }
    }
    
    func showSelectedView() {
        UIView.transition(with: selectedView, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.selectedView.isHidden = false
        })
        //selectedView.isHidden = false
    }
    
    func hideSelectedView() {
        UIView.transition(with: selectedView, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.selectedView.isHidden = true
        })
        //selectedView.isHidden = true
    }
}

extension EditProfileEmailTableViewCell: UITextFieldDelegate {

    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.showSelectedView()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if self.delegate != nil {
            if textField.text != nil {
                 self.delegate?.updateEmail(text: textField.text!)
            }
        }
        self.hideSelectedView()
    }
    
}













