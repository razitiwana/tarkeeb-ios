//
//  LoadingTableViewCell.swift
//  Tarkeeb
//
//  Created by Osama Azmat Khan on 9/21/18.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import UIKit
import MBProgressHUD

class LoadingTableViewCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
//    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        activityIndicator.activityIndicatorViewStyle = .gray
    }
    
}
