//
//  EditProfileGenderTableViewCell.swift
//  Tarkeeb
//
//  Created by Osama Azmat Khan on 12/8/18.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

protocol EditProfileGenderTableViewCellDelegate {
    func updateGender(gender: String)
    func openGenderPickerView(delegate: GenericPickerViewControllerDelegate)
}

class EditProfileGenderTableViewCell: UITableViewCell {

    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var mainSkyFloatingView: UIView!
    @IBOutlet weak var selectedView: UIView!
    
    var delegate: EditProfileGenderTableViewCellDelegate?
    var textField: SkyFloatingLabelTextField?
    var firstTimeFlag: Bool = true
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureView()
        configureTextField(gender: nil)
    }
    
    func configureView() {
        containerView.roundTopOnly()
        
        self.hideSelectedView()
        
        textField = SkyFloatingLabelTextField(frame: CGRect(x: 0, y: 0, width: self.mainSkyFloatingView.frame.width, height: 50))
        textField?.isUserInteractionEnabled = false
        textField?.placeholder = "Gender"
        textField?.title = "Gender"
        textField?.tintColor = UIColor.appThemeColor()
        textField?.selectedTitleColor = UIColor.appThemeColor()
        textField?.selectedLineColor = UIColor.appThemeColor()
        textField?.selectedLineHeight = 0
        textField?.lineHeight = 0
        self.mainSkyFloatingView.addSubview(textField!)
        
        let openGenderView = UITapGestureRecognizer(target: self, action: #selector(openGenderPickerView))
        containerView.addGestureRecognizer(openGenderView)
        containerView.isUserInteractionEnabled = true
    }
    
    func configureTextField(gender: String?) {
        
        if gender != nil {
            textField?.text = gender!
        }
        else {
            textField?.text = ""
        }
    }
    
    func showSelectedView() {
        UIView.transition(with: selectedView, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.selectedView.isHidden = false
        })
        //selectedView.isHidden = false
    }
    
    func hideSelectedView() {
        UIView.transition(with: selectedView, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.selectedView.isHidden = true
        })
        //selectedView.isHidden = true
    }
    
    @objc func openGenderPickerView() {
        if self.delegate != nil {
            self.delegate?.openGenderPickerView(delegate: self)
            self.showSelectedView()
        }
    }
}

extension EditProfileGenderTableViewCell: GenericPickerViewControllerDelegate {
    func didSelectValue(selectedValue: String) {
        if self.delegate != nil {
            self.delegate?.updateGender(gender: selectedValue)
        }
         self.hideSelectedView()
    }
    
    func cancel() {
        self.hideSelectedView()
    }
    
    
}
