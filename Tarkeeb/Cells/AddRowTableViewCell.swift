//
//  AddRowTableViewCell.swift
//  Tarkeeb
//
//  Created by Osama Azmat Khan on 11/20/18.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import UIKit

protocol AddRowTableViewCellDelegate {
    func addRow()
}

class AddRowTableViewCell: UITableViewCell {

    @IBOutlet weak var addRowButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    
    var delegate: AddRowTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.roundVeryLittle()
        addRowButton.round()
        addRowButton.addAppThemeBorder()
    }
    
    @IBAction func onClickAddRow(_ sender: Any) {
            if self.delegate != nil {
                self.delegate?.addRow()
            }
    }
    
}
