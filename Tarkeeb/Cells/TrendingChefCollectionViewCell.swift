//
//  TrendingChefCollectionViewCell.swift
//  Tarkeeb
//
//  Created by Razi Tiwana on 17/09/2018.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import UIKit

protocol TrendingChefCollectionViewCellDelegate  {
    func follow(user: User)
}

class TrendingChefCollectionViewCell: UICollectionViewCell {
   
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var followButton: UIButton!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var verifiedImageView: UIImageView!
    
    var user: User?
    var delegate: TrendingChefCollectionViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        backgroundColor = .tableViewBackgroundColor()
        
        profileImageView.round()
        containerView.roundTwenty()
        configureView()
    }
    
    func configureView() {
        
        followButton.round()
        
        let followTapGesture = UITapGestureRecognizer(target: self, action: #selector(followTapped))
        followButton.addGestureRecognizer(followTapGesture)
        followButton.isUserInteractionEnabled = true
    }
    
    @objc func followTapped() {
        if self.delegate != nil {
            self.delegate?.follow(user: user!)
        }
    }
    
    // MARK: - Public Methods
    
    public func configureCell(with chef: User) {
        user = chef
        userNameLabel.text = chef.fullName()
        
        if let userPicture = chef.userPicture {
            profileImageView.kf.setImage(with: URL(string:userPicture), placeholder: UIImage(named:"ic_profile_placeholder"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        
        if chef.isFollowed {
            self.followButton.followingButton()
        }
        else
        {
            self.followButton.followButton()
        }
        
        // If you are in the top chef
        self.followButton.isHidden = chef.userID == User.currentUser!.userID
        
        verifiedImageView.isHidden = !(chef.isVerified)
    }
}

