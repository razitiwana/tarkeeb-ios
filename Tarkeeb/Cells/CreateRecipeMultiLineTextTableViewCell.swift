//
//  CreateRecipeMultiLineTextTableViewCell.swift
//  Tarkeeb
//
//  Created by Osama Azmat Khan on 11/19/18.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import UIKit


protocol CreateRecipeMultiLineTextTableViewCellDelegate {
    func updateTextView(mainTextView: UITextView, rightTextField: UITextField, index: Int, section:Int)
    func removeRow(index: Int)
}

class CreateRecipeMultiLineTextTableViewCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var mainTextView: UITextView!
    @IBOutlet weak var circularView: UIView!
    @IBOutlet weak var rightTextField: UITextField!
    @IBOutlet weak var crossButton: UIButton!
    
    @IBOutlet weak var textFieldWidthConstraint: NSLayoutConstraint!
    
    var indexToSend = 0
    var sectionToSend = 0
    
    var delegate: CreateRecipeMultiLineTextTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        mainTextView.delegate = self
        containerView.roundVeryLittle()
        circularView.round()
        circularView.addAppThemeBorder()
        
        rightTextField.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)

    }
    
    override func prepareForReuse() {
        rightTextField.isHidden = false
        rightTextField.text = ""
        mainTextView.textColor = UIColor.black
    }
    
    @objc func textFieldDidChange(textField: UITextField)
    {
        if delegate != nil {
            self.delegate?.updateTextView(mainTextView: mainTextView, rightTextField: self.rightTextField, index: indexToSend, section: sectionToSend)
        }
    }
    
    public func hideTextField()
    {
        self.textFieldWidthConstraint.constant = 0
        self.rightTextField.isHidden = true
        self.layoutIfNeeded()
    }
   
    public func showTextfield()
    {
        self.textFieldWidthConstraint.constant = 80
        rightTextField.isHidden = false
        self.layoutIfNeeded()
    }
    
    public func configureCell(with textForLabel: String) {
        if textForLabel != "" {
            mainTextView.text = textForLabel
        }
        else {
            mainTextView.text = ""
        }
    }
    
    @IBAction func onClickCrossButton(_ sender: Any) {
        if self.delegate != nil {
            self.delegate?.removeRow(index: indexToSend)
        }
    }
    
    
}
extension CreateRecipeMultiLineTextTableViewCell: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if mainTextView.textColor == UIColor.lightGray {
            mainTextView.text = nil
            mainTextView.textColor = UIColor.black
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if self.delegate != nil {
            self.delegate?.updateTextView(mainTextView: mainTextView, rightTextField: self.rightTextField, index: indexToSend, section: sectionToSend)
        }
    }
}
