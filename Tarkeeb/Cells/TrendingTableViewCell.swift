//
//  TrendingTableViewCell.swift
//  Tarkeeb
//
//  Created by Razi Tiwana on 16/09/2018.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import UIKit

enum TrendingTableViewCellType {
    case trendingRecipes
    case trendingChefs
}

class TrendingTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var cellType = TrendingTableViewCellType.trendingRecipes
    
    var trendingRecipes: [Recipe] = []
    var trendingChefs: [User] = []
    
    var delegate: RecipeTableViewCellDelegate?
    var delegateTendingChefCollectionViewCell: TrendingChefCollectionViewCellDelegate?
    
    var isLoading = true
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func reloadData() {
        collectionView.reloadData()
    }
    
}

extension TrendingTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        var numberOfItems = 0
        var noDataMessage: String?
        
        switch cellType {
        case .trendingChefs:
            numberOfItems = trendingChefs.count
            noDataMessage = "No chefs found"
            break
        case .trendingRecipes:
            numberOfItems =  trendingRecipes.count
             noDataMessage = "No recipies found"
            break
        }
        
        if isLoading  && (numberOfItems == 0) {
            //self.collectionView.setActivityIndicator()
//             self.collectionView.setEmptyMessage("")
            isLoading = false
        } else if (numberOfItems == 0) {
            self.collectionView.setEmptyMessage(noDataMessage!)
        } else {
            self.collectionView.restore()
        }
        
        return numberOfItems
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var cellToReturn: UICollectionViewCell?
        
        switch cellType {
        case .trendingRecipes:
          let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TrendingRecipeCollectionViewCell", for: indexPath) as? TrendingRecipeCollectionViewCell
          
          let recipe = trendingRecipes[indexPath.row]
          cell?.configureCell(with: recipe)
          cell?.delegate = self
          cellToReturn = cell
            
            break
        case .trendingChefs:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TrendingChefCollectionViewCell", for: indexPath) as? TrendingChefCollectionViewCell
            
            let chefs = trendingChefs[indexPath.row]
            cell?.configureCell(with: chefs)
            cell?.delegate = delegateTendingChefCollectionViewCell
            cellToReturn = cell
            break
        }
        
        return cellToReturn!
    }
}

extension TrendingTableViewCell: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch cellType {
        case .trendingRecipes:
            let selectedRecipe = trendingRecipes[indexPath.row]
            
            if self.delegate != nil {
                self.delegate?.openDetail(for: selectedRecipe)
            }
            
            break
        case .trendingChefs:
            let selectedChef = trendingChefs[indexPath.row]
            
            if self.delegate != nil {
                self.delegate?.openDetail(for: selectedChef)
            }
            break
        }
    }
}

extension TrendingTableViewCell: RecipeTableViewCellDelegate {
    
    func like(post: Recipe) {
        if self.delegate != nil {
            self.delegate?.like(post: post)
        }
    }
    
    func openDetail(for user: User) {
        if self.delegate != nil {
            self.delegate?.openDetail(for: user)
        }
    }
}
