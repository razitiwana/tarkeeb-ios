//
//  TrendingRecipeCollectionViewCell.swift
//  Tarkeeb
//
//  Created by Razi Tiwana on 16/09/2018.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import UIKit

class TrendingRecipeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var likesView: UIView!
    @IBOutlet weak var likeView: UIView!
    
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!

    @IBOutlet weak var likesLabel: UILabel!
    
    @IBOutlet weak var recipieImageView: UIImageView!
    
    @IBOutlet weak var likeImageView: UIImageView!
    @IBOutlet weak var verifiedImageView: UIImageView!
    
    var post: Recipe?
    var delegate: RecipeTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        backgroundColor = .tableViewBackgroundColor()
        
        likesView.round()
        containerView.roundTwenty()
        
        // Add gestures
        addGestures()
    }
    
    func addGestures()  {
        let likeViewTapGesture = UITapGestureRecognizer(target: self, action: #selector(likeViewTapped))
        likeView.addGestureRecognizer(likeViewTapGesture)
        likeView.isUserInteractionEnabled = true
        
        let profileViewTapGesture = UITapGestureRecognizer(target: self, action: #selector(profileViewTapped))
        userNameLabel.addGestureRecognizer(profileViewTapGesture)
        userNameLabel.isUserInteractionEnabled = true
    }
    
    
    // MARK: - Action Methods
    
    @objc func likeViewTapped() {
        if self.delegate != nil {
            self.delegate?.like(post: post!)
        }
    }
    
    @objc func profileViewTapped() {
        if self.delegate != nil {
            self.delegate?.openDetail(for: (post?.user)!)
        }
    }
    
    
    // MARK: - Public Methods
    
    public func configureCell(with post: Recipe) {
        self.post = post
        
        addGestures()
        
        userNameLabel.text = post.user?.userName
        likesLabel.text = post.numberOflikes.formatKandM()
        titleLabel.text = post.title
        
        if let postPicture = post.picture {
            recipieImageView.kf.setImage(with: URL(string:postPicture) , placeholder: UIImage(named:"ic_recipe_placeholder"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        
        likesView.isHidden = post.numberOflikes == 0
        verifiedImageView.isHidden = !(post.user?.isVerified)!
        
        let likeImage = (post.isLiked) ? "ic_heart_selected" : "ic_heart_unselected"
        likeImageView.image = UIImage(named: likeImage)
        
    }
}
