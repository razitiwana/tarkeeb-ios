//
//  EditProfileNameTableViewCell.swift
//  Tarkeeb
//
//  Created by Osama Azmat Khan on 12/8/18.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

protocol EditProfileNameTableViewCellDelegate {
    func updateFirstName(text: String)
    func updateLastName(text: String)
}

enum EditProfileNameTableViewCellSelectedTextField: Int {
    case firstName
    case lastName
    
    static var count :Int {return EditProfileNameTableViewCellSelectedTextField.lastName.hashValue + 1}
}

class EditProfileNameTableViewCell: UITableViewCell {


    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var mainSkyFloatingView: UIView!
    @IBOutlet weak var selectedView: UIView!
    
    var textField: SkyFloatingLabelTextField?
    var textFieldLastName: SkyFloatingLabelTextField?
    var delegate: EditProfileNameTableViewCellDelegate?
    var selectedtextField = EditProfileNameTableViewCellSelectedTextField.firstName
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureView()
    }
    
    func configureView() {
        containerView.roundTopOnly()
        
        hideSelectedView()
        
        textField = SkyFloatingLabelTextField(frame: CGRect(x: 0, y: 0, width: self.mainSkyFloatingView.frame.width/2, height: 50))
        textField?.placeholder = "Name"
        textField?.title = "Name"
        textField?.tintColor = UIColor.appThemeColor()
        textField?.selectedTitleColor = UIColor.appThemeColor()
        textField?.selectedLineColor = UIColor.appThemeColor()
        textField?.selectedLineHeight = 0
        textField?.lineHeight = 0
        textField?.delegate = self
        textField?.tag = 10
        self.mainSkyFloatingView.addSubview(textField!)
        
        textFieldLastName = SkyFloatingLabelTextField(frame: CGRect(x: (textField?.frame.maxX)! + 8, y: 0, width: self.mainSkyFloatingView.frame.width/2, height: 50))
        textFieldLastName?.placeholder = "Last Name"
        textFieldLastName?.title = "Last Name"
        textFieldLastName?.tintColor = UIColor.appThemeColor()
        textFieldLastName?.selectedTitleColor = UIColor.appThemeColor()
        textFieldLastName?.selectedLineColor = UIColor.appThemeColor()
        textFieldLastName?.selectedLineHeight = 0
        textFieldLastName?.lineHeight = 0
        textFieldLastName?.delegate = self
        textField?.tag = 20
        self.mainSkyFloatingView.addSubview(textFieldLastName!)
    }
    
    func configureTextField(name: String?, selectedTextFieldLocal: EditProfileNameTableViewCellSelectedTextField?) {
        
        if selectedTextFieldLocal == EditProfileNameTableViewCellSelectedTextField.firstName {
            if name != nil {
                textField?.text = name!
            }
            else {
                textField?.text = ""
            }
        }
        else {
            if name != nil {
                textFieldLastName?.text = name!
            }
            else {
                textFieldLastName?.text = ""
            }
        }
    }
    
    func showSelectedView() {
        UIView.transition(with: selectedView, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.selectedView.isHidden = false
        })
        //selectedView.isHidden = false
    }
    
    func hideSelectedView() {
        UIView.transition(with: selectedView, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.selectedView.isHidden = true
        })
        //selectedView.isHidden = true
    }
    
}

extension EditProfileNameTableViewCell: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.showSelectedView()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.tag == 10 {
            if self.delegate != nil {
                if textField.text != nil {
                    self.delegate?.updateFirstName(text: textField.text!)
                }
            }
        }
        else {
            if self.delegate != nil {
                if textField.text != nil {
                    self.delegate?.updateLastName(text: textField.text!)
                }
            }
        }
        
        self.hideSelectedView()
    }
    
}

