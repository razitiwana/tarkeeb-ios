//
//  EditProfileTopHeaderTableViewCell.swift
//  Tarkeeb
//
//  Created by Osama Azmat Khan on 12/8/18.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import UIKit

protocol EditProfileTopHeaderTableViewCellDelegate {
    func pickCoverImage()
    func pickProfileImage()
}

class EditProfileTopHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var coverPictureImageView: UIImageView!
    @IBOutlet weak var profilePictureImageView: UIImageView!
    
    var delegate: EditProfileTopHeaderTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureGestures()
        configureView()
    }

    func configureView() {
        profilePictureImageView.round()
        profilePictureImageView.addBorders()
    }
    
    func configureGestures(){
        
        let singleTapProfilePicture = UITapGestureRecognizer(target: self, action: #selector(EditProfileTopHeaderTableViewCell.selectProfilePicture))
        profilePictureImageView.isUserInteractionEnabled = true
        profilePictureImageView.addGestureRecognizer(singleTapProfilePicture)
        
        let singleTapCoverPicture = UITapGestureRecognizer(target: self, action: #selector(EditProfileTopHeaderTableViewCell.selectCoverPicture))
        coverPictureImageView.isUserInteractionEnabled = true
        coverPictureImageView.addGestureRecognizer(singleTapCoverPicture)
        
    }
    
    @objc func selectProfilePicture() {
        if self.delegate != nil {
            self.delegate?.pickProfileImage()
        }
    }
    
    @objc func selectCoverPicture() {
        if self.delegate != nil {
            self.delegate?.pickCoverImage()
        }
    }
    
}
