//
//  RecipeTableViewCell.swift
//  Tarkeeb
//
//  Created by Razi Tiwana on 07/09/2018.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import UIKit
import Kingfisher

protocol RecipeTableViewCellDelegate {
    func like(post: Recipe)
    func share(post: Recipe)
    func save(post: Recipe)
    func openDetail(for user: User)
    func openDetail(for post: Recipe)
}

extension RecipeTableViewCellDelegate {
    // To make these optional
    func share(post: Recipe) {
        
    }
    
    func save(post: Recipe) {
        
    }
    
    func openDetail(for post: Recipe) {
        
    }
}


class RecipeTableViewCell: UITableViewCell {
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var likesView: UIView!
    @IBOutlet weak var shareView: UIView!
    @IBOutlet weak var saveView: UIView!
    @IBOutlet weak var likeView: UIView!
    @IBOutlet weak var pictureView: UIView!
    
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var postedTimeLabel: UILabel!
    @IBOutlet weak var timeRequiredLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var viewsLabel: UILabel!
    @IBOutlet weak var likesLabel: UILabel!
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var recipieImageView: UIImageView!
    
    @IBOutlet weak var likeImageView: UIImageView!
    @IBOutlet weak var saveImageView: UIImageView!
    @IBOutlet weak var verifiedImageView: UIImageView!
    
    var post: Recipe?
    var delegate: RecipeTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        backgroundColor = .tableViewBackgroundColor()
        
        profileImageView.round()
        
        likesView.round()
        containerView.roundVeryLittle()
//        containerView.addBorders()
        
        // Add gestures
        addGestures()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
    func addGestures()  {
        let likeViewTapGesture = UITapGestureRecognizer(target: self, action: #selector(likeViewTapped))
        likeView.addGestureRecognizer(likeViewTapGesture)
        likeView.isUserInteractionEnabled = true
        
        let shareViewTapGesture = UITapGestureRecognizer(target: self, action: #selector(shareViewTapped))
        shareView.addGestureRecognizer(shareViewTapGesture)
        shareView.isUserInteractionEnabled = true
        
        let saveViewTapGesture = UITapGestureRecognizer(target: self, action: #selector(saveViewTapped))
        saveView.addGestureRecognizer(saveViewTapGesture)
        saveView.isUserInteractionEnabled = true
        
        let profileViewTapGesture = UITapGestureRecognizer(target: self, action: #selector(profileViewTapped))
        profileView.addGestureRecognizer(profileViewTapGesture)
        profileView.isUserInteractionEnabled = true
    }
    
    
    // MARK: - Action Methods
    
    @objc func likeViewTapped() {
        if self.delegate != nil {
            self.delegate?.like(post: post!)
        }
    }
    
    @objc func shareViewTapped() {
        if self.delegate != nil {
             self.delegate?.share(post: post!)
        }
    }
    
    @objc func saveViewTapped() {
        if self.delegate != nil {
            self.delegate?.save(post: post!)
        }
    }
    
    @objc func profileViewTapped() {
        if self.delegate != nil {
            self.delegate?.openDetail(for: (post?.user)!)
        }
    }
    
    
    // MARK: - Public Methods
    
    public func configureCell(with post: Recipe) {
        self.post = post
        
        addGestures()
        
        userNameLabel.text = post.user?.userName
        postedTimeLabel.text = post.postedTime?.timeAgo()
        timeRequiredLabel.text = post.timeRequired
        likesLabel.text = post.numberOflikes.formatKandM()
        viewsLabel.text = post.postViews.formatKandM() + ((post.postViews > 1) ? " views" : " view")
        descriptionLabel.text = post.discription
        titleLabel.text = post.title
        
        if let userPicture = post.user?.userPicture {
            profileImageView.kf.setImage(with: URL(string:userPicture), placeholder: UIImage(named:"ic_profile_placeholder"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        
        if let postPicture = post.picture {
            recipieImageView.kf.setImage(with: URL(string:postPicture) , placeholder: UIImage(named:"ic_recipe_placeholder"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        
        likesView.isHidden = post.numberOflikes == 0
        viewsLabel.isHidden = post.postViews == 0
        verifiedImageView.isHidden = !(post.user?.isVerified)!
        
        let likeImage = (post.isLiked) ? "ic_heart_selected" : "ic_heart_unselected"
        likeImageView.image = UIImage(named: likeImage)
        
        let saveImage = (post.isSaved) ? "ic_save_selected" : "ic_save_unselected"
        saveImageView.image = UIImage(named: saveImage)
    }
}
