//
//  NotificationTableViewCell.swift
//  Tarkeeb
//
//  Created by Osama Azmat Khan on 9/17/18.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import UIKit



class NotificationTableViewCell: UITableViewCell {

    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var notificationTypeIconImageView: UIImageView!
    @IBOutlet weak var notificationContentLabel: UILabel!
    
    var notification: Notification?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        backgroundColor = .tableViewBackgroundColor()
        
        containerView.roundVeryLittle()
        profileImageView.round()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func configureCell(with notification: Notification) {
        self.notification = notification
        
        self.notificationContentLabel.text = notification.content
        
        containerView.removeGradiant()
        
        if !notification.isRead! {
            self.containerView.addGradiant()
            self.notificationContentLabel.textColor = UIColor.darkGray
        } else {
            self.notificationContentLabel.textColor = UIColor.lightGray
        }
        
        if let userPicture = notification.user?.userPicture {
             profileImageView.kf.setImage(with: URL(string:userPicture), placeholder: UIImage(named:"ic_profile_placeholder"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        else{
            let iconImage = "ic_profile_placeholder"
            self.profileImageView.image = UIImage(named: iconImage)
        }
        
        self.notificationTypeIconImageView.isHidden = false
        
        if notification.notificationType == NotificationType.like {
            let iconImage = "ic_heart_selected"
            self.notificationTypeIconImageView.image = UIImage(named: iconImage)
        }
        else if notification.notificationType == NotificationType.comment {
            // TODO:- Change the image to the right image and unhide this thing
            self.notificationTypeIconImageView.isHidden = true
            
            let iconImage = "ic_search_selected"
            self.notificationTypeIconImageView.image = UIImage(named: iconImage)
        }
        else if notification.notificationType == NotificationType.follow {
            let iconImage = "ic_profile_selected"
            self.notificationTypeIconImageView.image = UIImage(named: iconImage)
        }
        else if notification.notificationType == NotificationType.sharedBy {
            // TODO:- Change the image to the right image and unhide this thing
            self.notificationTypeIconImageView.isHidden = true
            
            let iconImage = "ic_notification_selected"
            self.notificationTypeIconImageView.image = UIImage(named: iconImage)
        } else {
            self.notificationTypeIconImageView.isHidden = true
        }
    }
    
}
