//
//  FollowersAndFollowingTableViewCell.swift
//  Tarkeeb
//
//  Created by Osama Azmat Khan on 9/29/18.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import UIKit

protocol FollowersAndFollowingTableViewCellDelegate {
    func openDetails(user: User)
    func follow(user: User)
}

class FollowersAndFollowingTableViewCell: UITableViewCell {

    // MARK: - Outlets
    
    @IBOutlet weak var verifiedImageView: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var profilePictureImageView: UIImageView!
    @IBOutlet weak var thirdChefCapImageView: UIImageView!
    @IBOutlet weak var secondChefCapImageView: UIImageView!
    @IBOutlet weak var firstChefCapImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var followAndFollowingButton: UIButton!
    
    
    // MARK: - Variables
    
    var user: User?
    var delegate: FollowersAndFollowingTableViewCellDelegate?
    
    
    // MARK: - ViewController LifeCycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        addGestures()
        backgroundColor = .tableViewBackgroundColor()
        
        containerView.roundVeryLittle()
        profilePictureImageView.round()
        verifiedImageView.isHidden = true
        
    }

    
    // MARK: - Private Methods
    
    func addGestures()  {
        
        let followTapGesture = UITapGestureRecognizer(target: self, action: #selector(followTapped))
        followAndFollowingButton.addGestureRecognizer(followTapGesture)
        followAndFollowingButton.isUserInteractionEnabled = true

//        let profileImageViewTapGesture = UITapGestureRecognizer(target: self, action: #selector(openUserDetailsTapped))
//        profilePictureImageView.addGestureRecognizer(profileImageViewTapGesture)
//        profilePictureImageView.isUserInteractionEnabled = true
    }
    
    public func configureCell(with user: User) {
        self.user = user
        
        self.nameLabel.text = user.fullName()
        self.usernameLabel.text = user.userName
        
        verifiedImageView.isHidden = !user.isVerified
        
        if let userPicture = user.userPicture {
            self.profilePictureImageView.kf.setImage(with: URL(string:userPicture), placeholder: UIImage(named:"ic_profile_placeholder"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        else{
            let iconImage = "ic_profile_placeholder"
            self.profilePictureImageView.image = UIImage(named: iconImage)
        }
        
        if user.isFollowed {
            self.followAndFollowingButton.followingButton()
        }
        else
        {
            self.followAndFollowingButton.followButton()
        }
        
        if let cap1 = user.chefCaps?.cap1 {
            self.firstChefCapImageView.kf.setImage(with: URL(string:cap1), placeholder: UIImage(named:"ic_chefcap_default"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        
        if let cap2 = user.chefCaps?.cap2 {
            self.secondChefCapImageView.kf.setImage(with: URL(string:cap2), placeholder: UIImage(named:"ic_chefcap_default"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        
        if let cap3 = user.chefCaps?.cap3 {
            self.thirdChefCapImageView.kf.setImage(with: URL(string:cap3), placeholder: UIImage(named:"ic_chefcap_default"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        
    }
    
    // MARK: - Actions
    
    @objc func followTapped() {
        if self.delegate != nil {
            self.delegate?.follow(user: user!)
        }
    }
    
    @objc func openUserDetailsTapped() {
        if self.delegate != nil {
            self.delegate?.openDetails(user: user!)
        }
    }
    
   
    
}
