//
//  UITextField+Extension.swift
//  Tarkeeb
//
//  Created by Osama Azmat Khan on 9/22/18.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
    
    // MARK: - default
    func defaultTextField() {
        round()
//        dropShadow()
        self.backgroundColor = UIColor.textFieldColor()
        self.textColor = .white
        
        
        //
        let colorAttributes = [
            NSAttributedStringKey.foregroundColor : UIColor.white.withAlphaComponent(0.75)]
        let attributeString = NSMutableAttributedString(string: self.placeholder!, attributes: colorAttributes)
        self.attributedPlaceholder = attributeString
        
        //
        self.setLeftPaddingPoints(20)
        self.setRightPaddingPoints(20)
    }
    
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}
