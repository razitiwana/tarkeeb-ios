//
//  UIView+Extension.swift
//  Tarkeeb
//
//  Created by Razi Tiwana on 08/09/2018.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import UIKit

extension UIView {
    
    static let cornerRadius :CGFloat = 10
    
    func round() {
        self.layer.cornerRadius = self.frame.height / 2
        self.layer.masksToBounds = true
    }
    
    func roundVeryLittle(cornerRadius: CGFloat = UIView.cornerRadius) {
        self.layer.cornerRadius = cornerRadius
        self.clipsToBounds = true
    }
    
    func roundTwenty() {
        self.layer.cornerRadius = 20
        self.clipsToBounds = true
    }
    
    func roundBottomOnly() {
        self.clipsToBounds = true
//        self.layer.cornerRadius = UIView.cornerRadius
        if #available(iOS 11.0, *) {
            self.layer.cornerRadius = UIView.cornerRadius
            self.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        } else {
            // Fallback on earlier versions
            self.roundCorners([.bottomLeft, .bottomRight], radius: UIView.cornerRadius)
        }
    }
    
    func roundTopOnly() {
        self.clipsToBounds = true
        
        if #available(iOS 11.0, *) {
            self.layer.cornerRadius = UIView.cornerRadius
            self.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        } else {
            self.roundCorners([.topLeft, .topRight], radius: UIView.cornerRadius)
        }
    }
    
    // Should use roundVeryLittle but for some reason not working for ingrediant
    func roundTopBottom() {
        self.clipsToBounds = true
        
        if #available(iOS 11.0, *) {
            self.layer.cornerRadius = UIView.cornerRadius
            self.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMinXMinYCorner]
        } else {
            self.roundCorners([.topLeft, .topRight, .topLeft, .topRight], radius: UIView.cornerRadius)
        }
    }
    
    func defaultState() {
        self.clipsToBounds = false
        self.layer.cornerRadius = 0
        if #available(iOS 11.0, *) {
            self.layer.maskedCorners = []
        } else {
//            self.roundCorners([.topLeft, .topRight], radius: 0)
//            self.roundCorners([.bottomLeft, .bottomRight], radius: 0)
            layer.mask = nil
        }
    }
    
    func roundCorners(_ corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.frame = bounds
        mask.path = path.cgPath
        layer.mask = mask
    }
    
    func addBorders() {
        self.borders(for: [.all])
    }
    
    func addAppThemeBorder(width: CGFloat = 2) {
        self.borders(for: [.all], width: width, color: .appThemeColor())
    }
    
    func borders(for edges:[UIRectEdge], width:CGFloat = 2, color: UIColor = UIColor.borderColor()) {
        if edges.contains(.all) {
            layer.borderWidth = width
            layer.borderColor = color.cgColor
        } else {
            let allSpecificBorders:[UIRectEdge] = [.top, .bottom, .left, .right]
            
            for edge in allSpecificBorders {
                if let v = viewWithTag(Int(edge.rawValue)) {
                    v.removeFromSuperview()
                }
                
                if edges.contains(edge) {
                    let v = UIView()
                    v.tag = Int(edge.rawValue)
                    v.backgroundColor = color
                    v.translatesAutoresizingMaskIntoConstraints = false
                    addSubview(v)
                    
                    var horizontalVisualFormat = "H:"
                    var verticalVisualFormat = "V:"
                    
                    switch edge {
                    case UIRectEdge.bottom:
                        horizontalVisualFormat += "|-(0)-[v]-(0)-|"
                        verticalVisualFormat += "[v(\(width))]-(0)-|"
                    case UIRectEdge.top:
                        horizontalVisualFormat += "|-(0)-[v]-(0)-|"
                        verticalVisualFormat += "|-(0)-[v(\(width))]"
                    case UIRectEdge.left:
                        horizontalVisualFormat += "|-(0)-[v(\(width))]"
                        verticalVisualFormat += "|-(0)-[v]-(0)-|"
                    case UIRectEdge.right:
                        horizontalVisualFormat += "[v(\(width))]-(0)-|"
                        verticalVisualFormat += "|-(0)-[v]-(0)-|"
                    default:
                        break
                    }
                    
                    self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: horizontalVisualFormat, options: .directionLeadingToTrailing, metrics: nil, views: ["v": v]))
                    self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: verticalVisualFormat, options: .directionLeadingToTrailing, metrics: nil, views: ["v": v]))
                }
            }
        }
    }
        
    func addGradiant(name: String = "masklayer", firstColor: UIColor = #colorLiteral(red: 0.9098039216, green: 0.2549019608, blue: 0.2117647059, alpha: 1), secondColor: UIColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)) {
        
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.colors = [firstColor, secondColor].map {$0.cgColor}
        gradient.locations = [0.0 , 1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 0.05, y: 1.0)
        gradient.frame = CGRect(x: 0.0, y: 0.0, width: self.frame.size.width, height: self.frame.size.height)
        gradient.name = name
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func removeGradiant(name: String = "masklayer") {
        for layer in self.layer.sublayers! {
            if layer.name == name {
                layer.removeFromSuperlayer()
            }
        }
    }
    
    func dropShadow(scale: Bool = true, color: UIColor = UIColor.shadowColor()) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = 0.3
        layer.shadowOffset = CGSize(width: 0, height: 5)
        layer.shadowRadius = 5
        
        //        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    func errorAnimation() {
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 4
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: center.x - 20, y: center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: center.x + 20, y: center.y))
        layer.add(animation, forKey: "position")
    }
    
    // MARK: - Fetch all Subviews
    
    /** This is the function to get subViews of a view of a particular type
     */
    func subViews<T : UIView>(type : T.Type) -> [T] {
        var all = [T]()
        
        for view in self.subviews {
            if let aView = view as? T {
                all.append(aView)
            }
        }
        return all
    }
    
    
    /** This is a function to get subviews of a particular type from view recursively. It would look recursively in all subviews and return back the subviews of the type T */
    func allSubViewsOf<T : UIView>(type : T.Type) -> [T] {
        var all = [T]()
        func getSubview(view: UIView) {
            if let aView = view as? T {
                all.append(aView)
            }
            guard view.subviews.count > 0 else { return }
            view.subviews.forEach { getSubview(view: $0) }
        }
        getSubview(view: self)
        return all
    }
    
    func blurView(style: UIBlurEffectStyle = .regular) {
        let blurEffect = UIBlurEffect(style: style)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        blurEffectView.tag = 10
        self.addSubview(blurEffectView)
        self.sendSubview(toBack: blurEffectView)
        blurEffectView.isUserInteractionEnabled = false
    }
    
    func removeBlurView() {
        guard let blurView = self.viewWithTag(10) else {return}
        blurView.removeFromSuperview()
    }
}
