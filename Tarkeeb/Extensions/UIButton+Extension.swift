//
//  UIButton+Extension.swift
//  Tarkeeb
//
//  Created by Osama Azmat Khan on 9/22/18.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import UIKit

extension UIButton {
    
    // MARK: - default
    
    func defaultButton() {
        round()
        dropShadow()
        self.backgroundColor = .white
        
        changeColorForTitle(color: .white)
    }
    
    func followButton() {
        self.setTitle("Follow", for: .normal)
        //self.titleLabel?.text = "Follow"
        roundVeryLittle()
        round()
        
        //dropShadow()
        self.backgroundColor = .appThemeColor()
        
        changeColorForTitle(color: .white)
    }
    
    func followingButton() {
        self.setTitle("Following", for: .normal)
        //self.titleLabel?.text = "Following"
        roundVeryLittle()
        round()
        addAppThemeBorder(width: 1)
        //dropShadow()
        self.backgroundColor = .white
        
        changeColorForTitle(color: .appThemeColor())
    }
    
    func defaultUnSelectedButton() {
        round()
//        dropShadow(scale: true, color: UIColor.defaultButtonGreyColor())
//        self.backgroundColor = UIColor.defaultButtonGreyColor()
        
        changeColorForTitle(color: .lightText)
    }
    
    func changeColorForTitle(color: UIColor = .appThemeColor()) {
        let colorAttributes = [
            NSAttributedStringKey.foregroundColor : color]
        
        let attributeString = NSMutableAttributedString(string: self.title(for: .normal)!,
                                                        attributes: colorAttributes)
        
        self.setAttributedTitle(attributeString, for: .normal)
        self.setAttributedTitle(attributeString, for: .selected)
    }
    
    
    func underLineText()  {
        let underLineAttributes : [NSAttributedStringKey: Any] = [
            NSAttributedStringKey.font : UIFont.systemFont(ofSize: (self.titleLabel?.font.pointSize)!),
            NSAttributedStringKey.foregroundColor : UIColor.appThemeColor(),
            NSAttributedStringKey.underlineStyle : NSUnderlineStyle.styleSingle.rawValue]
        
        let attributeString = NSMutableAttributedString(string: self.title(for: .normal)!,
                                                        attributes: underLineAttributes)
        
        self.setAttributedTitle(attributeString, for: .normal)
    }
    
}
