//
//  SegmentedControl+Extension.swift
//  Tarkeeb
//
//  Created by Razi Tiwana on 17/09/2018.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import UIKit

extension UISegmentedControl {
    
    func removeBorder() {
        let backgroundImage = UIImage.getColoredRectImageWith(color: UIColor.white.cgColor, andSize: self.bounds.size)
        self.setBackgroundImage(backgroundImage, for: .normal, barMetrics: .default)
        self.setBackgroundImage(backgroundImage, for: .selected, barMetrics: .default)
        self.setBackgroundImage(backgroundImage, for: .highlighted, barMetrics: .default)
        
        let deviderImage = UIImage.getColoredRectImageWith(color: UIColor.white.cgColor, andSize: CGSize(width: 1.0, height: self.bounds.size.height))
        self.setDividerImage(deviderImage, forLeftSegmentState: .selected, rightSegmentState: .normal, barMetrics: .default)
        
        
        let attributes = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 17), NSAttributedStringKey.foregroundColor: UIColor.black]
        
        self.setTitleTextAttributes(attributes, for: .normal)
        self.setTitleTextAttributes(attributes, for: .selected)
    }
    
    func addUnderlineForSelectedSegment(numberofSegments: Int, width: CGFloat ) {
        if self.viewWithTag(1) != nil {
            return
        }
  
        removeBorder()
        
        let factor: CGFloat = (numberofSegments == 2) ? 32 : 16
        
        let underlineWidth: CGFloat = (width / CGFloat(numberofSegments) - factor)
        
        let underlineHeight: CGFloat = 4.0
        let underlineXPosition = CGFloat((selectedSegmentIndex * Int(underlineWidth)) + Int(factor/2))
        let underLineYPosition = self.bounds.size.height + 0
        let underlineFrame = CGRect(x: underlineXPosition, y: underLineYPosition, width: underlineWidth, height: underlineHeight)
        let underline = UIView(frame: underlineFrame)
        underline.round()
        underline.backgroundColor = UIColor.appThemeColor()
        underline.tag = 1
        self.addSubview(underline)
        self.bringSubview(toFront: underline)
    }
    
    func removeLinefromView() {
        if self.viewWithTag(1) != nil {
            self.viewWithTag(1)?.removeFromSuperview()
        }
    }
    
    func changeUnderlinePosition() {
        guard let underline = self.viewWithTag(1) else {return}
        
        let factor: CGFloat = (self.numberOfSegments == 2) ? 16 : 8

        let underlineFinalXPosition = (self.bounds.width / CGFloat(self.numberOfSegments)) * CGFloat(selectedSegmentIndex) + factor
    
        
        // Need to animate this further
        // Only able to animate once with .layoutSubview
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.8, options: .curveEaseIn, animations: {
             underline.frame.origin.x = underlineFinalXPosition
        }) { (status) in
//             self.bringSubview(toFront: underline)
        }
    }
}

extension UIImage {
    
    class func getColoredRectImageWith(color: CGColor, andSize size: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        let graphicsContext = UIGraphicsGetCurrentContext()
        graphicsContext?.setFillColor(color)
        let rectangle = CGRect(x: 0.0, y: 0.0, width: size.width, height: size.height)
        graphicsContext?.fill(rectangle)
        let rectangleImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return rectangleImage!
    }
}
