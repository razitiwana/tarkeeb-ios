//
//  Notification+NotificationHelper.swift
//  Tarkeeb
//
//  Created by Razi Tiwana on 05/09/2018.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import Foundation

enum NotificationsHelper : String {
    case userProfileUpdated = "userProfileUpdated"
    case notificationCountUpdated = "notificationCountUpdated"
}

extension Foundation.Notification.Name {
    static let userProfileUpdated = Foundation.Notification.Name(
        rawValue: NotificationsHelper.userProfileUpdated.rawValue)
    
    static let notificationCountUpdated = Foundation.Notification.Name(
        rawValue: NotificationsHelper.notificationCountUpdated.rawValue)
}
