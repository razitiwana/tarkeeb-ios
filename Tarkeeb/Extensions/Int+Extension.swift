//
//  Int+Extension.swift
//  Tarkeeb
//
//  Created by Razi Tiwana on 05/09/2018.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import Foundation

extension Int {
    
    var toFloat:Float {
        return Float(self)
    }
    
    var toDouble:Double {
        return Double(self)
    }
    
    var toString:String {
        return "\(self)"
    }
    
    var ordinal: String {
        switch (self % 10) {
        case 1:  return "st";
        case 2:  return "nd";
        case 3:  return "rd";
        default: return "th";
        }
    }
    
    func formatKandM() -> String {
        let num = Double(self)
        var thousandNum = num/1000
        var millionNum = num/1000000
        if num >= 1000 && num < 1000000 {
            if(floor(thousandNum) == thousandNum){
                return("\(Int(thousandNum))K")
            }
            return("\(thousandNum.roundToPlaces(places: 1))K")
        }
        if num > 1000000 {
            if(floor(millionNum) == millionNum){
                return("\(Int(thousandNum))K")
            }
            return ("\(millionNum.roundToPlaces(places: 1))M")
        }
        else{
            if(floor(num) == num){
                return ("\(Int(num))")
            }
            return ("\(num)")
        }
    }
    
}
