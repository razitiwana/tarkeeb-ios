//
//  Color+Extension.swift
//  Tarkeeb
//
//  Created by Razi Tiwana on 05/09/2018.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import UIKit

extension UIColor {
    static func appThemeColor() -> UIColor {
        return #colorLiteral(red: 0.9176470588, green: 0.2549019608, blue: 0.1764705882, alpha: 1)//#colorLiteral(red: 0.9215686275, green: 0.2470588235, blue: 0.1843137255, alpha: 1)
    }
    
    static func tableViewBackgroundColor() -> UIColor {
        return #colorLiteral(red: 0.9019607843, green: 0.9019607843, blue: 0.9019607843, alpha: 1)
    }
    
    static func borderColor() -> UIColor {
        return .white//#colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
    }
    
    static func shadowColor() -> UIColor {
        return #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
    static func textFieldColor() -> UIColor {
        return #colorLiteral(red: 0.662745098, green: 0.2078431373, blue: 0.1647058824, alpha: 1)
    }
}
