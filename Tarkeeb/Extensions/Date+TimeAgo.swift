//
//  Date+TimeAgo.swift
//  Tarkeeb
//
//  Created by Razi Tiwana on 05/09/2018.
//  Copyright © 2018 Tarkeeb Technologies. All rights reserved.
//

import Foundation

extension Date {
    func timeAgo() -> String {
        var secondsAgo = Int(Date().timeIntervalSince(self))
        if secondsAgo < 0 {
            secondsAgo = secondsAgo * (-1)
            return "just now"
        }
        
        let minute = 60
        let hour = 60 * minute
        let day = 24 * hour
        let week = 7 * day
        
        if secondsAgo < minute  {
            if secondsAgo < 10 {
                return "just now"
            } else {
                return "\(secondsAgo) secs ago"
            }
        } else if secondsAgo < hour {
            let min = secondsAgo/minute
            if min == 1{
                return "a min ago"
            }else{
                return "\(min) mins ago"
            }
        } else if secondsAgo < day {
            let hr = secondsAgo/hour
            if hr == 1{
                return "an hr ago"
            } else {
                return "\(hr) hrs ago"
            }
        } else if secondsAgo < week {
            let day = secondsAgo/day
            if day == 1 {
                return "Yesterday"
            } else {
                return "\(day) days ago"
            }
        } else {
            let formatter = DateFormatter()
            formatter.dateFormat = "MMMM d"
            
            // Same year
            if Date().year != self.year {
                formatter.dateFormat = "MMMM d, yyyy"
            }
            
            let strDate: String = formatter.string(from: self)
            return strDate
        }
    }
}
