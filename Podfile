# Uncomment the next line to define a global platform for your project
# platform :ios, '9.0'

target 'Tarkeeb' do
  # Comment the next line if you're not using Swift and don't want to use dynamic frameworks
  use_frameworks!

  # Pods for Tarkeeb

    # Alamofire: Elegant networking in Swift
    # https://github.com/Alamofire/Alamofire
    pod 'Alamofire'
    
    # ObjectMapper: A framweork to convert model objects to and from JSON
    # https://github.com/Hearst-DD/ObjectMapper/
    pod 'ObjectMapper'

    # Kingfisher: A lightweight, pure-Swift library for downloading and caching images
    #https://github.com/onevcat/Kingfisher
    pod 'Kingfisher'

    # IQKeyboardManager: Codeless drop-in universal library allows to prevent issues of keyboard sliding up
    # https://github.com/hackiftekhar/IQKeyboardManager
    pod 'IQKeyboardManagerSwift'

    # MBProgressHud
    # https://github.com/jdg/MBProgressHUD
    pod 'MBProgressHUD', '~> 1.1.0'

    # PKHUD
    # https://github.com/pkluz/PKHUD
    pod 'PKHUD', '~> 5.0'

    # SwiftDate
    # https://github.com/malcommac/SwiftDate
    pod 'SwiftDate', '~> 4.5.0'

    # SwiftyGif
    # https://github.com/kirualex/SwiftyGif
    pod 'SwiftyGif'
    
    # TransitionButton
    # https://github.com/AladinWay/TransitionButton
    pod 'TransitionButton'
    
    # Firebase
    # https://firebase.google.com/docs/ios/setup
    pod 'Firebase/Core'
    pod 'Firebase/Messaging'
    
    pod 'Firebase/Crash'
    
    pod 'Fabric', '~> 1.7.11'
    pod 'Crashlytics', '~> 3.10.7'
    
    # Facebook
    # https://developers.facebook.com/docs/facebook-login/ios/
    pod 'FBSDKLoginKit'
    
    # SkyFloatingLabelTextField
    # https://github.com/Skyscanner/SkyFloatingLabelTextField
    pod 'SkyFloatingLabelTextField', '~> 3.0'
    
  target 'TarkeebTests' do
    inherit! :search_paths
    # Pods for testing
  end

  target 'TarkeebUITests' do
    inherit! :search_paths
    # Pods for testing
  end

end
